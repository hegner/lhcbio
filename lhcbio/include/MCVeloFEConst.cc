// datamodel specific includes
#include "MCVeloFE.h"
#include "MCVeloFEConst.h"
#include "MCVeloFEObj.h"
#include "MCVeloFEData.h"
#include "MCVeloFECollection.h"
#include <iostream>

ConstMCVeloFE::ConstMCVeloFE() : m_obj(new MCVeloFEObj()){
 m_obj->acquire();
};

ConstMCVeloFE::ConstMCVeloFE(double m_addedSignal,double m_addedPedestal,double m_addedNoise,double m_addedCMNoise) : m_obj(new MCVeloFEObj()){
 m_obj->acquire();
   m_obj->data.m_addedSignal = m_addedSignal;  m_obj->data.m_addedPedestal = m_addedPedestal;  m_obj->data.m_addedNoise = m_addedNoise;  m_obj->data.m_addedCMNoise = m_addedCMNoise;
};


ConstMCVeloFE::ConstMCVeloFE(const ConstMCVeloFE& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCVeloFE& ConstMCVeloFE::operator=(const ConstMCVeloFE& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCVeloFE::ConstMCVeloFE(MCVeloFEObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCVeloFE ConstMCVeloFE::clone() const {
  return {new MCVeloFEObj(*m_obj)};
}

ConstMCVeloFE::~ConstMCVeloFE(){
  if ( m_obj != nullptr) m_obj->release();
}


std::vector<ConstMCHit>::const_iterator ConstMCVeloFE::m_MCHits_begin() const {
  auto ret_value = m_obj->m_m_MCHits->begin();
  std::advance(ret_value, m_obj->data.m_MCHits_begin);
  return ret_value;
}

std::vector<ConstMCHit>::const_iterator ConstMCVeloFE::m_MCHits_end() const {
  auto ret_value = m_obj->m_m_MCHits->begin();
  std::advance(ret_value, m_obj->data.m_MCHits_end-1);
  return ++ret_value;
}

unsigned int ConstMCVeloFE::m_MCHits_size() const {
  return (m_obj->data.m_MCHits_end-m_obj->data.m_MCHits_begin);
}

ConstMCHit ConstMCVeloFE::m_MCHits(unsigned int index) const {
  if (m_MCHits_size() > index) {
    return m_obj->m_m_MCHits->at(m_obj->data.m_MCHits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<double>::const_iterator ConstMCVeloFE::MCHitsCharge_begin() const {
  auto ret_value = m_obj->m_MCHitsCharge->begin();
  std::advance(ret_value, m_obj->data.MCHitsCharge_begin);
  return ret_value;
}

std::vector<double>::const_iterator ConstMCVeloFE::MCHitsCharge_end() const {
  auto ret_value = m_obj->m_MCHitsCharge->begin();
  std::advance(ret_value, m_obj->data.MCHitsCharge_end-1);
  return ++ret_value;
}

unsigned int ConstMCVeloFE::MCHitsCharge_size() const {
  return (m_obj->data.MCHitsCharge_end-m_obj->data.MCHitsCharge_begin);
}

double ConstMCVeloFE::MCHitsCharge(unsigned int index) const {
  if (MCHitsCharge_size() > index) {
    return m_obj->m_MCHitsCharge->at(m_obj->data.MCHitsCharge_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCVeloFE::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCVeloFE::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCVeloFE::operator==(const MCVeloFE& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCVeloFE& p1, const MCVeloFE& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
