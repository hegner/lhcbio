// datamodel specific includes
#include "MCSTDeposit.h"
#include "MCSTDepositConst.h"
#include "MCSTDepositObj.h"
#include "MCSTDepositData.h"
#include "MCSTDepositCollection.h"
#include <iostream>
#include "MCHit.h"

ConstMCSTDeposit::ConstMCSTDeposit() : m_obj(new MCSTDepositObj()){
 m_obj->acquire();
};

ConstMCSTDeposit::ConstMCSTDeposit(double m_depositedCharge,STChannelID m_channelID) : m_obj(new MCSTDepositObj()){
 m_obj->acquire();
   m_obj->data.m_depositedCharge = m_depositedCharge;  m_obj->data.m_channelID = m_channelID;
};


ConstMCSTDeposit::ConstMCSTDeposit(const ConstMCSTDeposit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCSTDeposit& ConstMCSTDeposit::operator=(const ConstMCSTDeposit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCSTDeposit::ConstMCSTDeposit(MCSTDepositObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCSTDeposit ConstMCSTDeposit::clone() const {
  return {new MCSTDepositObj(*m_obj)};
}

ConstMCSTDeposit::~ConstMCSTDeposit(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCHit ConstMCSTDeposit::m_mcHit() const { if (m_obj->m_m_mcHit == nullptr) {
 return ConstMCHit(nullptr);}
 return ConstMCHit(*(m_obj->m_m_mcHit));};


bool  ConstMCSTDeposit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCSTDeposit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCSTDeposit::operator==(const MCSTDeposit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCSTDeposit& p1, const MCSTDeposit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
