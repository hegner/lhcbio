#ifndef ConstMCRichDigit_H
#define ConstMCRichDigit_H
#include "MCRichDigitData.h"
#include "MCRichDigitHistoryCode.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit-packed word containing the overall history information for this digit
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichDigitObj;
class MCRichDigit;
class MCRichDigitCollection;
class MCRichDigitCollectionIterator;


#include "MCRichDigitObj.h"

class ConstMCRichDigit {

  friend MCRichDigit;
  friend MCRichDigitCollection;
  friend MCRichDigitCollectionIterator;

public:

  /// default constructor
  ConstMCRichDigit();
  ConstMCRichDigit(int m_hits,MCRichDigitHistoryCode m_history);

  /// constructor from existing MCRichDigitObj
  ConstMCRichDigit(MCRichDigitObj* obj);
  /// copy constructor
  ConstMCRichDigit(const ConstMCRichDigit& other);
  /// copy-assignment operator
  ConstMCRichDigit& operator=(const ConstMCRichDigit& other);
  /// support cloning (deep-copy)
  ConstMCRichDigit clone() const;
  /// destructor
  ~ConstMCRichDigit();


public:

  const int& m_hits() const;
  const MCRichDigitHistoryCode& m_history() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCRichDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCRichDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichDigit& p1,
//       const MCRichDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichDigitObj* m_obj;

};

#endif
