#ifndef ConstMCCaloDigit_H
#define ConstMCCaloDigit_H
#include "MCCaloDigitData.h"
#include <vector>
#include "MCCaloHit.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Monte Carlo active energy deposition in the given cell
// author: Vanya Belyaev Ivan.Belyaev@itep.ru

//forward declarations
class MCCaloDigitObj;
class MCCaloDigit;
class MCCaloDigitCollection;
class MCCaloDigitCollectionIterator;


#include "MCCaloDigitObj.h"

class ConstMCCaloDigit {

  friend MCCaloDigit;
  friend MCCaloDigitCollection;
  friend MCCaloDigitCollectionIterator;

public:

  /// default constructor
  ConstMCCaloDigit();
  ConstMCCaloDigit(double m_activeE);

  /// constructor from existing MCCaloDigitObj
  ConstMCCaloDigit(MCCaloDigitObj* obj);
  /// copy constructor
  ConstMCCaloDigit(const ConstMCCaloDigit& other);
  /// copy-assignment operator
  ConstMCCaloDigit& operator=(const ConstMCCaloDigit& other);
  /// support cloning (deep-copy)
  ConstMCCaloDigit clone() const;
  /// destructor
  ~ConstMCCaloDigit();


public:

  const double& m_activeE() const;

  unsigned int m_hits_size() const;
  ConstMCCaloHit m_hits(unsigned int) const;
  std::vector<ConstMCCaloHit>::const_iterator m_hits_begin() const;
  std::vector<ConstMCCaloHit>::const_iterator m_hits_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCCaloDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCCaloDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCCaloDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCCaloDigit& p1,
//       const MCCaloDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCCaloDigitObj* m_obj;

};

#endif
