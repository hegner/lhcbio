// standard includes
#include <stdexcept>
#include "MCParticleCollection.h" 
#include "MCParticleCollection.h" 


#include "MCVertexCollection.h"

MCVertexCollection::MCVertexCollection() : m_collectionID(0), m_entries() ,m_rel_m_products(new std::vector<ConstMCParticle>()),m_rel_m_mother(new std::vector<ConstMCParticle>()),m_refCollections(nullptr), m_data(new MCVertexDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCVertex MCVertexCollection::operator[](unsigned int index) const {
  return MCVertex(m_entries[index]);
}

const MCVertex MCVertexCollection::at(unsigned int index) const {
  return MCVertex(m_entries.at(index));
}

int  MCVertexCollection::size() const {
  return m_entries.size();
}

MCVertex MCVertexCollection::create(){
  auto obj = new MCVertexObj();
  m_entries.emplace_back(obj);
  m_rel_m_products_tmp.push_back(obj->m_m_products);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCVertex(obj);
}

void MCVertexCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  // clear relations to m_products. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_products_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_products_tmp.clear();
  for (auto& item : (*m_rel_m_products)) {item.unlink(); }
  m_rel_m_products->clear();
  for (auto& item : (*m_rel_m_mother)) {item.unlink(); }
  m_rel_m_mother->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCVertexCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
     (*m_data)[i].m_products_begin=index;
   (*m_data)[i].m_products_end+=index;
   index = (*m_data)[index].m_products_end;
   for(auto it : (*m_rel_m_products_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[0]->emplace_back(it.getObjectID());
     m_rel_m_products->push_back(it);
   }

  }
    for (auto& obj : m_entries) {
if (obj->m_m_mother != nullptr){
(*m_refCollections)[1]->emplace_back(obj->m_m_mother->getObjectID());} else {(*m_refCollections)[1]->push_back({-2,-2}); } };

}

void MCVertexCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCVertexObj({index,m_collectionID}, data);
        obj->m_m_products = m_rel_m_products;
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCVertexCollection::setReferences(const podio::ICollectionProvider* collectionProvider){
  for(unsigned int i=0, size=(*m_refCollections)[0]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCParticleCollection* tmp_coll = static_cast<MCParticleCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_products->emplace_back(tmp);
  }

  for(unsigned int i=0, size=m_entries.size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[1])[i];
    if (id.index != podio::ObjectID::invalid) {
      CollectionBase* coll = nullptr;
      collectionProvider->get(id.collectionID,coll);
      MCParticleCollection* tmp_coll = static_cast<MCParticleCollection*>(coll);
      m_entries[i]->m_m_mother = new ConstMCParticle((*tmp_coll)[id.index]);
    } else {
      m_entries[i]->m_m_mother = nullptr;
    }
  }

  return true; //TODO: check success
}

void MCVertexCollection::push_back(ConstMCVertex object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
          m_rel_m_products_tmp.push_back(obj->m_m_products);

    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCVertexCollection::setBuffer(void* address){
  m_data = static_cast<MCVertexDataContainer*>(address);
}


const MCVertex MCVertexCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCVertex* MCVertexCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCVertexCollectionIterator& MCVertexCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
