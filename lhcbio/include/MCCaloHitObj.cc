#include "MCCaloHitObj.h"
#include "MCParticleConst.h"


MCCaloHitObj::MCCaloHitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_particle(nullptr)

    { }

MCCaloHitObj::MCCaloHitObj(const podio::ObjectID id, MCCaloHitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCCaloHitObj::MCCaloHitObj(const MCCaloHitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCCaloHitObj::~MCCaloHitObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_particle;

  }
}
