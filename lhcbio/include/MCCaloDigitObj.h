#ifndef MCCaloDigitOBJ_H
#define MCCaloDigitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCCaloDigitData.h"

#include <vector>
#include "MCCaloHit.h"


// forward declarations
class MCCaloDigit;
class ConstMCCaloDigit;


class MCCaloDigitObj : public podio::ObjBase {
public:
  /// constructor
  MCCaloDigitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCCaloDigitObj(const MCCaloDigitObj&);
  /// constructor from ObjectID and MCCaloDigitData
  /// does not initialize the internal relation containers
  MCCaloDigitObj(const podio::ObjectID id, MCCaloDigitData data);
  virtual ~MCCaloDigitObj();

public:
  MCCaloDigitData data;
  std::vector<ConstMCCaloHit>* m_m_hits;


};


#endif
