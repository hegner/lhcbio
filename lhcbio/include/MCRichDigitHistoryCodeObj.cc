#include "MCRichDigitHistoryCodeObj.h"


MCRichDigitHistoryCodeObj::MCRichDigitHistoryCodeObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    
    { }

MCRichDigitHistoryCodeObj::MCRichDigitHistoryCodeObj(const podio::ObjectID id, MCRichDigitHistoryCodeData data) :
    ObjBase{id,0},
    data(data)
    { }

MCRichDigitHistoryCodeObj::MCRichDigitHistoryCodeObj(const MCRichDigitHistoryCodeObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCRichDigitHistoryCodeObj::~MCRichDigitHistoryCodeObj() {
  if (id.index == podio::ObjectID::untracked) {

  }
}
