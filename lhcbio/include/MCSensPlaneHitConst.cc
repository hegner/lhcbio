// datamodel specific includes
#include "MCSensPlaneHit.h"
#include "MCSensPlaneHitConst.h"
#include "MCSensPlaneHitObj.h"
#include "MCSensPlaneHitData.h"
#include "MCSensPlaneHitCollection.h"
#include <iostream>
#include "MCParticle.h"

ConstMCSensPlaneHit::ConstMCSensPlaneHit() : m_obj(new MCSensPlaneHitObj()){
 m_obj->acquire();
};

ConstMCSensPlaneHit::ConstMCSensPlaneHit(LorentzVector m_position,LorentzVector m_momentum,int m_type) : m_obj(new MCSensPlaneHitObj()){
 m_obj->acquire();
   m_obj->data.m_position = m_position;  m_obj->data.m_momentum = m_momentum;  m_obj->data.m_type = m_type;
};


ConstMCSensPlaneHit::ConstMCSensPlaneHit(const ConstMCSensPlaneHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCSensPlaneHit& ConstMCSensPlaneHit::operator=(const ConstMCSensPlaneHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCSensPlaneHit::ConstMCSensPlaneHit(MCSensPlaneHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCSensPlaneHit ConstMCSensPlaneHit::clone() const {
  return {new MCSensPlaneHitObj(*m_obj)};
}

ConstMCSensPlaneHit::~ConstMCSensPlaneHit(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCParticle ConstMCSensPlaneHit::m_particle() const { if (m_obj->m_m_particle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_particle));};


bool  ConstMCSensPlaneHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCSensPlaneHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCSensPlaneHit::operator==(const MCSensPlaneHit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCSensPlaneHit& p1, const MCSensPlaneHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
