// datamodel specific includes
#include "MCBcmDigit.h"
#include "MCBcmDigitConst.h"
#include "MCBcmDigitObj.h"
#include "MCBcmDigitData.h"
#include "MCBcmDigitCollection.h"
#include <iostream>

MCBcmDigit::MCBcmDigit() : m_obj(new MCBcmDigitObj()){
 m_obj->acquire();
};



MCBcmDigit::MCBcmDigit(const MCBcmDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCBcmDigit& MCBcmDigit::operator=(const MCBcmDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCBcmDigit::MCBcmDigit(MCBcmDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCBcmDigit MCBcmDigit::clone() const {
  return {new MCBcmDigitObj(*m_obj)};
}

MCBcmDigit::~MCBcmDigit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCBcmDigit::operator ConstMCBcmDigit() const {return ConstMCBcmDigit(m_obj);};



std::vector<int>::const_iterator MCBcmDigit::MCHitKeys_begin() const {
  auto ret_value = m_obj->m_MCHitKeys->begin();
  std::advance(ret_value, m_obj->data.MCHitKeys_begin);
  return ret_value;
}

std::vector<int>::const_iterator MCBcmDigit::MCHitKeys_end() const {
  auto ret_value = m_obj->m_MCHitKeys->begin();
  std::advance(ret_value, m_obj->data.MCHitKeys_end-1);
  return ++ret_value;
}

void MCBcmDigit::addMCHitKeys(int component) {
  m_obj->m_MCHitKeys->push_back(component);
  m_obj->data.MCHitKeys_end++;
}

unsigned int MCBcmDigit::MCHitKeys_size() const {
  return (m_obj->data.MCHitKeys_end-m_obj->data.MCHitKeys_begin);
}

int MCBcmDigit::MCHitKeys(unsigned int index) const {
  if (MCHitKeys_size() > index) {
    return m_obj->m_MCHitKeys->at(m_obj->data.MCHitKeys_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCBcmDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCBcmDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCBcmDigit::operator==(const ConstMCBcmDigit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCBcmDigit& p1, const MCBcmDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
