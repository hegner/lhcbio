#ifndef ConstMCSensPlaneHit_H
#define ConstMCSensPlaneHit_H
#include "MCSensPlaneHitData.h"
#include "LorentzVector.h"
#include "LorentzVector.h"

#include <vector>
#include "podio/ObjectID.h"

// /< true particle Geant identifier
// author: Vanya Belyaev, revised by V.Romanovski and G.Corti

//forward declarations
class MCSensPlaneHitObj;
class MCSensPlaneHit;
class MCSensPlaneHitCollection;
class MCSensPlaneHitCollectionIterator;
class MCParticle;
class ConstMCParticle;


#include "MCSensPlaneHitObj.h"

class ConstMCSensPlaneHit {

  friend MCSensPlaneHit;
  friend MCSensPlaneHitCollection;
  friend MCSensPlaneHitCollectionIterator;

public:

  /// default constructor
  ConstMCSensPlaneHit();
  ConstMCSensPlaneHit(LorentzVector m_position,LorentzVector m_momentum,int m_type);

  /// constructor from existing MCSensPlaneHitObj
  ConstMCSensPlaneHit(MCSensPlaneHitObj* obj);
  /// copy constructor
  ConstMCSensPlaneHit(const ConstMCSensPlaneHit& other);
  /// copy-assignment operator
  ConstMCSensPlaneHit& operator=(const ConstMCSensPlaneHit& other);
  /// support cloning (deep-copy)
  ConstMCSensPlaneHit clone() const;
  /// destructor
  ~ConstMCSensPlaneHit();


public:

  const LorentzVector& m_position() const;
  const LorentzVector& m_momentum() const;
  const int& m_type() const;
  const ConstMCParticle m_particle() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCSensPlaneHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCSensPlaneHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCSensPlaneHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCSensPlaneHit& p1,
//       const MCSensPlaneHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCSensPlaneHitObj* m_obj;

};

#endif
