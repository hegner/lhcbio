#ifndef ConstMCCaloHit_H
#define ConstMCCaloHit_H
#include "MCCaloHitData.h"
#include "Time.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Time slot# for energy deposition(in 25ns units, 0 means 'current BX')
// author: Olivier Callot (Olivier.Callot@cern.ch) & Vanya Belyaev(Ivan.Belyaev@itep.ru)

//forward declarations
class MCCaloHitObj;
class MCCaloHit;
class MCCaloHitCollection;
class MCCaloHitCollectionIterator;
class MCParticle;
class ConstMCParticle;


#include "MCCaloHitObj.h"

class ConstMCCaloHit {

  friend MCCaloHit;
  friend MCCaloHitCollection;
  friend MCCaloHitCollectionIterator;

public:

  /// default constructor
  ConstMCCaloHit();
  ConstMCCaloHit(double m_activeE,int m_sensDetID,Time m_time);

  /// constructor from existing MCCaloHitObj
  ConstMCCaloHit(MCCaloHitObj* obj);
  /// copy constructor
  ConstMCCaloHit(const ConstMCCaloHit& other);
  /// copy-assignment operator
  ConstMCCaloHit& operator=(const ConstMCCaloHit& other);
  /// support cloning (deep-copy)
  ConstMCCaloHit clone() const;
  /// destructor
  ~ConstMCCaloHit();


public:

  const double& m_activeE() const;
  const int& m_sensDetID() const;
  const Time& m_time() const;
  const ConstMCParticle m_particle() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCCaloHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCCaloHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCCaloHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCCaloHit& p1,
//       const MCCaloHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCCaloHitObj* m_obj;

};

#endif
