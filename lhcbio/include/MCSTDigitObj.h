#ifndef MCSTDigitOBJ_H
#define MCSTDigitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCSTDigitData.h"

#include <vector>
#include "MCSTDeposit.h"


// forward declarations
class MCSTDigit;
class ConstMCSTDigit;


class MCSTDigitObj : public podio::ObjBase {
public:
  /// constructor
  MCSTDigitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCSTDigitObj(const MCSTDigitObj&);
  /// constructor from ObjectID and MCSTDigitData
  /// does not initialize the internal relation containers
  MCSTDigitObj(const podio::ObjectID id, MCSTDigitData data);
  virtual ~MCSTDigitObj();

public:
  MCSTDigitData data;
  std::vector<ConstMCSTDeposit>* m_m_mcDeposit;


};


#endif
