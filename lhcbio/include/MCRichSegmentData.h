#ifndef MCRichSegmentDATA_H
#define MCRichSegmentDATA_H

// /< Bit packed field containing detector information
// author: Christopher Jones    Christopher.Rob.Jones@cern.ch



class MCRichSegmentData {
public:
  unsigned m_historyCode; ////< Bit packed field containing detector information 
  unsigned int trajectoryPoints_begin; 
  unsigned int trajectoryPoints_end; 
  unsigned int trajectoryMomenta_begin; 
  unsigned int trajectoryMomenta_end; 
  unsigned int m_MCRichOpticalPhotons_begin; 
  unsigned int m_MCRichOpticalPhotons_end; 
  unsigned int m_MCRichHits_begin; 
  unsigned int m_MCRichHits_end; 

};

#endif
