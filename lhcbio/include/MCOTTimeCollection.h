//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCOTTimeCollection_H
#define  MCOTTimeCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCOTTimeData.h"
#include "MCOTTime.h"
#include "MCOTTimeObj.h"

typedef std::vector<MCOTTimeData> MCOTTimeDataContainer;
typedef std::deque<MCOTTimeObj*> MCOTTimeObjPointerContainer;

class MCOTTimeCollectionIterator {

  public:
    MCOTTimeCollectionIterator(int index, const MCOTTimeObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCOTTimeCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCOTTime operator*() const;
    const MCOTTime* operator->() const;
    const MCOTTimeCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCOTTime m_object;
    const MCOTTimeObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCOTTimeCollection : public podio::CollectionBase {

public:
  typedef const MCOTTimeCollectionIterator const_iterator;

  MCOTTimeCollection();
//  MCOTTimeCollection(const MCOTTimeCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCOTTimeCollection(MCOTTimeVector* data, int collectionID);
  ~MCOTTimeCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCOTTime create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCOTTime create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCOTTime operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCOTTime at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCOTTime object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCOTTimeData>* _getBuffer() { return m_data;};

   

private:
  int m_collectionID;
  MCOTTimeObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCOTDeposit>* m_rel_m_deposits; //relation buffer for r/w
  std::vector<std::vector<ConstMCOTDeposit>*> m_rel_m_deposits_tmp;
 
  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCOTTimeDataContainer* m_data;
};

template<typename... Args>
MCOTTime  MCOTTimeCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCOTTimeObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCOTTime(obj);
}



#endif
