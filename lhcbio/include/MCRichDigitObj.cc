#include "MCRichDigitObj.h"


MCRichDigitObj::MCRichDigitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    
    { }

MCRichDigitObj::MCRichDigitObj(const podio::ObjectID id, MCRichDigitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCRichDigitObj::MCRichDigitObj(const MCRichDigitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCRichDigitObj::~MCRichDigitObj() {
  if (id.index == podio::ObjectID::untracked) {

  }
}
