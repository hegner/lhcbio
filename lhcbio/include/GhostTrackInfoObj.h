#ifndef GhostTrackInfoOBJ_H
#define GhostTrackInfoOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "GhostTrackInfoData.h"



// forward declarations
class GhostTrackInfo;
class ConstGhostTrackInfo;


class GhostTrackInfoObj : public podio::ObjBase {
public:
  /// constructor
  GhostTrackInfoObj();
  /// copy constructor (does a deep-copy of relation containers)
  GhostTrackInfoObj(const GhostTrackInfoObj&);
  /// constructor from ObjectID and GhostTrackInfoData
  /// does not initialize the internal relation containers
  GhostTrackInfoObj(const podio::ObjectID id, GhostTrackInfoData data);
  virtual ~GhostTrackInfoObj();

public:
  GhostTrackInfoData data;


};


#endif
