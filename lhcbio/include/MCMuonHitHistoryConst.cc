// datamodel specific includes
#include "MCMuonHitHistory.h"
#include "MCMuonHitHistoryConst.h"
#include "MCMuonHitHistoryObj.h"
#include "MCMuonHitHistoryData.h"
#include "MCMuonHitHistoryCollection.h"
#include <iostream>

ConstMCMuonHitHistory::ConstMCMuonHitHistory() : m_obj(new MCMuonHitHistoryObj()){
 m_obj->acquire();
};

ConstMCMuonHitHistory::ConstMCMuonHitHistory(unsigned m_hitHistory) : m_obj(new MCMuonHitHistoryObj()){
 m_obj->acquire();
   m_obj->data.m_hitHistory = m_hitHistory;
};


ConstMCMuonHitHistory::ConstMCMuonHitHistory(const ConstMCMuonHitHistory& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCMuonHitHistory& ConstMCMuonHitHistory::operator=(const ConstMCMuonHitHistory& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCMuonHitHistory::ConstMCMuonHitHistory(MCMuonHitHistoryObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCMuonHitHistory ConstMCMuonHitHistory::clone() const {
  return {new MCMuonHitHistoryObj(*m_obj)};
}

ConstMCMuonHitHistory::~ConstMCMuonHitHistory(){
  if ( m_obj != nullptr) m_obj->release();
}



bool  ConstMCMuonHitHistory::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCMuonHitHistory::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCMuonHitHistory::operator==(const MCMuonHitHistory& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCMuonHitHistory& p1, const MCMuonHitHistory& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
