// datamodel specific includes
#include "MCParticle.h"
#include "MCParticleConst.h"
#include "MCParticleObj.h"
#include "MCParticleData.h"
#include "MCParticleCollection.h"
#include <iostream>
#include "MCVertex.h"

ConstMCParticle::ConstMCParticle() : m_obj(new MCParticleObj()){
 m_obj->acquire();
};

ConstMCParticle::ConstMCParticle(LorentzVector m_momentum,ParticleID m_particleID,unsigned m_flags) : m_obj(new MCParticleObj()){
 m_obj->acquire();
   m_obj->data.m_momentum = m_momentum;  m_obj->data.m_particleID = m_particleID;  m_obj->data.m_flags = m_flags;
};


ConstMCParticle::ConstMCParticle(const ConstMCParticle& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCParticle& ConstMCParticle::operator=(const ConstMCParticle& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCParticle::ConstMCParticle(MCParticleObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCParticle ConstMCParticle::clone() const {
  return {new MCParticleObj(*m_obj)};
}

ConstMCParticle::~ConstMCParticle(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCVertex ConstMCParticle::m_originVertex() const { if (m_obj->m_m_originVertex == nullptr) {
 return ConstMCVertex(nullptr);}
 return ConstMCVertex(*(m_obj->m_m_originVertex));};

std::vector<ConstMCVertex>::const_iterator ConstMCParticle::m_endVertices_begin() const {
  auto ret_value = m_obj->m_m_endVertices->begin();
  std::advance(ret_value, m_obj->data.m_endVertices_begin);
  return ret_value;
}

std::vector<ConstMCVertex>::const_iterator ConstMCParticle::m_endVertices_end() const {
  auto ret_value = m_obj->m_m_endVertices->begin();
  std::advance(ret_value, m_obj->data.m_endVertices_end-1);
  return ++ret_value;
}

unsigned int ConstMCParticle::m_endVertices_size() const {
  return (m_obj->data.m_endVertices_end-m_obj->data.m_endVertices_begin);
}

ConstMCVertex ConstMCParticle::m_endVertices(unsigned int index) const {
  if (m_endVertices_size() > index) {
    return m_obj->m_m_endVertices->at(m_obj->data.m_endVertices_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCParticle::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCParticle::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCParticle::operator==(const MCParticle& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCParticle& p1, const MCParticle& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
