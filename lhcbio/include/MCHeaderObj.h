#ifndef MCHeaderOBJ_H
#define MCHeaderOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCHeaderData.h"

#include <vector>
#include "MCVertex.h"


// forward declarations
class MCHeader;
class ConstMCHeader;


class MCHeaderObj : public podio::ObjBase {
public:
  /// constructor
  MCHeaderObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCHeaderObj(const MCHeaderObj&);
  /// constructor from ObjectID and MCHeaderData
  /// does not initialize the internal relation containers
  MCHeaderObj(const podio::ObjectID id, MCHeaderData data);
  virtual ~MCHeaderObj();

public:
  MCHeaderData data;
  std::vector<ConstMCVertex>* m_m_primaryVertices;


};


#endif
