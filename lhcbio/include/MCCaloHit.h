#ifndef MCCaloHit_H
#define MCCaloHit_H
#include "MCCaloHitData.h"
#include "Time.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Time slot# for energy deposition(in 25ns units, 0 means 'current BX')
// author: Olivier Callot (Olivier.Callot@cern.ch) & Vanya Belyaev(Ivan.Belyaev@itep.ru)

//forward declarations
class MCCaloHitCollection;
class MCCaloHitCollectionIterator;
class ConstMCCaloHit;
class MCParticle;
class ConstMCParticle;


#include "MCCaloHitConst.h"
#include "MCCaloHitObj.h"

class MCCaloHit {

  friend MCCaloHitCollection;
  friend MCCaloHitCollectionIterator;
  friend ConstMCCaloHit;

public:

  /// default constructor
  MCCaloHit();
    MCCaloHit(double m_activeE,int m_sensDetID,Time m_time);

  /// constructor from existing MCCaloHitObj
  MCCaloHit(MCCaloHitObj* obj);
  /// copy constructor
  MCCaloHit(const MCCaloHit& other);
  /// copy-assignment operator
  MCCaloHit& operator=(const MCCaloHit& other);
  /// support cloning (deep-copy)
  MCCaloHit clone() const;
  /// destructor
  ~MCCaloHit();

  /// conversion to const object
  operator ConstMCCaloHit () const;

public:

  const double& m_activeE() const;
  const int& m_sensDetID() const;
  const Time& m_time() const;
  const ConstMCParticle m_particle() const;

  void m_activeE(double value);
  void m_sensDetID(int value);
  Time& m_time();
  void m_time(class Time value);
  void m_particle(ConstMCParticle value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCCaloHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCCaloHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCCaloHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCCaloHit& p1,
//       const MCCaloHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCCaloHitObj* m_obj;

};

#endif
