#ifndef MCVPDigitDATA_H
#define MCVPDigitDATA_H

// Charge deposits in a given pixel
// author: Marcin Kucharczyk



class MCVPDigitData {
public:
  unsigned int deposits_begin; 
  unsigned int deposits_end; 
  unsigned int m_mcHits_begin; 
  unsigned int m_mcHits_end; 

};

#endif
