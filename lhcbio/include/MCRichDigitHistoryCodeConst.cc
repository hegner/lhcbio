// datamodel specific includes
#include "MCRichDigitHistoryCode.h"
#include "MCRichDigitHistoryCodeConst.h"
#include "MCRichDigitHistoryCodeObj.h"
#include "MCRichDigitHistoryCodeData.h"
#include "MCRichDigitHistoryCodeCollection.h"
#include <iostream>

ConstMCRichDigitHistoryCode::ConstMCRichDigitHistoryCode() : m_obj(new MCRichDigitHistoryCodeObj()){
 m_obj->acquire();
};

ConstMCRichDigitHistoryCode::ConstMCRichDigitHistoryCode(unsigned m_historyCode) : m_obj(new MCRichDigitHistoryCodeObj()){
 m_obj->acquire();
   m_obj->data.m_historyCode = m_historyCode;
};


ConstMCRichDigitHistoryCode::ConstMCRichDigitHistoryCode(const ConstMCRichDigitHistoryCode& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCRichDigitHistoryCode& ConstMCRichDigitHistoryCode::operator=(const ConstMCRichDigitHistoryCode& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCRichDigitHistoryCode::ConstMCRichDigitHistoryCode(MCRichDigitHistoryCodeObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCRichDigitHistoryCode ConstMCRichDigitHistoryCode::clone() const {
  return {new MCRichDigitHistoryCodeObj(*m_obj)};
}

ConstMCRichDigitHistoryCode::~ConstMCRichDigitHistoryCode(){
  if ( m_obj != nullptr) m_obj->release();
}



bool  ConstMCRichDigitHistoryCode::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCRichDigitHistoryCode::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCRichDigitHistoryCode::operator==(const MCRichDigitHistoryCode& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCRichDigitHistoryCode& p1, const MCRichDigitHistoryCode& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
