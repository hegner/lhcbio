#ifndef MCRichTrackDATA_H
#define MCRichTrackDATA_H

// Complimentary object to a single MCParticle with addition RICH information
// author: Christopher Jones   Christopher.Rob.Jones@cern.ch



class MCRichTrackData {
public:
  unsigned int m_mcSegments_begin; 
  unsigned int m_mcSegments_end; 

};

#endif
