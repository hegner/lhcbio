// datamodel specific includes
#include "MCMuonHitHistory.h"
#include "MCMuonHitHistoryConst.h"
#include "MCMuonHitHistoryObj.h"
#include "MCMuonHitHistoryData.h"
#include "MCMuonHitHistoryCollection.h"
#include <iostream>

MCMuonHitHistory::MCMuonHitHistory() : m_obj(new MCMuonHitHistoryObj()){
 m_obj->acquire();
};

MCMuonHitHistory::MCMuonHitHistory(unsigned m_hitHistory) : m_obj(new MCMuonHitHistoryObj()){
 m_obj->acquire();
   m_obj->data.m_hitHistory = m_hitHistory;
};

MCMuonHitHistory::MCMuonHitHistory(const MCMuonHitHistory& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCMuonHitHistory& MCMuonHitHistory::operator=(const MCMuonHitHistory& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCMuonHitHistory::MCMuonHitHistory(MCMuonHitHistoryObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCMuonHitHistory MCMuonHitHistory::clone() const {
  return {new MCMuonHitHistoryObj(*m_obj)};
}

MCMuonHitHistory::~MCMuonHitHistory(){
  if ( m_obj != nullptr) m_obj->release();
}

MCMuonHitHistory::operator ConstMCMuonHitHistory() const {return ConstMCMuonHitHistory(m_obj);};

  const unsigned& MCMuonHitHistory::m_hitHistory() const { return m_obj->data.m_hitHistory; };

void MCMuonHitHistory::m_hitHistory(unsigned value){ m_obj->data.m_hitHistory = value;}


bool  MCMuonHitHistory::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCMuonHitHistory::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCMuonHitHistory::operator==(const ConstMCMuonHitHistory& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCMuonHitHistory& p1, const MCMuonHitHistory& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
