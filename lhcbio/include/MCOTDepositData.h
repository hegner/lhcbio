#ifndef MCOTDepositDATA_H
#define MCOTDepositDATA_H

// /< MC solution for the correct left/right ambiguity
// author: Jeroen van Tilburg

#include "OTChannelID.h"


class MCOTDepositData {
public:
  int m_type; ////< Deposit type 
  OTChannelID m_channel; ////< OTChannelID 
  double m_time; ////< time 
  double m_driftDistance; ////< Drift distance to wire (used for cheating) 
  int m_ambiguity; ////< MC solution for the correct left/right ambiguity 

};

#endif
