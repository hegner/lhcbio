//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCBcmDigitCollection_H
#define  MCBcmDigitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCBcmDigitData.h"
#include "MCBcmDigit.h"
#include "MCBcmDigitObj.h"

typedef std::vector<MCBcmDigitData> MCBcmDigitDataContainer;
typedef std::deque<MCBcmDigitObj*> MCBcmDigitObjPointerContainer;

class MCBcmDigitCollectionIterator {

  public:
    MCBcmDigitCollectionIterator(int index, const MCBcmDigitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCBcmDigitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCBcmDigit operator*() const;
    const MCBcmDigit* operator->() const;
    const MCBcmDigitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCBcmDigit m_object;
    const MCBcmDigitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCBcmDigitCollection : public podio::CollectionBase {

public:
  typedef const MCBcmDigitCollectionIterator const_iterator;

  MCBcmDigitCollection();
//  MCBcmDigitCollection(const MCBcmDigitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCBcmDigitCollection(MCBcmDigitVector* data, int collectionID);
  ~MCBcmDigitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCBcmDigit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCBcmDigit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCBcmDigit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCBcmDigit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCBcmDigit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCBcmDigitData>* _getBuffer() { return m_data;};

   

private:
  int m_collectionID;
  MCBcmDigitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCBcmDigitDataContainer* m_data;
};

template<typename... Args>
MCBcmDigit  MCBcmDigitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCBcmDigitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCBcmDigit(obj);
}



#endif
