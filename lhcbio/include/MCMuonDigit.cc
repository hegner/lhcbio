// datamodel specific includes
#include "MCMuonDigit.h"
#include "MCMuonDigitConst.h"
#include "MCMuonDigitObj.h"
#include "MCMuonDigitData.h"
#include "MCMuonDigitCollection.h"
#include <iostream>

MCMuonDigit::MCMuonDigit() : m_obj(new MCMuonDigitObj()){
 m_obj->acquire();
};

MCMuonDigit::MCMuonDigit(MCMuonDigitInfo m_DigitInfo,double m_firingTime) : m_obj(new MCMuonDigitObj()){
 m_obj->acquire();
   m_obj->data.m_DigitInfo = m_DigitInfo;  m_obj->data.m_firingTime = m_firingTime;
};

MCMuonDigit::MCMuonDigit(const MCMuonDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCMuonDigit& MCMuonDigit::operator=(const MCMuonDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCMuonDigit::MCMuonDigit(MCMuonDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCMuonDigit MCMuonDigit::clone() const {
  return {new MCMuonDigitObj(*m_obj)};
}

MCMuonDigit::~MCMuonDigit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCMuonDigit::operator ConstMCMuonDigit() const {return ConstMCMuonDigit(m_obj);};

  const MCMuonDigitInfo& MCMuonDigit::m_DigitInfo() const { return m_obj->data.m_DigitInfo; };
  const double& MCMuonDigit::m_firingTime() const { return m_obj->data.m_firingTime; };

  MCMuonDigitInfo& MCMuonDigit::m_DigitInfo() { return m_obj->data.m_DigitInfo; };
void MCMuonDigit::m_DigitInfo(class MCMuonDigitInfo value){ m_obj->data.m_DigitInfo = value;}
void MCMuonDigit::m_firingTime(double value){ m_obj->data.m_firingTime = value;}

std::vector<ConstMCHit>::const_iterator MCMuonDigit::m_MCHits_begin() const {
  auto ret_value = m_obj->m_m_MCHits->begin();
  std::advance(ret_value, m_obj->data.m_MCHits_begin);
  return ret_value;
}

std::vector<ConstMCHit>::const_iterator MCMuonDigit::m_MCHits_end() const {
  auto ret_value = m_obj->m_m_MCHits->begin();
  std::advance(ret_value, m_obj->data.m_MCHits_end-1);
  return ++ret_value;
}

void MCMuonDigit::addm_MCHits(ConstMCHit component) {
  m_obj->m_m_MCHits->push_back(component);
  m_obj->data.m_MCHits_end++;
}

unsigned int MCMuonDigit::m_MCHits_size() const {
  return (m_obj->data.m_MCHits_end-m_obj->data.m_MCHits_begin);
}

ConstMCHit MCMuonDigit::m_MCHits(unsigned int index) const {
  if (m_MCHits_size() > index) {
    return m_obj->m_m_MCHits->at(m_obj->data.m_MCHits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<MCMuonHitHistory>::const_iterator MCMuonDigit::HitsHistory_begin() const {
  auto ret_value = m_obj->m_HitsHistory->begin();
  std::advance(ret_value, m_obj->data.HitsHistory_begin);
  return ret_value;
}

std::vector<MCMuonHitHistory>::const_iterator MCMuonDigit::HitsHistory_end() const {
  auto ret_value = m_obj->m_HitsHistory->begin();
  std::advance(ret_value, m_obj->data.HitsHistory_end-1);
  return ++ret_value;
}

void MCMuonDigit::addHitsHistory(MCMuonHitHistory component) {
  m_obj->m_HitsHistory->push_back(component);
  m_obj->data.HitsHistory_end++;
}

unsigned int MCMuonDigit::HitsHistory_size() const {
  return (m_obj->data.HitsHistory_end-m_obj->data.HitsHistory_begin);
}

MCMuonHitHistory MCMuonDigit::HitsHistory(unsigned int index) const {
  if (HitsHistory_size() > index) {
    return m_obj->m_HitsHistory->at(m_obj->data.HitsHistory_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCMuonDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCMuonDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCMuonDigit::operator==(const ConstMCMuonDigit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCMuonDigit& p1, const MCMuonDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
