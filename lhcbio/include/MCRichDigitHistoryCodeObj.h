#ifndef MCRichDigitHistoryCodeOBJ_H
#define MCRichDigitHistoryCodeOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCRichDigitHistoryCodeData.h"



// forward declarations
class MCRichDigitHistoryCode;
class ConstMCRichDigitHistoryCode;


class MCRichDigitHistoryCodeObj : public podio::ObjBase {
public:
  /// constructor
  MCRichDigitHistoryCodeObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCRichDigitHistoryCodeObj(const MCRichDigitHistoryCodeObj&);
  /// constructor from ObjectID and MCRichDigitHistoryCodeData
  /// does not initialize the internal relation containers
  MCRichDigitHistoryCodeObj(const podio::ObjectID id, MCRichDigitHistoryCodeData data);
  virtual ~MCRichDigitHistoryCodeObj();

public:
  MCRichDigitHistoryCodeData data;


};


#endif
