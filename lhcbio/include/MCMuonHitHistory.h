#ifndef MCMuonHitHistory_H
#define MCMuonHitHistory_H
#include "MCMuonHitHistoryData.h"

#include <vector>
#include "podio/ObjectID.h"

// /< hit history bit pattern
// author: Alessia Satta

//forward declarations
class MCMuonHitHistoryCollection;
class MCMuonHitHistoryCollectionIterator;
class ConstMCMuonHitHistory;


#include "MCMuonHitHistoryConst.h"
#include "MCMuonHitHistoryObj.h"

class MCMuonHitHistory {

  friend MCMuonHitHistoryCollection;
  friend MCMuonHitHistoryCollectionIterator;
  friend ConstMCMuonHitHistory;

public:

  /// default constructor
  MCMuonHitHistory();
    MCMuonHitHistory(unsigned m_hitHistory);

  /// constructor from existing MCMuonHitHistoryObj
  MCMuonHitHistory(MCMuonHitHistoryObj* obj);
  /// copy constructor
  MCMuonHitHistory(const MCMuonHitHistory& other);
  /// copy-assignment operator
  MCMuonHitHistory& operator=(const MCMuonHitHistory& other);
  /// support cloning (deep-copy)
  MCMuonHitHistory clone() const;
  /// destructor
  ~MCMuonHitHistory();

  /// conversion to const object
  operator ConstMCMuonHitHistory () const;

public:

  const unsigned& m_hitHistory() const;

  void m_hitHistory(unsigned value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCMuonHitHistoryObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCMuonHitHistory& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCMuonHitHistory& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCMuonHitHistory& p1,
//       const MCMuonHitHistory& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCMuonHitHistoryObj* m_obj;

};

#endif
