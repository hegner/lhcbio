#include "GhostTrackInfoObj.h"


GhostTrackInfoObj::GhostTrackInfoObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    
    { }

GhostTrackInfoObj::GhostTrackInfoObj(const podio::ObjectID id, GhostTrackInfoData data) :
    ObjBase{id,0},
    data(data)
    { }

GhostTrackInfoObj::GhostTrackInfoObj(const GhostTrackInfoObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

GhostTrackInfoObj::~GhostTrackInfoObj() {
  if (id.index == podio::ObjectID::untracked) {

  }
}
