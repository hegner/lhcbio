#ifndef MCSensPlaneHitOBJ_H
#define MCSensPlaneHitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCSensPlaneHitData.h"



// forward declarations
class MCSensPlaneHit;
class ConstMCSensPlaneHit;
class ConstMCParticle;


class MCSensPlaneHitObj : public podio::ObjBase {
public:
  /// constructor
  MCSensPlaneHitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCSensPlaneHitObj(const MCSensPlaneHitObj&);
  /// constructor from ObjectID and MCSensPlaneHitData
  /// does not initialize the internal relation containers
  MCSensPlaneHitObj(const podio::ObjectID id, MCSensPlaneHitData data);
  virtual ~MCSensPlaneHitObj();

public:
  MCSensPlaneHitData data;
  ConstMCParticle* m_m_particle;


};


#endif
