#ifndef MCRichDigitOBJ_H
#define MCRichDigitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCRichDigitData.h"



// forward declarations
class MCRichDigit;
class ConstMCRichDigit;


class MCRichDigitObj : public podio::ObjBase {
public:
  /// constructor
  MCRichDigitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCRichDigitObj(const MCRichDigitObj&);
  /// constructor from ObjectID and MCRichDigitData
  /// does not initialize the internal relation containers
  MCRichDigitObj(const podio::ObjectID id, MCRichDigitData data);
  virtual ~MCRichDigitObj();

public:
  MCRichDigitData data;


};


#endif
