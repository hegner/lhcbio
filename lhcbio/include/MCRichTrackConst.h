#ifndef ConstMCRichTrack_H
#define ConstMCRichTrack_H
#include "MCRichTrackData.h"
#include <vector>
#include "MCRichSegment.h"

#include <vector>
#include "podio/ObjectID.h"

// Complimentary object to a single MCParticle with addition RICH information
// author: Christopher Jones   Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichTrackObj;
class MCRichTrack;
class MCRichTrackCollection;
class MCRichTrackCollectionIterator;
class MCParticle;
class ConstMCParticle;


#include "MCRichTrackObj.h"

class ConstMCRichTrack {

  friend MCRichTrack;
  friend MCRichTrackCollection;
  friend MCRichTrackCollectionIterator;

public:

  /// default constructor
  ConstMCRichTrack();
  
  /// constructor from existing MCRichTrackObj
  ConstMCRichTrack(MCRichTrackObj* obj);
  /// copy constructor
  ConstMCRichTrack(const ConstMCRichTrack& other);
  /// copy-assignment operator
  ConstMCRichTrack& operator=(const ConstMCRichTrack& other);
  /// support cloning (deep-copy)
  ConstMCRichTrack clone() const;
  /// destructor
  ~ConstMCRichTrack();


public:

  const ConstMCParticle m_mcParticle() const;

  unsigned int m_mcSegments_size() const;
  ConstMCRichSegment m_mcSegments(unsigned int) const;
  std::vector<ConstMCRichSegment>::const_iterator m_mcSegments_begin() const;
  std::vector<ConstMCRichSegment>::const_iterator m_mcSegments_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichTrackObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCRichTrack& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCRichTrack& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichTrack& p1,
//       const MCRichTrack& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichTrackObj* m_obj;

};

#endif
