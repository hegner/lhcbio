// datamodel specific includes
#include "MCHit.h"
#include "MCHitConst.h"
#include "MCHitObj.h"
#include "MCHitData.h"
#include "MCHitCollection.h"
#include <iostream>
#include "MCParticle.h"

ConstMCHit::ConstMCHit() : m_obj(new MCHitObj()){
 m_obj->acquire();
};

ConstMCHit::ConstMCHit(int m_sensDetID,XYZPoint m_entry,XYZVector m_displacement,double m_energy,double m_time,double m_p) : m_obj(new MCHitObj()){
 m_obj->acquire();
   m_obj->data.m_sensDetID = m_sensDetID;  m_obj->data.m_entry = m_entry;  m_obj->data.m_displacement = m_displacement;  m_obj->data.m_energy = m_energy;  m_obj->data.m_time = m_time;  m_obj->data.m_p = m_p;
};


ConstMCHit::ConstMCHit(const ConstMCHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCHit& ConstMCHit::operator=(const ConstMCHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCHit::ConstMCHit(MCHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCHit ConstMCHit::clone() const {
  return {new MCHitObj(*m_obj)};
}

ConstMCHit::~ConstMCHit(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCParticle ConstMCHit::m_MCParticle() const { if (m_obj->m_m_MCParticle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_MCParticle));};


bool  ConstMCHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCHit::operator==(const MCHit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCHit& p1, const MCHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
