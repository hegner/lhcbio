// datamodel specific includes
#include "GhostTrackInfo.h"
#include "GhostTrackInfoConst.h"
#include "GhostTrackInfoObj.h"
#include "GhostTrackInfoData.h"
#include "GhostTrackInfoCollection.h"
#include <iostream>

ConstGhostTrackInfo::ConstGhostTrackInfo() : m_obj(new GhostTrackInfoObj()){
 m_obj->acquire();
};

ConstGhostTrackInfo::ConstGhostTrackInfo(LinkMap m_linkMap,Classification m_classification) : m_obj(new GhostTrackInfoObj()){
 m_obj->acquire();
   m_obj->data.m_linkMap = m_linkMap;  m_obj->data.m_classification = m_classification;
};


ConstGhostTrackInfo::ConstGhostTrackInfo(const ConstGhostTrackInfo& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstGhostTrackInfo& ConstGhostTrackInfo::operator=(const ConstGhostTrackInfo& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstGhostTrackInfo::ConstGhostTrackInfo(GhostTrackInfoObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstGhostTrackInfo ConstGhostTrackInfo::clone() const {
  return {new GhostTrackInfoObj(*m_obj)};
}

ConstGhostTrackInfo::~ConstGhostTrackInfo(){
  if ( m_obj != nullptr) m_obj->release();
}



bool  ConstGhostTrackInfo::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstGhostTrackInfo::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstGhostTrackInfo::operator==(const GhostTrackInfo& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const GhostTrackInfo& p1, const GhostTrackInfo& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
