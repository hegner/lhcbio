#include "MCRichDigitHitObj.h"
#include "MCRichHitConst.h"


MCRichDigitHitObj::MCRichDigitHitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_mcRichHit(nullptr)

    { }

MCRichDigitHitObj::MCRichDigitHitObj(const podio::ObjectID id, MCRichDigitHitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCRichDigitHitObj::MCRichDigitHitObj(const MCRichDigitHitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCRichDigitHitObj::~MCRichDigitHitObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_mcRichHit;

  }
}
