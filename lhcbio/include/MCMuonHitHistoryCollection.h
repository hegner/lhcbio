//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCMuonHitHistoryCollection_H
#define  MCMuonHitHistoryCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCMuonHitHistoryData.h"
#include "MCMuonHitHistory.h"
#include "MCMuonHitHistoryObj.h"

typedef std::vector<MCMuonHitHistoryData> MCMuonHitHistoryDataContainer;
typedef std::deque<MCMuonHitHistoryObj*> MCMuonHitHistoryObjPointerContainer;

class MCMuonHitHistoryCollectionIterator {

  public:
    MCMuonHitHistoryCollectionIterator(int index, const MCMuonHitHistoryObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCMuonHitHistoryCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCMuonHitHistory operator*() const;
    const MCMuonHitHistory* operator->() const;
    const MCMuonHitHistoryCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCMuonHitHistory m_object;
    const MCMuonHitHistoryObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCMuonHitHistoryCollection : public podio::CollectionBase {

public:
  typedef const MCMuonHitHistoryCollectionIterator const_iterator;

  MCMuonHitHistoryCollection();
//  MCMuonHitHistoryCollection(const MCMuonHitHistoryCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCMuonHitHistoryCollection(MCMuonHitHistoryVector* data, int collectionID);
  ~MCMuonHitHistoryCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCMuonHitHistory create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCMuonHitHistory create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCMuonHitHistory operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCMuonHitHistory at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCMuonHitHistory object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCMuonHitHistoryData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<unsigned,arraysize> m_hitHistory() const;


private:
  int m_collectionID;
  MCMuonHitHistoryObjPointerContainer m_entries;
  // members to handle 1-to-N-relations

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCMuonHitHistoryDataContainer* m_data;
};

template<typename... Args>
MCMuonHitHistory  MCMuonHitHistoryCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCMuonHitHistoryObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCMuonHitHistory(obj);
}

template<size_t arraysize>
const std::array<unsigned,arraysize> MCMuonHitHistoryCollection::m_hitHistory() const {
  std::array<unsigned,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_hitHistory;
 }
 return tmp;
}


#endif
