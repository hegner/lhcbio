#include "MCSTDigitObj.h"


MCSTDigitObj::MCSTDigitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_mcDeposit(new std::vector<ConstMCSTDeposit>())
    { }

MCSTDigitObj::MCSTDigitObj(const podio::ObjectID id, MCSTDigitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCSTDigitObj::MCSTDigitObj(const MCSTDigitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_mcDeposit(new std::vector<ConstMCSTDeposit>(*(other.m_m_mcDeposit)))
    { }

MCSTDigitObj::~MCSTDigitObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_mcDeposit;

  }
}
