//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCRichDigitHistoryCodeCollection_H
#define  MCRichDigitHistoryCodeCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCRichDigitHistoryCodeData.h"
#include "MCRichDigitHistoryCode.h"
#include "MCRichDigitHistoryCodeObj.h"

typedef std::vector<MCRichDigitHistoryCodeData> MCRichDigitHistoryCodeDataContainer;
typedef std::deque<MCRichDigitHistoryCodeObj*> MCRichDigitHistoryCodeObjPointerContainer;

class MCRichDigitHistoryCodeCollectionIterator {

  public:
    MCRichDigitHistoryCodeCollectionIterator(int index, const MCRichDigitHistoryCodeObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCRichDigitHistoryCodeCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCRichDigitHistoryCode operator*() const;
    const MCRichDigitHistoryCode* operator->() const;
    const MCRichDigitHistoryCodeCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCRichDigitHistoryCode m_object;
    const MCRichDigitHistoryCodeObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCRichDigitHistoryCodeCollection : public podio::CollectionBase {

public:
  typedef const MCRichDigitHistoryCodeCollectionIterator const_iterator;

  MCRichDigitHistoryCodeCollection();
//  MCRichDigitHistoryCodeCollection(const MCRichDigitHistoryCodeCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCRichDigitHistoryCodeCollection(MCRichDigitHistoryCodeVector* data, int collectionID);
  ~MCRichDigitHistoryCodeCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCRichDigitHistoryCode create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCRichDigitHistoryCode create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCRichDigitHistoryCode operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCRichDigitHistoryCode at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCRichDigitHistoryCode object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCRichDigitHistoryCodeData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<unsigned,arraysize> m_historyCode() const;


private:
  int m_collectionID;
  MCRichDigitHistoryCodeObjPointerContainer m_entries;
  // members to handle 1-to-N-relations

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCRichDigitHistoryCodeDataContainer* m_data;
};

template<typename... Args>
MCRichDigitHistoryCode  MCRichDigitHistoryCodeCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCRichDigitHistoryCodeObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCRichDigitHistoryCode(obj);
}

template<size_t arraysize>
const std::array<unsigned,arraysize> MCRichDigitHistoryCodeCollection::m_historyCode() const {
  std::array<unsigned,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_historyCode;
 }
 return tmp;
}


#endif
