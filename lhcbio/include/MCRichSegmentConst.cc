// datamodel specific includes
#include "MCRichSegment.h"
#include "MCRichSegmentConst.h"
#include "MCRichSegmentObj.h"
#include "MCRichSegmentData.h"
#include "MCRichSegmentCollection.h"
#include <iostream>
#include "MCParticle.h"
#include "MCRichTrack.h"

ConstMCRichSegment::ConstMCRichSegment() : m_obj(new MCRichSegmentObj()){
 m_obj->acquire();
};

ConstMCRichSegment::ConstMCRichSegment(unsigned m_historyCode) : m_obj(new MCRichSegmentObj()){
 m_obj->acquire();
   m_obj->data.m_historyCode = m_historyCode;
};


ConstMCRichSegment::ConstMCRichSegment(const ConstMCRichSegment& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCRichSegment& ConstMCRichSegment::operator=(const ConstMCRichSegment& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCRichSegment::ConstMCRichSegment(MCRichSegmentObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCRichSegment ConstMCRichSegment::clone() const {
  return {new MCRichSegmentObj(*m_obj)};
}

ConstMCRichSegment::~ConstMCRichSegment(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCParticle ConstMCRichSegment::m_mcParticle() const { if (m_obj->m_m_mcParticle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_mcParticle));};
  const ConstMCRichTrack ConstMCRichSegment::m_MCRichTrack() const { if (m_obj->m_m_MCRichTrack == nullptr) {
 return ConstMCRichTrack(nullptr);}
 return ConstMCRichTrack(*(m_obj->m_m_MCRichTrack));};

std::vector<ConstMCRichOpticalPhoton>::const_iterator ConstMCRichSegment::m_MCRichOpticalPhotons_begin() const {
  auto ret_value = m_obj->m_m_MCRichOpticalPhotons->begin();
  std::advance(ret_value, m_obj->data.m_MCRichOpticalPhotons_begin);
  return ret_value;
}

std::vector<ConstMCRichOpticalPhoton>::const_iterator ConstMCRichSegment::m_MCRichOpticalPhotons_end() const {
  auto ret_value = m_obj->m_m_MCRichOpticalPhotons->begin();
  std::advance(ret_value, m_obj->data.m_MCRichOpticalPhotons_end-1);
  return ++ret_value;
}

unsigned int ConstMCRichSegment::m_MCRichOpticalPhotons_size() const {
  return (m_obj->data.m_MCRichOpticalPhotons_end-m_obj->data.m_MCRichOpticalPhotons_begin);
}

ConstMCRichOpticalPhoton ConstMCRichSegment::m_MCRichOpticalPhotons(unsigned int index) const {
  if (m_MCRichOpticalPhotons_size() > index) {
    return m_obj->m_m_MCRichOpticalPhotons->at(m_obj->data.m_MCRichOpticalPhotons_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<ConstMCRichHit>::const_iterator ConstMCRichSegment::m_MCRichHits_begin() const {
  auto ret_value = m_obj->m_m_MCRichHits->begin();
  std::advance(ret_value, m_obj->data.m_MCRichHits_begin);
  return ret_value;
}

std::vector<ConstMCRichHit>::const_iterator ConstMCRichSegment::m_MCRichHits_end() const {
  auto ret_value = m_obj->m_m_MCRichHits->begin();
  std::advance(ret_value, m_obj->data.m_MCRichHits_end-1);
  return ++ret_value;
}

unsigned int ConstMCRichSegment::m_MCRichHits_size() const {
  return (m_obj->data.m_MCRichHits_end-m_obj->data.m_MCRichHits_begin);
}

ConstMCRichHit ConstMCRichSegment::m_MCRichHits(unsigned int index) const {
  if (m_MCRichHits_size() > index) {
    return m_obj->m_m_MCRichHits->at(m_obj->data.m_MCRichHits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<XYZPoint>::const_iterator ConstMCRichSegment::trajectoryPoints_begin() const {
  auto ret_value = m_obj->m_trajectoryPoints->begin();
  std::advance(ret_value, m_obj->data.trajectoryPoints_begin);
  return ret_value;
}

std::vector<XYZPoint>::const_iterator ConstMCRichSegment::trajectoryPoints_end() const {
  auto ret_value = m_obj->m_trajectoryPoints->begin();
  std::advance(ret_value, m_obj->data.trajectoryPoints_end-1);
  return ++ret_value;
}

unsigned int ConstMCRichSegment::trajectoryPoints_size() const {
  return (m_obj->data.trajectoryPoints_end-m_obj->data.trajectoryPoints_begin);
}

XYZPoint ConstMCRichSegment::trajectoryPoints(unsigned int index) const {
  if (trajectoryPoints_size() > index) {
    return m_obj->m_trajectoryPoints->at(m_obj->data.trajectoryPoints_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<XYZVector>::const_iterator ConstMCRichSegment::trajectoryMomenta_begin() const {
  auto ret_value = m_obj->m_trajectoryMomenta->begin();
  std::advance(ret_value, m_obj->data.trajectoryMomenta_begin);
  return ret_value;
}

std::vector<XYZVector>::const_iterator ConstMCRichSegment::trajectoryMomenta_end() const {
  auto ret_value = m_obj->m_trajectoryMomenta->begin();
  std::advance(ret_value, m_obj->data.trajectoryMomenta_end-1);
  return ++ret_value;
}

unsigned int ConstMCRichSegment::trajectoryMomenta_size() const {
  return (m_obj->data.trajectoryMomenta_end-m_obj->data.trajectoryMomenta_begin);
}

XYZVector ConstMCRichSegment::trajectoryMomenta(unsigned int index) const {
  if (trajectoryMomenta_size() > index) {
    return m_obj->m_trajectoryMomenta->at(m_obj->data.trajectoryMomenta_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCRichSegment::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCRichSegment::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCRichSegment::operator==(const MCRichSegment& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCRichSegment& p1, const MCRichSegment& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
