// standard includes
#include <stdexcept>


#include "MCRichDigitHistoryCodeCollection.h"

MCRichDigitHistoryCodeCollection::MCRichDigitHistoryCodeCollection() : m_collectionID(0), m_entries() ,m_refCollections(nullptr), m_data(new MCRichDigitHistoryCodeDataContainer() ) {
  
}

const MCRichDigitHistoryCode MCRichDigitHistoryCodeCollection::operator[](unsigned int index) const {
  return MCRichDigitHistoryCode(m_entries[index]);
}

const MCRichDigitHistoryCode MCRichDigitHistoryCodeCollection::at(unsigned int index) const {
  return MCRichDigitHistoryCode(m_entries.at(index));
}

int  MCRichDigitHistoryCodeCollection::size() const {
  return m_entries.size();
}

MCRichDigitHistoryCode MCRichDigitHistoryCodeCollection::create(){
  auto obj = new MCRichDigitHistoryCodeObj();
  m_entries.emplace_back(obj);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCRichDigitHistoryCode(obj);
}

void MCRichDigitHistoryCodeCollection::clear(){
  m_data->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCRichDigitHistoryCodeCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
  
  }
  
}

void MCRichDigitHistoryCodeCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCRichDigitHistoryCodeObj({index,m_collectionID}, data);
    
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCRichDigitHistoryCodeCollection::setReferences(const podio::ICollectionProvider* collectionProvider){


  return true; //TODO: check success
}

void MCRichDigitHistoryCodeCollection::push_back(ConstMCRichDigitHistoryCode object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
        
    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCRichDigitHistoryCodeCollection::setBuffer(void* address){
  m_data = static_cast<MCRichDigitHistoryCodeDataContainer*>(address);
}


const MCRichDigitHistoryCode MCRichDigitHistoryCodeCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCRichDigitHistoryCode* MCRichDigitHistoryCodeCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCRichDigitHistoryCodeCollectionIterator& MCRichDigitHistoryCodeCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
