//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCRichOpticalPhotonCollection_H
#define  MCRichOpticalPhotonCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCRichOpticalPhotonData.h"
#include "MCRichOpticalPhoton.h"
#include "MCRichOpticalPhotonObj.h"

typedef std::vector<MCRichOpticalPhotonData> MCRichOpticalPhotonDataContainer;
typedef std::deque<MCRichOpticalPhotonObj*> MCRichOpticalPhotonObjPointerContainer;

class MCRichOpticalPhotonCollectionIterator {

  public:
    MCRichOpticalPhotonCollectionIterator(int index, const MCRichOpticalPhotonObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCRichOpticalPhotonCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCRichOpticalPhoton operator*() const;
    const MCRichOpticalPhoton* operator->() const;
    const MCRichOpticalPhotonCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCRichOpticalPhoton m_object;
    const MCRichOpticalPhotonObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCRichOpticalPhotonCollection : public podio::CollectionBase {

public:
  typedef const MCRichOpticalPhotonCollectionIterator const_iterator;

  MCRichOpticalPhotonCollection();
//  MCRichOpticalPhotonCollection(const MCRichOpticalPhotonCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCRichOpticalPhotonCollection(MCRichOpticalPhotonVector* data, int collectionID);
  ~MCRichOpticalPhotonCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCRichOpticalPhoton create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCRichOpticalPhoton create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCRichOpticalPhoton operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCRichOpticalPhoton at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCRichOpticalPhoton object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCRichOpticalPhotonData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<XYZPoint,arraysize> m_pdIncidencePoint() const;
  template<size_t arraysize>  
  const std::array<XYZPoint,arraysize> m_sphericalMirrorReflectPoint() const;
  template<size_t arraysize>  
  const std::array<XYZPoint,arraysize> m_flatMirrorReflectPoint() const;
  template<size_t arraysize>  
  const std::array<XYZPoint,arraysize> m_aerogelExitPoint() const;
  template<size_t arraysize>  
  const std::array<float,arraysize> m_cherenkovTheta() const;
  template<size_t arraysize>  
  const std::array<float,arraysize> m_cherenkovPhi() const;
  template<size_t arraysize>  
  const std::array<XYZPoint,arraysize> m_emissionPoint() const;
  template<size_t arraysize>  
  const std::array<float,arraysize> m_energyAtProduction() const;
  template<size_t arraysize>  
  const std::array<XYZVector,arraysize> m_parentMomentum() const;
  template<size_t arraysize>  
  const std::array<XYZPoint,arraysize> m_hpdQWIncidencePoint() const;


private:
  int m_collectionID;
  MCRichOpticalPhotonObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCRichHit>* m_rel_m_mcRichHit; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCRichOpticalPhotonDataContainer* m_data;
};

template<typename... Args>
MCRichOpticalPhoton  MCRichOpticalPhotonCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCRichOpticalPhotonObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCRichOpticalPhoton(obj);
}

template<size_t arraysize>
const std::array<XYZPoint,arraysize> MCRichOpticalPhotonCollection::m_pdIncidencePoint() const {
  std::array<XYZPoint,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_pdIncidencePoint;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<XYZPoint,arraysize> MCRichOpticalPhotonCollection::m_sphericalMirrorReflectPoint() const {
  std::array<XYZPoint,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_sphericalMirrorReflectPoint;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<XYZPoint,arraysize> MCRichOpticalPhotonCollection::m_flatMirrorReflectPoint() const {
  std::array<XYZPoint,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_flatMirrorReflectPoint;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<XYZPoint,arraysize> MCRichOpticalPhotonCollection::m_aerogelExitPoint() const {
  std::array<XYZPoint,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_aerogelExitPoint;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<float,arraysize> MCRichOpticalPhotonCollection::m_cherenkovTheta() const {
  std::array<float,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_cherenkovTheta;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<float,arraysize> MCRichOpticalPhotonCollection::m_cherenkovPhi() const {
  std::array<float,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_cherenkovPhi;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<XYZPoint,arraysize> MCRichOpticalPhotonCollection::m_emissionPoint() const {
  std::array<XYZPoint,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_emissionPoint;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<float,arraysize> MCRichOpticalPhotonCollection::m_energyAtProduction() const {
  std::array<float,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_energyAtProduction;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<XYZVector,arraysize> MCRichOpticalPhotonCollection::m_parentMomentum() const {
  std::array<XYZVector,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_parentMomentum;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<XYZPoint,arraysize> MCRichOpticalPhotonCollection::m_hpdQWIncidencePoint() const {
  std::array<XYZPoint,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_hpdQWIncidencePoint;
 }
 return tmp;
}


#endif
