#ifndef MCHeader_H
#define MCHeader_H
#include "MCHeaderData.h"
#include <vector>
#include "MCVertex.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Event time
// author: P. Koppenburg, revised by G.Corti

//forward declarations
class MCHeaderCollection;
class MCHeaderCollectionIterator;
class ConstMCHeader;


#include "MCHeaderConst.h"
#include "MCHeaderObj.h"

class MCHeader {

  friend MCHeaderCollection;
  friend MCHeaderCollectionIterator;
  friend ConstMCHeader;

public:

  /// default constructor
  MCHeader();
    MCHeader(int m_evtNumber,unsigned m_evtTime);

  /// constructor from existing MCHeaderObj
  MCHeader(MCHeaderObj* obj);
  /// copy constructor
  MCHeader(const MCHeader& other);
  /// copy-assignment operator
  MCHeader& operator=(const MCHeader& other);
  /// support cloning (deep-copy)
  MCHeader clone() const;
  /// destructor
  ~MCHeader();

  /// conversion to const object
  operator ConstMCHeader () const;

public:

  const int& m_evtNumber() const;
  const unsigned& m_evtTime() const;

  void m_evtNumber(int value);
  void m_evtTime(unsigned value);

  void addm_primaryVertices(ConstMCVertex);
  unsigned int m_primaryVertices_size() const;
  ConstMCVertex m_primaryVertices(unsigned int) const;
  std::vector<ConstMCVertex>::const_iterator m_primaryVertices_begin() const;
  std::vector<ConstMCVertex>::const_iterator m_primaryVertices_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCHeaderObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCHeader& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCHeader& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCHeader& p1,
//       const MCHeader& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCHeaderObj* m_obj;

};

#endif
