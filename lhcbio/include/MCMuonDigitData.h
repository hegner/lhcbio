#ifndef MCMuonDigitDATA_H
#define MCMuonDigitDATA_H

// /< Firing Time of the hit
// author: Alessia Satta

#include "MCMuonDigitInfo.h"


class MCMuonDigitData {
public:
  MCMuonDigitInfo m_DigitInfo; ////< Packed information of the origin of the hit generating the digit,  the earliest, and the fate of the digit 
  double m_firingTime; ////< Firing Time of the hit 
  unsigned int HitsHistory_begin; 
  unsigned int HitsHistory_end; 
  unsigned int m_MCHits_begin; 
  unsigned int m_MCHits_end; 

};

#endif
