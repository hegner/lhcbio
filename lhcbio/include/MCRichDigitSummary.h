#ifndef MCRichDigitSummary_H
#define MCRichDigitSummary_H
#include "MCRichDigitSummaryData.h"
#include "MCRichDigitHistoryCode.h"
#include "RichSmartID.h"

#include <vector>
#include "podio/ObjectID.h"

// /< RichSmartID channel identifier
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichDigitSummaryCollection;
class MCRichDigitSummaryCollectionIterator;
class ConstMCRichDigitSummary;
class MCParticle;
class ConstMCParticle;


#include "MCRichDigitSummaryConst.h"
#include "MCRichDigitSummaryObj.h"

class MCRichDigitSummary {

  friend MCRichDigitSummaryCollection;
  friend MCRichDigitSummaryCollectionIterator;
  friend ConstMCRichDigitSummary;

public:

  /// default constructor
  MCRichDigitSummary();
    MCRichDigitSummary(MCRichDigitHistoryCode m_history,RichSmartID m_richSmartID);

  /// constructor from existing MCRichDigitSummaryObj
  MCRichDigitSummary(MCRichDigitSummaryObj* obj);
  /// copy constructor
  MCRichDigitSummary(const MCRichDigitSummary& other);
  /// copy-assignment operator
  MCRichDigitSummary& operator=(const MCRichDigitSummary& other);
  /// support cloning (deep-copy)
  MCRichDigitSummary clone() const;
  /// destructor
  ~MCRichDigitSummary();

  /// conversion to const object
  operator ConstMCRichDigitSummary () const;

public:

  const MCRichDigitHistoryCode& m_history() const;
  const RichSmartID& m_richSmartID() const;
  const ConstMCParticle m_MCParticle() const;

  MCRichDigitHistoryCode& m_history();
  void m_history(class MCRichDigitHistoryCode value);
  RichSmartID& m_richSmartID();
  void m_richSmartID(class RichSmartID value);
  void m_MCParticle(ConstMCParticle value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichDigitSummaryObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCRichDigitSummary& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCRichDigitSummary& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichDigitSummary& p1,
//       const MCRichDigitSummary& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichDigitSummaryObj* m_obj;

};

#endif
