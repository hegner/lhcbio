// datamodel specific includes
#include "MCHeader.h"
#include "MCHeaderConst.h"
#include "MCHeaderObj.h"
#include "MCHeaderData.h"
#include "MCHeaderCollection.h"
#include <iostream>

MCHeader::MCHeader() : m_obj(new MCHeaderObj()){
 m_obj->acquire();
};

MCHeader::MCHeader(int m_evtNumber,unsigned m_evtTime) : m_obj(new MCHeaderObj()){
 m_obj->acquire();
   m_obj->data.m_evtNumber = m_evtNumber;  m_obj->data.m_evtTime = m_evtTime;
};

MCHeader::MCHeader(const MCHeader& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCHeader& MCHeader::operator=(const MCHeader& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCHeader::MCHeader(MCHeaderObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCHeader MCHeader::clone() const {
  return {new MCHeaderObj(*m_obj)};
}

MCHeader::~MCHeader(){
  if ( m_obj != nullptr) m_obj->release();
}

MCHeader::operator ConstMCHeader() const {return ConstMCHeader(m_obj);};

  const int& MCHeader::m_evtNumber() const { return m_obj->data.m_evtNumber; };
  const unsigned& MCHeader::m_evtTime() const { return m_obj->data.m_evtTime; };

void MCHeader::m_evtNumber(int value){ m_obj->data.m_evtNumber = value;}
void MCHeader::m_evtTime(unsigned value){ m_obj->data.m_evtTime = value;}

std::vector<ConstMCVertex>::const_iterator MCHeader::m_primaryVertices_begin() const {
  auto ret_value = m_obj->m_m_primaryVertices->begin();
  std::advance(ret_value, m_obj->data.m_primaryVertices_begin);
  return ret_value;
}

std::vector<ConstMCVertex>::const_iterator MCHeader::m_primaryVertices_end() const {
  auto ret_value = m_obj->m_m_primaryVertices->begin();
  std::advance(ret_value, m_obj->data.m_primaryVertices_end-1);
  return ++ret_value;
}

void MCHeader::addm_primaryVertices(ConstMCVertex component) {
  m_obj->m_m_primaryVertices->push_back(component);
  m_obj->data.m_primaryVertices_end++;
}

unsigned int MCHeader::m_primaryVertices_size() const {
  return (m_obj->data.m_primaryVertices_end-m_obj->data.m_primaryVertices_begin);
}

ConstMCVertex MCHeader::m_primaryVertices(unsigned int index) const {
  if (m_primaryVertices_size() > index) {
    return m_obj->m_m_primaryVertices->at(m_obj->data.m_primaryVertices_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCHeader::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCHeader::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCHeader::operator==(const ConstMCHeader& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCHeader& p1, const MCHeader& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
