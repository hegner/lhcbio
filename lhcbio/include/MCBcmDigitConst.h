#ifndef ConstMCBcmDigit_H
#define ConstMCBcmDigit_H
#include "MCBcmDigitData.h"
#include <vector>

#include <vector>
#include "podio/ObjectID.h"

// BCM MC digit class
// author: M Lieng

//forward declarations
class MCBcmDigitObj;
class MCBcmDigit;
class MCBcmDigitCollection;
class MCBcmDigitCollectionIterator;


#include "MCBcmDigitObj.h"

class ConstMCBcmDigit {

  friend MCBcmDigit;
  friend MCBcmDigitCollection;
  friend MCBcmDigitCollectionIterator;

public:

  /// default constructor
  ConstMCBcmDigit();
  
  /// constructor from existing MCBcmDigitObj
  ConstMCBcmDigit(MCBcmDigitObj* obj);
  /// copy constructor
  ConstMCBcmDigit(const ConstMCBcmDigit& other);
  /// copy-assignment operator
  ConstMCBcmDigit& operator=(const ConstMCBcmDigit& other);
  /// support cloning (deep-copy)
  ConstMCBcmDigit clone() const;
  /// destructor
  ~ConstMCBcmDigit();


public:


  unsigned int MCHitKeys_size() const;
  int MCHitKeys(unsigned int) const;
  std::vector<int>::const_iterator MCHitKeys_begin() const;
  std::vector<int>::const_iterator MCHitKeys_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCBcmDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCBcmDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCBcmDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCBcmDigit& p1,
//       const MCBcmDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCBcmDigitObj* m_obj;

};

#endif
