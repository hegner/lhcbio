#include "MCRichSegmentObj.h"
#include "MCParticleConst.h"
#include "MCRichTrackConst.h"


MCRichSegmentObj::MCRichSegmentObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_mcParticle(nullptr)
,m_m_MCRichTrack(nullptr)
,m_m_MCRichOpticalPhotons(new std::vector<ConstMCRichOpticalPhoton>()),m_m_MCRichHits(new std::vector<ConstMCRichHit>()),m_trajectoryPoints(new std::vector<XYZPoint>()),m_trajectoryMomenta(new std::vector<XYZVector>())
    { }

MCRichSegmentObj::MCRichSegmentObj(const podio::ObjectID id, MCRichSegmentData data) :
    ObjBase{id,0},
    data(data)
    { }

MCRichSegmentObj::MCRichSegmentObj(const MCRichSegmentObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_MCRichOpticalPhotons(new std::vector<ConstMCRichOpticalPhoton>(*(other.m_m_MCRichOpticalPhotons))),m_m_MCRichHits(new std::vector<ConstMCRichHit>(*(other.m_m_MCRichHits))),m_trajectoryPoints(new std::vector<XYZPoint>(*(other.m_trajectoryPoints))),m_trajectoryMomenta(new std::vector<XYZVector>(*(other.m_trajectoryMomenta)))
    { }

MCRichSegmentObj::~MCRichSegmentObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_mcParticle;
delete m_m_MCRichTrack;
delete m_m_MCRichOpticalPhotons;
delete m_m_MCRichHits;
delete m_trajectoryPoints;
delete m_trajectoryMomenta;

  }
}
