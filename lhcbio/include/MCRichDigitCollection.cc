// standard includes
#include <stdexcept>


#include "MCRichDigitCollection.h"

MCRichDigitCollection::MCRichDigitCollection() : m_collectionID(0), m_entries() ,m_refCollections(nullptr), m_data(new MCRichDigitDataContainer() ) {
  
}

const MCRichDigit MCRichDigitCollection::operator[](unsigned int index) const {
  return MCRichDigit(m_entries[index]);
}

const MCRichDigit MCRichDigitCollection::at(unsigned int index) const {
  return MCRichDigit(m_entries.at(index));
}

int  MCRichDigitCollection::size() const {
  return m_entries.size();
}

MCRichDigit MCRichDigitCollection::create(){
  auto obj = new MCRichDigitObj();
  m_entries.emplace_back(obj);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCRichDigit(obj);
}

void MCRichDigitCollection::clear(){
  m_data->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCRichDigitCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
  
  }
  
}

void MCRichDigitCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCRichDigitObj({index,m_collectionID}, data);
    
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCRichDigitCollection::setReferences(const podio::ICollectionProvider* collectionProvider){


  return true; //TODO: check success
}

void MCRichDigitCollection::push_back(ConstMCRichDigit object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
        
    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCRichDigitCollection::setBuffer(void* address){
  m_data = static_cast<MCRichDigitDataContainer*>(address);
}


const MCRichDigit MCRichDigitCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCRichDigit* MCRichDigitCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCRichDigitCollectionIterator& MCRichDigitCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
