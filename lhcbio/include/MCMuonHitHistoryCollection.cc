// standard includes
#include <stdexcept>


#include "MCMuonHitHistoryCollection.h"

MCMuonHitHistoryCollection::MCMuonHitHistoryCollection() : m_collectionID(0), m_entries() ,m_refCollections(nullptr), m_data(new MCMuonHitHistoryDataContainer() ) {
  
}

const MCMuonHitHistory MCMuonHitHistoryCollection::operator[](unsigned int index) const {
  return MCMuonHitHistory(m_entries[index]);
}

const MCMuonHitHistory MCMuonHitHistoryCollection::at(unsigned int index) const {
  return MCMuonHitHistory(m_entries.at(index));
}

int  MCMuonHitHistoryCollection::size() const {
  return m_entries.size();
}

MCMuonHitHistory MCMuonHitHistoryCollection::create(){
  auto obj = new MCMuonHitHistoryObj();
  m_entries.emplace_back(obj);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCMuonHitHistory(obj);
}

void MCMuonHitHistoryCollection::clear(){
  m_data->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCMuonHitHistoryCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
  
  }
  
}

void MCMuonHitHistoryCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCMuonHitHistoryObj({index,m_collectionID}, data);
    
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCMuonHitHistoryCollection::setReferences(const podio::ICollectionProvider* collectionProvider){


  return true; //TODO: check success
}

void MCMuonHitHistoryCollection::push_back(ConstMCMuonHitHistory object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
        
    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCMuonHitHistoryCollection::setBuffer(void* address){
  m_data = static_cast<MCMuonHitHistoryDataContainer*>(address);
}


const MCMuonHitHistory MCMuonHitHistoryCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCMuonHitHistory* MCMuonHitHistoryCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCMuonHitHistoryCollectionIterator& MCMuonHitHistoryCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
