#ifndef MCSTDigitDATA_H
#define MCSTDigitDATA_H

// This class turns represents total charge deposited on strip in an event
// author: Matthew Needham



class MCSTDigitData {
public:
  unsigned int m_mcDeposit_begin; 
  unsigned int m_mcDeposit_end; 

};

#endif
