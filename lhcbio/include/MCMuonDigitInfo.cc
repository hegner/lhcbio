// datamodel specific includes
#include "MCMuonDigitInfo.h"
#include "MCMuonDigitInfoConst.h"
#include "MCMuonDigitInfoObj.h"
#include "MCMuonDigitInfoData.h"
#include "MCMuonDigitInfoCollection.h"
#include <iostream>

MCMuonDigitInfo::MCMuonDigitInfo() : m_obj(new MCMuonDigitInfoObj()){
 m_obj->acquire();
};

MCMuonDigitInfo::MCMuonDigitInfo(unsigned m_DigitInfo) : m_obj(new MCMuonDigitInfoObj()){
 m_obj->acquire();
   m_obj->data.m_DigitInfo = m_DigitInfo;
};

MCMuonDigitInfo::MCMuonDigitInfo(const MCMuonDigitInfo& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCMuonDigitInfo& MCMuonDigitInfo::operator=(const MCMuonDigitInfo& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCMuonDigitInfo::MCMuonDigitInfo(MCMuonDigitInfoObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCMuonDigitInfo MCMuonDigitInfo::clone() const {
  return {new MCMuonDigitInfoObj(*m_obj)};
}

MCMuonDigitInfo::~MCMuonDigitInfo(){
  if ( m_obj != nullptr) m_obj->release();
}

MCMuonDigitInfo::operator ConstMCMuonDigitInfo() const {return ConstMCMuonDigitInfo(m_obj);};

  const unsigned& MCMuonDigitInfo::m_DigitInfo() const { return m_obj->data.m_DigitInfo; };

void MCMuonDigitInfo::m_DigitInfo(unsigned value){ m_obj->data.m_DigitInfo = value;}


bool  MCMuonDigitInfo::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCMuonDigitInfo::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCMuonDigitInfo::operator==(const ConstMCMuonDigitInfo& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCMuonDigitInfo& p1, const MCMuonDigitInfo& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
