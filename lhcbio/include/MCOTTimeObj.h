#ifndef MCOTTimeOBJ_H
#define MCOTTimeOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCOTTimeData.h"

#include <vector>
#include "MCOTDeposit.h"


// forward declarations
class MCOTTime;
class ConstMCOTTime;


class MCOTTimeObj : public podio::ObjBase {
public:
  /// constructor
  MCOTTimeObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCOTTimeObj(const MCOTTimeObj&);
  /// constructor from ObjectID and MCOTTimeData
  /// does not initialize the internal relation containers
  MCOTTimeObj(const podio::ObjectID id, MCOTTimeData data);
  virtual ~MCOTTimeObj();

public:
  MCOTTimeData data;
  std::vector<ConstMCOTDeposit>* m_m_deposits;


};


#endif
