// datamodel specific includes
#include "MCVertex.h"
#include "MCVertexConst.h"
#include "MCVertexObj.h"
#include "MCVertexData.h"
#include "MCVertexCollection.h"
#include <iostream>
#include "MCParticle.h"

ConstMCVertex::ConstMCVertex() : m_obj(new MCVertexObj()){
 m_obj->acquire();
};

ConstMCVertex::ConstMCVertex(XYZPoint m_position,double m_time,MCVertexType m_type) : m_obj(new MCVertexObj()){
 m_obj->acquire();
   m_obj->data.m_position = m_position;  m_obj->data.m_time = m_time;  m_obj->data.m_type = m_type;
};


ConstMCVertex::ConstMCVertex(const ConstMCVertex& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCVertex& ConstMCVertex::operator=(const ConstMCVertex& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCVertex::ConstMCVertex(MCVertexObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCVertex ConstMCVertex::clone() const {
  return {new MCVertexObj(*m_obj)};
}

ConstMCVertex::~ConstMCVertex(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCParticle ConstMCVertex::m_mother() const { if (m_obj->m_m_mother == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_mother));};

std::vector<ConstMCParticle>::const_iterator ConstMCVertex::m_products_begin() const {
  auto ret_value = m_obj->m_m_products->begin();
  std::advance(ret_value, m_obj->data.m_products_begin);
  return ret_value;
}

std::vector<ConstMCParticle>::const_iterator ConstMCVertex::m_products_end() const {
  auto ret_value = m_obj->m_m_products->begin();
  std::advance(ret_value, m_obj->data.m_products_end-1);
  return ++ret_value;
}

unsigned int ConstMCVertex::m_products_size() const {
  return (m_obj->data.m_products_end-m_obj->data.m_products_begin);
}

ConstMCParticle ConstMCVertex::m_products(unsigned int index) const {
  if (m_products_size() > index) {
    return m_obj->m_m_products->at(m_obj->data.m_products_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCVertex::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCVertex::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCVertex::operator==(const MCVertex& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCVertex& p1, const MCVertex& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
