// standard includes
#include <stdexcept>
#include "MCParticleCollection.h" 


#include "MCHitCollection.h"

MCHitCollection::MCHitCollection() : m_collectionID(0), m_entries() ,m_rel_m_MCParticle(new std::vector<ConstMCParticle>()),m_refCollections(nullptr), m_data(new MCHitDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCHit MCHitCollection::operator[](unsigned int index) const {
  return MCHit(m_entries[index]);
}

const MCHit MCHitCollection::at(unsigned int index) const {
  return MCHit(m_entries.at(index));
}

int  MCHitCollection::size() const {
  return m_entries.size();
}

MCHit MCHitCollection::create(){
  auto obj = new MCHitObj();
  m_entries.emplace_back(obj);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCHit(obj);
}

void MCHitCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  for (auto& item : (*m_rel_m_MCParticle)) {item.unlink(); }
  m_rel_m_MCParticle->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCHitCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
  
  }
    for (auto& obj : m_entries) {
if (obj->m_m_MCParticle != nullptr){
(*m_refCollections)[0]->emplace_back(obj->m_m_MCParticle->getObjectID());} else {(*m_refCollections)[0]->push_back({-2,-2}); } };

}

void MCHitCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCHitObj({index,m_collectionID}, data);
    
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCHitCollection::setReferences(const podio::ICollectionProvider* collectionProvider){

  for(unsigned int i=0, size=m_entries.size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    if (id.index != podio::ObjectID::invalid) {
      CollectionBase* coll = nullptr;
      collectionProvider->get(id.collectionID,coll);
      MCParticleCollection* tmp_coll = static_cast<MCParticleCollection*>(coll);
      m_entries[i]->m_m_MCParticle = new ConstMCParticle((*tmp_coll)[id.index]);
    } else {
      m_entries[i]->m_m_MCParticle = nullptr;
    }
  }

  return true; //TODO: check success
}

void MCHitCollection::push_back(ConstMCHit object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
        
    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCHitCollection::setBuffer(void* address){
  m_data = static_cast<MCHitDataContainer*>(address);
}


const MCHit MCHitCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCHit* MCHitCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCHitCollectionIterator& MCHitCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
