#ifndef MCParticle_H
#define MCParticle_H
#include "MCParticleData.h"
#include "LorentzVector.h"
#include "ParticleID.h"
#include <vector>
#include "MCVertex.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit-packed information on how this MCParticle was produced
// author: Gloria Corti, revised by P. Koppenburg

//forward declarations
class MCParticleCollection;
class MCParticleCollectionIterator;
class ConstMCParticle;
class MCVertex;
class ConstMCVertex;


#include "MCParticleConst.h"
#include "MCParticleObj.h"

class MCParticle {

  friend MCParticleCollection;
  friend MCParticleCollectionIterator;
  friend ConstMCParticle;

public:

  /// default constructor
  MCParticle();
    MCParticle(LorentzVector m_momentum,ParticleID m_particleID,unsigned m_flags);

  /// constructor from existing MCParticleObj
  MCParticle(MCParticleObj* obj);
  /// copy constructor
  MCParticle(const MCParticle& other);
  /// copy-assignment operator
  MCParticle& operator=(const MCParticle& other);
  /// support cloning (deep-copy)
  MCParticle clone() const;
  /// destructor
  ~MCParticle();

  /// conversion to const object
  operator ConstMCParticle () const;

public:

  const LorentzVector& m_momentum() const;
  const ParticleID& m_particleID() const;
  const unsigned& m_flags() const;
  const ConstMCVertex m_originVertex() const;

  LorentzVector& m_momentum();
  void m_momentum(class LorentzVector value);
  ParticleID& m_particleID();
  void m_particleID(class ParticleID value);
  void m_flags(unsigned value);
  void m_originVertex(ConstMCVertex value);

  void addm_endVertices(ConstMCVertex);
  unsigned int m_endVertices_size() const;
  ConstMCVertex m_endVertices(unsigned int) const;
  std::vector<ConstMCVertex>::const_iterator m_endVertices_begin() const;
  std::vector<ConstMCVertex>::const_iterator m_endVertices_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCParticleObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCParticle& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCParticle& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCParticle& p1,
//       const MCParticle& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCParticleObj* m_obj;

};

#endif
