// datamodel specific includes
#include "MCBcmDigit.h"
#include "MCBcmDigitConst.h"
#include "MCBcmDigitObj.h"
#include "MCBcmDigitData.h"
#include "MCBcmDigitCollection.h"
#include <iostream>

ConstMCBcmDigit::ConstMCBcmDigit() : m_obj(new MCBcmDigitObj()){
 m_obj->acquire();
};



ConstMCBcmDigit::ConstMCBcmDigit(const ConstMCBcmDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCBcmDigit& ConstMCBcmDigit::operator=(const ConstMCBcmDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCBcmDigit::ConstMCBcmDigit(MCBcmDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCBcmDigit ConstMCBcmDigit::clone() const {
  return {new MCBcmDigitObj(*m_obj)};
}

ConstMCBcmDigit::~ConstMCBcmDigit(){
  if ( m_obj != nullptr) m_obj->release();
}


std::vector<int>::const_iterator ConstMCBcmDigit::MCHitKeys_begin() const {
  auto ret_value = m_obj->m_MCHitKeys->begin();
  std::advance(ret_value, m_obj->data.MCHitKeys_begin);
  return ret_value;
}

std::vector<int>::const_iterator ConstMCBcmDigit::MCHitKeys_end() const {
  auto ret_value = m_obj->m_MCHitKeys->begin();
  std::advance(ret_value, m_obj->data.MCHitKeys_end-1);
  return ++ret_value;
}

unsigned int ConstMCBcmDigit::MCHitKeys_size() const {
  return (m_obj->data.MCHitKeys_end-m_obj->data.MCHitKeys_begin);
}

int ConstMCBcmDigit::MCHitKeys(unsigned int index) const {
  if (MCHitKeys_size() > index) {
    return m_obj->m_MCHitKeys->at(m_obj->data.MCHitKeys_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCBcmDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCBcmDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCBcmDigit::operator==(const MCBcmDigit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCBcmDigit& p1, const MCBcmDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
