#ifndef MCExtendedHit_H
#define MCExtendedHit_H
#include "MCExtendedHitData.h"
#include "XYZVector.h"

#include <vector>
#include "podio/ObjectID.h"

// /< momentum-vector of particle when entering the detector
// author: G.Corti

//forward declarations
class MCExtendedHitCollection;
class MCExtendedHitCollectionIterator;
class ConstMCExtendedHit;


#include "MCExtendedHitConst.h"
#include "MCExtendedHitObj.h"

class MCExtendedHit {

  friend MCExtendedHitCollection;
  friend MCExtendedHitCollectionIterator;
  friend ConstMCExtendedHit;

public:

  /// default constructor
  MCExtendedHit();
    MCExtendedHit(XYZVector m_momentum);

  /// constructor from existing MCExtendedHitObj
  MCExtendedHit(MCExtendedHitObj* obj);
  /// copy constructor
  MCExtendedHit(const MCExtendedHit& other);
  /// copy-assignment operator
  MCExtendedHit& operator=(const MCExtendedHit& other);
  /// support cloning (deep-copy)
  MCExtendedHit clone() const;
  /// destructor
  ~MCExtendedHit();

  /// conversion to const object
  operator ConstMCExtendedHit () const;

public:

  const XYZVector& m_momentum() const;

  XYZVector& m_momentum();
  void m_momentum(class XYZVector value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCExtendedHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCExtendedHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCExtendedHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCExtendedHit& p1,
//       const MCExtendedHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCExtendedHitObj* m_obj;

};

#endif
