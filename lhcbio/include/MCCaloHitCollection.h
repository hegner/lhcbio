//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCCaloHitCollection_H
#define  MCCaloHitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCCaloHitData.h"
#include "MCCaloHit.h"
#include "MCCaloHitObj.h"

typedef std::vector<MCCaloHitData> MCCaloHitDataContainer;
typedef std::deque<MCCaloHitObj*> MCCaloHitObjPointerContainer;

class MCCaloHitCollectionIterator {

  public:
    MCCaloHitCollectionIterator(int index, const MCCaloHitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCCaloHitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCCaloHit operator*() const;
    const MCCaloHit* operator->() const;
    const MCCaloHitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCCaloHit m_object;
    const MCCaloHitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCCaloHitCollection : public podio::CollectionBase {

public:
  typedef const MCCaloHitCollectionIterator const_iterator;

  MCCaloHitCollection();
//  MCCaloHitCollection(const MCCaloHitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCCaloHitCollection(MCCaloHitVector* data, int collectionID);
  ~MCCaloHitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCCaloHit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCCaloHit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCCaloHit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCCaloHit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCCaloHit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCCaloHitData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<double,arraysize> m_activeE() const;
  template<size_t arraysize>  
  const std::array<int,arraysize> m_sensDetID() const;
  template<size_t arraysize>  
  const std::array<Time,arraysize> m_time() const;


private:
  int m_collectionID;
  MCCaloHitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCParticle>* m_rel_m_particle; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCCaloHitDataContainer* m_data;
};

template<typename... Args>
MCCaloHit  MCCaloHitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCCaloHitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCCaloHit(obj);
}

template<size_t arraysize>
const std::array<double,arraysize> MCCaloHitCollection::m_activeE() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_activeE;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<int,arraysize> MCCaloHitCollection::m_sensDetID() const {
  std::array<int,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_sensDetID;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<Time,arraysize> MCCaloHitCollection::m_time() const {
  std::array<Time,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_time;
 }
 return tmp;
}


#endif
