#ifndef MCRichTrackOBJ_H
#define MCRichTrackOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCRichTrackData.h"

#include <vector>
#include "MCRichSegment.h"


// forward declarations
class MCRichTrack;
class ConstMCRichTrack;
class ConstMCParticle;


class MCRichTrackObj : public podio::ObjBase {
public:
  /// constructor
  MCRichTrackObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCRichTrackObj(const MCRichTrackObj&);
  /// constructor from ObjectID and MCRichTrackData
  /// does not initialize the internal relation containers
  MCRichTrackObj(const podio::ObjectID id, MCRichTrackData data);
  virtual ~MCRichTrackObj();

public:
  MCRichTrackData data;
  ConstMCParticle* m_m_mcParticle;
  std::vector<ConstMCRichSegment>* m_m_mcSegments;


};


#endif
