//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCExtendedHitCollection_H
#define  MCExtendedHitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCExtendedHitData.h"
#include "MCExtendedHit.h"
#include "MCExtendedHitObj.h"

typedef std::vector<MCExtendedHitData> MCExtendedHitDataContainer;
typedef std::deque<MCExtendedHitObj*> MCExtendedHitObjPointerContainer;

class MCExtendedHitCollectionIterator {

  public:
    MCExtendedHitCollectionIterator(int index, const MCExtendedHitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCExtendedHitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCExtendedHit operator*() const;
    const MCExtendedHit* operator->() const;
    const MCExtendedHitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCExtendedHit m_object;
    const MCExtendedHitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCExtendedHitCollection : public podio::CollectionBase {

public:
  typedef const MCExtendedHitCollectionIterator const_iterator;

  MCExtendedHitCollection();
//  MCExtendedHitCollection(const MCExtendedHitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCExtendedHitCollection(MCExtendedHitVector* data, int collectionID);
  ~MCExtendedHitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCExtendedHit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCExtendedHit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCExtendedHit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCExtendedHit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCExtendedHit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCExtendedHitData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<XYZVector,arraysize> m_momentum() const;


private:
  int m_collectionID;
  MCExtendedHitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCExtendedHitDataContainer* m_data;
};

template<typename... Args>
MCExtendedHit  MCExtendedHitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCExtendedHitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCExtendedHit(obj);
}

template<size_t arraysize>
const std::array<XYZVector,arraysize> MCExtendedHitCollection::m_momentum() const {
  std::array<XYZVector,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_momentum;
 }
 return tmp;
}


#endif
