#include "MCMuonDigitObj.h"


MCMuonDigitObj::MCMuonDigitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_MCHits(new std::vector<ConstMCHit>()),m_HitsHistory(new std::vector<MCMuonHitHistory>())
    { }

MCMuonDigitObj::MCMuonDigitObj(const podio::ObjectID id, MCMuonDigitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCMuonDigitObj::MCMuonDigitObj(const MCMuonDigitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_MCHits(new std::vector<ConstMCHit>(*(other.m_m_MCHits))),m_HitsHistory(new std::vector<MCMuonHitHistory>(*(other.m_HitsHistory)))
    { }

MCMuonDigitObj::~MCMuonDigitObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_MCHits;
delete m_HitsHistory;

  }
}
