#ifndef MCRichHit_H
#define MCRichHit_H
#include "MCRichHitData.h"
#include "XYZPoint.h"
#include "RichSmartID.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit packed field containing RICH specific information
// author: Chris Jones    Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichHitCollection;
class MCRichHitCollectionIterator;
class ConstMCRichHit;
class MCParticle;
class ConstMCParticle;


#include "MCRichHitConst.h"
#include "MCRichHitObj.h"

class MCRichHit {

  friend MCRichHitCollection;
  friend MCRichHitCollectionIterator;
  friend ConstMCRichHit;

public:

  /// default constructor
  MCRichHit();
    MCRichHit(XYZPoint m_entry,double m_energy,double m_timeOfFlight,RichSmartID m_sensDetID,unsigned m_historyCode);

  /// constructor from existing MCRichHitObj
  MCRichHit(MCRichHitObj* obj);
  /// copy constructor
  MCRichHit(const MCRichHit& other);
  /// copy-assignment operator
  MCRichHit& operator=(const MCRichHit& other);
  /// support cloning (deep-copy)
  MCRichHit clone() const;
  /// destructor
  ~MCRichHit();

  /// conversion to const object
  operator ConstMCRichHit () const;

public:

  const XYZPoint& m_entry() const;
  const double& m_energy() const;
  const double& m_timeOfFlight() const;
  const RichSmartID& m_sensDetID() const;
  const unsigned& m_historyCode() const;
  const ConstMCParticle m_MCParticle() const;

  XYZPoint& m_entry();
  void m_entry(class XYZPoint value);
  void m_energy(double value);
  void m_timeOfFlight(double value);
  RichSmartID& m_sensDetID();
  void m_sensDetID(class RichSmartID value);
  void m_historyCode(unsigned value);
  void m_MCParticle(ConstMCParticle value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCRichHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCRichHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichHit& p1,
//       const MCRichHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichHitObj* m_obj;

};

#endif
