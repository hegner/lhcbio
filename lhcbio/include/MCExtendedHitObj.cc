#include "MCExtendedHitObj.h"


MCExtendedHitObj::MCExtendedHitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    
    { }

MCExtendedHitObj::MCExtendedHitObj(const podio::ObjectID id, MCExtendedHitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCExtendedHitObj::MCExtendedHitObj(const MCExtendedHitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCExtendedHitObj::~MCExtendedHitObj() {
  if (id.index == podio::ObjectID::untracked) {

  }
}
