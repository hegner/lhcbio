#include "MCRichOpticalPhotonObj.h"
#include "MCRichHitConst.h"


MCRichOpticalPhotonObj::MCRichOpticalPhotonObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_mcRichHit(nullptr)

    { }

MCRichOpticalPhotonObj::MCRichOpticalPhotonObj(const podio::ObjectID id, MCRichOpticalPhotonData data) :
    ObjBase{id,0},
    data(data)
    { }

MCRichOpticalPhotonObj::MCRichOpticalPhotonObj(const MCRichOpticalPhotonObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCRichOpticalPhotonObj::~MCRichOpticalPhotonObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_mcRichHit;

  }
}
