// datamodel specific includes
#include "MCRichDigit.h"
#include "MCRichDigitConst.h"
#include "MCRichDigitObj.h"
#include "MCRichDigitData.h"
#include "MCRichDigitCollection.h"
#include <iostream>

MCRichDigit::MCRichDigit() : m_obj(new MCRichDigitObj()){
 m_obj->acquire();
};

MCRichDigit::MCRichDigit(int m_hits,MCRichDigitHistoryCode m_history) : m_obj(new MCRichDigitObj()){
 m_obj->acquire();
   m_obj->data.m_hits = m_hits;  m_obj->data.m_history = m_history;
};

MCRichDigit::MCRichDigit(const MCRichDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCRichDigit& MCRichDigit::operator=(const MCRichDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCRichDigit::MCRichDigit(MCRichDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCRichDigit MCRichDigit::clone() const {
  return {new MCRichDigitObj(*m_obj)};
}

MCRichDigit::~MCRichDigit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCRichDigit::operator ConstMCRichDigit() const {return ConstMCRichDigit(m_obj);};

  const int& MCRichDigit::m_hits() const { return m_obj->data.m_hits; };
  const MCRichDigitHistoryCode& MCRichDigit::m_history() const { return m_obj->data.m_history; };

void MCRichDigit::m_hits(int value){ m_obj->data.m_hits = value;}
  MCRichDigitHistoryCode& MCRichDigit::m_history() { return m_obj->data.m_history; };
void MCRichDigit::m_history(class MCRichDigitHistoryCode value){ m_obj->data.m_history = value;}


bool  MCRichDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCRichDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCRichDigit::operator==(const ConstMCRichDigit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCRichDigit& p1, const MCRichDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
