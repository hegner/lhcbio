// standard includes
#include <stdexcept>
#include "MCParticleCollection.h" 


#include "MCSensPlaneHitCollection.h"

MCSensPlaneHitCollection::MCSensPlaneHitCollection() : m_collectionID(0), m_entries() ,m_rel_m_particle(new std::vector<ConstMCParticle>()),m_refCollections(nullptr), m_data(new MCSensPlaneHitDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCSensPlaneHit MCSensPlaneHitCollection::operator[](unsigned int index) const {
  return MCSensPlaneHit(m_entries[index]);
}

const MCSensPlaneHit MCSensPlaneHitCollection::at(unsigned int index) const {
  return MCSensPlaneHit(m_entries.at(index));
}

int  MCSensPlaneHitCollection::size() const {
  return m_entries.size();
}

MCSensPlaneHit MCSensPlaneHitCollection::create(){
  auto obj = new MCSensPlaneHitObj();
  m_entries.emplace_back(obj);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCSensPlaneHit(obj);
}

void MCSensPlaneHitCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  for (auto& item : (*m_rel_m_particle)) {item.unlink(); }
  m_rel_m_particle->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCSensPlaneHitCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
  
  }
    for (auto& obj : m_entries) {
if (obj->m_m_particle != nullptr){
(*m_refCollections)[0]->emplace_back(obj->m_m_particle->getObjectID());} else {(*m_refCollections)[0]->push_back({-2,-2}); } };

}

void MCSensPlaneHitCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCSensPlaneHitObj({index,m_collectionID}, data);
    
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCSensPlaneHitCollection::setReferences(const podio::ICollectionProvider* collectionProvider){

  for(unsigned int i=0, size=m_entries.size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    if (id.index != podio::ObjectID::invalid) {
      CollectionBase* coll = nullptr;
      collectionProvider->get(id.collectionID,coll);
      MCParticleCollection* tmp_coll = static_cast<MCParticleCollection*>(coll);
      m_entries[i]->m_m_particle = new ConstMCParticle((*tmp_coll)[id.index]);
    } else {
      m_entries[i]->m_m_particle = nullptr;
    }
  }

  return true; //TODO: check success
}

void MCSensPlaneHitCollection::push_back(ConstMCSensPlaneHit object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
        
    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCSensPlaneHitCollection::setBuffer(void* address){
  m_data = static_cast<MCSensPlaneHitDataContainer*>(address);
}


const MCSensPlaneHit MCSensPlaneHitCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCSensPlaneHit* MCSensPlaneHitCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCSensPlaneHitCollectionIterator& MCSensPlaneHitCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
