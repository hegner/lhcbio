// datamodel specific includes
#include "MCSensPlaneHit.h"
#include "MCSensPlaneHitConst.h"
#include "MCSensPlaneHitObj.h"
#include "MCSensPlaneHitData.h"
#include "MCSensPlaneHitCollection.h"
#include <iostream>
#include "MCParticle.h"

MCSensPlaneHit::MCSensPlaneHit() : m_obj(new MCSensPlaneHitObj()){
 m_obj->acquire();
};

MCSensPlaneHit::MCSensPlaneHit(LorentzVector m_position,LorentzVector m_momentum,int m_type) : m_obj(new MCSensPlaneHitObj()){
 m_obj->acquire();
   m_obj->data.m_position = m_position;  m_obj->data.m_momentum = m_momentum;  m_obj->data.m_type = m_type;
};

MCSensPlaneHit::MCSensPlaneHit(const MCSensPlaneHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCSensPlaneHit& MCSensPlaneHit::operator=(const MCSensPlaneHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCSensPlaneHit::MCSensPlaneHit(MCSensPlaneHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCSensPlaneHit MCSensPlaneHit::clone() const {
  return {new MCSensPlaneHitObj(*m_obj)};
}

MCSensPlaneHit::~MCSensPlaneHit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCSensPlaneHit::operator ConstMCSensPlaneHit() const {return ConstMCSensPlaneHit(m_obj);};

  const LorentzVector& MCSensPlaneHit::m_position() const { return m_obj->data.m_position; };
  const LorentzVector& MCSensPlaneHit::m_momentum() const { return m_obj->data.m_momentum; };
  const int& MCSensPlaneHit::m_type() const { return m_obj->data.m_type; };
  const ConstMCParticle MCSensPlaneHit::m_particle() const { if (m_obj->m_m_particle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_particle));};

  LorentzVector& MCSensPlaneHit::m_position() { return m_obj->data.m_position; };
void MCSensPlaneHit::m_position(class LorentzVector value){ m_obj->data.m_position = value;}
  LorentzVector& MCSensPlaneHit::m_momentum() { return m_obj->data.m_momentum; };
void MCSensPlaneHit::m_momentum(class LorentzVector value){ m_obj->data.m_momentum = value;}
void MCSensPlaneHit::m_type(int value){ m_obj->data.m_type = value;}
void MCSensPlaneHit::m_particle(ConstMCParticle value) { if (m_obj->m_m_particle != nullptr) delete m_obj->m_m_particle; m_obj->m_m_particle = new ConstMCParticle(value); };


bool  MCSensPlaneHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCSensPlaneHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCSensPlaneHit::operator==(const ConstMCSensPlaneHit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCSensPlaneHit& p1, const MCSensPlaneHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
