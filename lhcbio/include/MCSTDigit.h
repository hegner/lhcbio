#ifndef MCSTDigit_H
#define MCSTDigit_H
#include "MCSTDigitData.h"
#include <vector>
#include "MCSTDeposit.h"

#include <vector>
#include "podio/ObjectID.h"

// This class turns represents total charge deposited on strip in an event
// author: Matthew Needham

//forward declarations
class MCSTDigitCollection;
class MCSTDigitCollectionIterator;
class ConstMCSTDigit;


#include "MCSTDigitConst.h"
#include "MCSTDigitObj.h"

class MCSTDigit {

  friend MCSTDigitCollection;
  friend MCSTDigitCollectionIterator;
  friend ConstMCSTDigit;

public:

  /// default constructor
  MCSTDigit();
  
  /// constructor from existing MCSTDigitObj
  MCSTDigit(MCSTDigitObj* obj);
  /// copy constructor
  MCSTDigit(const MCSTDigit& other);
  /// copy-assignment operator
  MCSTDigit& operator=(const MCSTDigit& other);
  /// support cloning (deep-copy)
  MCSTDigit clone() const;
  /// destructor
  ~MCSTDigit();

  /// conversion to const object
  operator ConstMCSTDigit () const;

public:



  void addm_mcDeposit(ConstMCSTDeposit);
  unsigned int m_mcDeposit_size() const;
  ConstMCSTDeposit m_mcDeposit(unsigned int) const;
  std::vector<ConstMCSTDeposit>::const_iterator m_mcDeposit_begin() const;
  std::vector<ConstMCSTDeposit>::const_iterator m_mcDeposit_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCSTDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCSTDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCSTDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCSTDigit& p1,
//       const MCSTDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCSTDigitObj* m_obj;

};

#endif
