// datamodel specific includes
#include "MCMuonDigit.h"
#include "MCMuonDigitConst.h"
#include "MCMuonDigitObj.h"
#include "MCMuonDigitData.h"
#include "MCMuonDigitCollection.h"
#include <iostream>

ConstMCMuonDigit::ConstMCMuonDigit() : m_obj(new MCMuonDigitObj()){
 m_obj->acquire();
};

ConstMCMuonDigit::ConstMCMuonDigit(MCMuonDigitInfo m_DigitInfo,double m_firingTime) : m_obj(new MCMuonDigitObj()){
 m_obj->acquire();
   m_obj->data.m_DigitInfo = m_DigitInfo;  m_obj->data.m_firingTime = m_firingTime;
};


ConstMCMuonDigit::ConstMCMuonDigit(const ConstMCMuonDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCMuonDigit& ConstMCMuonDigit::operator=(const ConstMCMuonDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCMuonDigit::ConstMCMuonDigit(MCMuonDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCMuonDigit ConstMCMuonDigit::clone() const {
  return {new MCMuonDigitObj(*m_obj)};
}

ConstMCMuonDigit::~ConstMCMuonDigit(){
  if ( m_obj != nullptr) m_obj->release();
}


std::vector<ConstMCHit>::const_iterator ConstMCMuonDigit::m_MCHits_begin() const {
  auto ret_value = m_obj->m_m_MCHits->begin();
  std::advance(ret_value, m_obj->data.m_MCHits_begin);
  return ret_value;
}

std::vector<ConstMCHit>::const_iterator ConstMCMuonDigit::m_MCHits_end() const {
  auto ret_value = m_obj->m_m_MCHits->begin();
  std::advance(ret_value, m_obj->data.m_MCHits_end-1);
  return ++ret_value;
}

unsigned int ConstMCMuonDigit::m_MCHits_size() const {
  return (m_obj->data.m_MCHits_end-m_obj->data.m_MCHits_begin);
}

ConstMCHit ConstMCMuonDigit::m_MCHits(unsigned int index) const {
  if (m_MCHits_size() > index) {
    return m_obj->m_m_MCHits->at(m_obj->data.m_MCHits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<MCMuonHitHistory>::const_iterator ConstMCMuonDigit::HitsHistory_begin() const {
  auto ret_value = m_obj->m_HitsHistory->begin();
  std::advance(ret_value, m_obj->data.HitsHistory_begin);
  return ret_value;
}

std::vector<MCMuonHitHistory>::const_iterator ConstMCMuonDigit::HitsHistory_end() const {
  auto ret_value = m_obj->m_HitsHistory->begin();
  std::advance(ret_value, m_obj->data.HitsHistory_end-1);
  return ++ret_value;
}

unsigned int ConstMCMuonDigit::HitsHistory_size() const {
  return (m_obj->data.HitsHistory_end-m_obj->data.HitsHistory_begin);
}

MCMuonHitHistory ConstMCMuonDigit::HitsHistory(unsigned int index) const {
  if (HitsHistory_size() > index) {
    return m_obj->m_HitsHistory->at(m_obj->data.HitsHistory_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCMuonDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCMuonDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCMuonDigit::operator==(const MCMuonDigit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCMuonDigit& p1, const MCMuonDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
