#ifndef MCSTDepositOBJ_H
#define MCSTDepositOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCSTDepositData.h"



// forward declarations
class MCSTDeposit;
class ConstMCSTDeposit;
class ConstMCHit;


class MCSTDepositObj : public podio::ObjBase {
public:
  /// constructor
  MCSTDepositObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCSTDepositObj(const MCSTDepositObj&);
  /// constructor from ObjectID and MCSTDepositData
  /// does not initialize the internal relation containers
  MCSTDepositObj(const podio::ObjectID id, MCSTDepositData data);
  virtual ~MCSTDepositObj();

public:
  MCSTDepositData data;
  ConstMCHit* m_m_mcHit;


};


#endif
