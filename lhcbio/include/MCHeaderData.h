#ifndef MCHeaderDATA_H
#define MCHeaderDATA_H

// /< Event time
// author: P. Koppenburg, revised by G.Corti



class MCHeaderData {
public:
  int m_evtNumber; ////< Event number 
  unsigned m_evtTime; ////< Event time 
  unsigned int m_primaryVertices_begin; 
  unsigned int m_primaryVertices_end; 

};

#endif
