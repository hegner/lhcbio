#ifndef MCOTTime_H
#define MCOTTime_H
#include "MCOTTimeData.h"
#include <vector>
#include "MCOTDeposit.h"

#include <vector>
#include "podio/ObjectID.h"

// MCOTTime are constucted from the MCOTDeposits
// author: Jeroen van Tilburg and Jacopo Nardulli

//forward declarations
class MCOTTimeCollection;
class MCOTTimeCollectionIterator;
class ConstMCOTTime;


#include "MCOTTimeConst.h"
#include "MCOTTimeObj.h"

class MCOTTime {

  friend MCOTTimeCollection;
  friend MCOTTimeCollectionIterator;
  friend ConstMCOTTime;

public:

  /// default constructor
  MCOTTime();
  
  /// constructor from existing MCOTTimeObj
  MCOTTime(MCOTTimeObj* obj);
  /// copy constructor
  MCOTTime(const MCOTTime& other);
  /// copy-assignment operator
  MCOTTime& operator=(const MCOTTime& other);
  /// support cloning (deep-copy)
  MCOTTime clone() const;
  /// destructor
  ~MCOTTime();

  /// conversion to const object
  operator ConstMCOTTime () const;

public:



  void addm_deposits(ConstMCOTDeposit);
  unsigned int m_deposits_size() const;
  ConstMCOTDeposit m_deposits(unsigned int) const;
  std::vector<ConstMCOTDeposit>::const_iterator m_deposits_begin() const;
  std::vector<ConstMCOTDeposit>::const_iterator m_deposits_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCOTTimeObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCOTTime& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCOTTime& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCOTTime& p1,
//       const MCOTTime& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCOTTimeObj* m_obj;

};

#endif
