#include "MCParticleObj.h"
#include "MCVertexConst.h"


MCParticleObj::MCParticleObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_originVertex(nullptr)
,m_m_endVertices(new std::vector<ConstMCVertex>())
    { }

MCParticleObj::MCParticleObj(const podio::ObjectID id, MCParticleData data) :
    ObjBase{id,0},
    data(data)
    { }

MCParticleObj::MCParticleObj(const MCParticleObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_endVertices(new std::vector<ConstMCVertex>(*(other.m_m_endVertices)))
    { }

MCParticleObj::~MCParticleObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_originVertex;
delete m_m_endVertices;

  }
}
