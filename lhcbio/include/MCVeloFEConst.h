#ifndef ConstMCVeloFE_H
#define ConstMCVeloFE_H
#include "MCVeloFEData.h"
#include <vector>
#include "MCHit.h"
#include <vector>

#include <vector>
#include "podio/ObjectID.h"

// /< common mode noise added in Si/FE chip simulation in units of electrons
// author: Chris Parkes, update Tomasz Szumlak

//forward declarations
class MCVeloFEObj;
class MCVeloFE;
class MCVeloFECollection;
class MCVeloFECollectionIterator;


#include "MCVeloFEObj.h"

class ConstMCVeloFE {

  friend MCVeloFE;
  friend MCVeloFECollection;
  friend MCVeloFECollectionIterator;

public:

  /// default constructor
  ConstMCVeloFE();
  ConstMCVeloFE(double m_addedSignal,double m_addedPedestal,double m_addedNoise,double m_addedCMNoise);

  /// constructor from existing MCVeloFEObj
  ConstMCVeloFE(MCVeloFEObj* obj);
  /// copy constructor
  ConstMCVeloFE(const ConstMCVeloFE& other);
  /// copy-assignment operator
  ConstMCVeloFE& operator=(const ConstMCVeloFE& other);
  /// support cloning (deep-copy)
  ConstMCVeloFE clone() const;
  /// destructor
  ~ConstMCVeloFE();


public:

  const double& m_addedSignal() const;
  const double& m_addedPedestal() const;
  const double& m_addedNoise() const;
  const double& m_addedCMNoise() const;

  unsigned int m_MCHits_size() const;
  ConstMCHit m_MCHits(unsigned int) const;
  std::vector<ConstMCHit>::const_iterator m_MCHits_begin() const;
  std::vector<ConstMCHit>::const_iterator m_MCHits_end() const;
  unsigned int MCHitsCharge_size() const;
  double MCHitsCharge(unsigned int) const;
  std::vector<double>::const_iterator MCHitsCharge_begin() const;
  std::vector<double>::const_iterator MCHitsCharge_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCVeloFEObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCVeloFE& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCVeloFE& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCVeloFE& p1,
//       const MCVeloFE& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCVeloFEObj* m_obj;

};

#endif
