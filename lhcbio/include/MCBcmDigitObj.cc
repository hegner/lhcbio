#include "MCBcmDigitObj.h"


MCBcmDigitObj::MCBcmDigitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_MCHitKeys(new std::vector<int>())
    { }

MCBcmDigitObj::MCBcmDigitObj(const podio::ObjectID id, MCBcmDigitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCBcmDigitObj::MCBcmDigitObj(const MCBcmDigitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_MCHitKeys(new std::vector<int>(*(other.m_MCHitKeys)))
    { }

MCBcmDigitObj::~MCBcmDigitObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_MCHitKeys;

  }
}
