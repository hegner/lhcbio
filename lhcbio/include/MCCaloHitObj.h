#ifndef MCCaloHitOBJ_H
#define MCCaloHitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCCaloHitData.h"



// forward declarations
class MCCaloHit;
class ConstMCCaloHit;
class ConstMCParticle;


class MCCaloHitObj : public podio::ObjBase {
public:
  /// constructor
  MCCaloHitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCCaloHitObj(const MCCaloHitObj&);
  /// constructor from ObjectID and MCCaloHitData
  /// does not initialize the internal relation containers
  MCCaloHitObj(const podio::ObjectID id, MCCaloHitData data);
  virtual ~MCCaloHitObj();

public:
  MCCaloHitData data;
  ConstMCParticle* m_m_particle;


};


#endif
