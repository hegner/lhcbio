//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCOTDepositCollection_H
#define  MCOTDepositCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCOTDepositData.h"
#include "MCOTDeposit.h"
#include "MCOTDepositObj.h"

typedef std::vector<MCOTDepositData> MCOTDepositDataContainer;
typedef std::deque<MCOTDepositObj*> MCOTDepositObjPointerContainer;

class MCOTDepositCollectionIterator {

  public:
    MCOTDepositCollectionIterator(int index, const MCOTDepositObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCOTDepositCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCOTDeposit operator*() const;
    const MCOTDeposit* operator->() const;
    const MCOTDepositCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCOTDeposit m_object;
    const MCOTDepositObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCOTDepositCollection : public podio::CollectionBase {

public:
  typedef const MCOTDepositCollectionIterator const_iterator;

  MCOTDepositCollection();
//  MCOTDepositCollection(const MCOTDepositCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCOTDepositCollection(MCOTDepositVector* data, int collectionID);
  ~MCOTDepositCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCOTDeposit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCOTDeposit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCOTDeposit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCOTDeposit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCOTDeposit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCOTDepositData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<int,arraysize> m_type() const;
  template<size_t arraysize>  
  const std::array<OTChannelID,arraysize> m_channel() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_time() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_driftDistance() const;
  template<size_t arraysize>  
  const std::array<int,arraysize> m_ambiguity() const;


private:
  int m_collectionID;
  MCOTDepositObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCHit>* m_rel_m_mcHit; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCOTDepositDataContainer* m_data;
};

template<typename... Args>
MCOTDeposit  MCOTDepositCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCOTDepositObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCOTDeposit(obj);
}

template<size_t arraysize>
const std::array<int,arraysize> MCOTDepositCollection::m_type() const {
  std::array<int,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_type;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<OTChannelID,arraysize> MCOTDepositCollection::m_channel() const {
  std::array<OTChannelID,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_channel;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCOTDepositCollection::m_time() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_time;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCOTDepositCollection::m_driftDistance() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_driftDistance;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<int,arraysize> MCOTDepositCollection::m_ambiguity() const {
  std::array<int,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_ambiguity;
 }
 return tmp;
}


#endif
