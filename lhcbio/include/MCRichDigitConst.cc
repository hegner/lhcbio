// datamodel specific includes
#include "MCRichDigit.h"
#include "MCRichDigitConst.h"
#include "MCRichDigitObj.h"
#include "MCRichDigitData.h"
#include "MCRichDigitCollection.h"
#include <iostream>

ConstMCRichDigit::ConstMCRichDigit() : m_obj(new MCRichDigitObj()){
 m_obj->acquire();
};

ConstMCRichDigit::ConstMCRichDigit(int m_hits,MCRichDigitHistoryCode m_history) : m_obj(new MCRichDigitObj()){
 m_obj->acquire();
   m_obj->data.m_hits = m_hits;  m_obj->data.m_history = m_history;
};


ConstMCRichDigit::ConstMCRichDigit(const ConstMCRichDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCRichDigit& ConstMCRichDigit::operator=(const ConstMCRichDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCRichDigit::ConstMCRichDigit(MCRichDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCRichDigit ConstMCRichDigit::clone() const {
  return {new MCRichDigitObj(*m_obj)};
}

ConstMCRichDigit::~ConstMCRichDigit(){
  if ( m_obj != nullptr) m_obj->release();
}



bool  ConstMCRichDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCRichDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCRichDigit::operator==(const MCRichDigit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCRichDigit& p1, const MCRichDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
