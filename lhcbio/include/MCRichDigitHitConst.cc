// datamodel specific includes
#include "MCRichDigitHit.h"
#include "MCRichDigitHitConst.h"
#include "MCRichDigitHitObj.h"
#include "MCRichDigitHitData.h"
#include "MCRichDigitHitCollection.h"
#include <iostream>
#include "MCRichHit.h"

ConstMCRichDigitHit::ConstMCRichDigitHit() : m_obj(new MCRichDigitHitObj()){
 m_obj->acquire();
};

ConstMCRichDigitHit::ConstMCRichDigitHit(MCRichDigitHistoryCode m_history) : m_obj(new MCRichDigitHitObj()){
 m_obj->acquire();
   m_obj->data.m_history = m_history;
};


ConstMCRichDigitHit::ConstMCRichDigitHit(const ConstMCRichDigitHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCRichDigitHit& ConstMCRichDigitHit::operator=(const ConstMCRichDigitHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCRichDigitHit::ConstMCRichDigitHit(MCRichDigitHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCRichDigitHit ConstMCRichDigitHit::clone() const {
  return {new MCRichDigitHitObj(*m_obj)};
}

ConstMCRichDigitHit::~ConstMCRichDigitHit(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCRichHit ConstMCRichDigitHit::m_mcRichHit() const { if (m_obj->m_m_mcRichHit == nullptr) {
 return ConstMCRichHit(nullptr);}
 return ConstMCRichHit(*(m_obj->m_m_mcRichHit));};


bool  ConstMCRichDigitHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCRichDigitHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCRichDigitHit::operator==(const MCRichDigitHit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCRichDigitHit& p1, const MCRichDigitHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
