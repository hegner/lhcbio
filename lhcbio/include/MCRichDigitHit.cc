// datamodel specific includes
#include "MCRichDigitHit.h"
#include "MCRichDigitHitConst.h"
#include "MCRichDigitHitObj.h"
#include "MCRichDigitHitData.h"
#include "MCRichDigitHitCollection.h"
#include <iostream>
#include "MCRichHit.h"

MCRichDigitHit::MCRichDigitHit() : m_obj(new MCRichDigitHitObj()){
 m_obj->acquire();
};

MCRichDigitHit::MCRichDigitHit(MCRichDigitHistoryCode m_history) : m_obj(new MCRichDigitHitObj()){
 m_obj->acquire();
   m_obj->data.m_history = m_history;
};

MCRichDigitHit::MCRichDigitHit(const MCRichDigitHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCRichDigitHit& MCRichDigitHit::operator=(const MCRichDigitHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCRichDigitHit::MCRichDigitHit(MCRichDigitHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCRichDigitHit MCRichDigitHit::clone() const {
  return {new MCRichDigitHitObj(*m_obj)};
}

MCRichDigitHit::~MCRichDigitHit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCRichDigitHit::operator ConstMCRichDigitHit() const {return ConstMCRichDigitHit(m_obj);};

  const MCRichDigitHistoryCode& MCRichDigitHit::m_history() const { return m_obj->data.m_history; };
  const ConstMCRichHit MCRichDigitHit::m_mcRichHit() const { if (m_obj->m_m_mcRichHit == nullptr) {
 return ConstMCRichHit(nullptr);}
 return ConstMCRichHit(*(m_obj->m_m_mcRichHit));};

  MCRichDigitHistoryCode& MCRichDigitHit::m_history() { return m_obj->data.m_history; };
void MCRichDigitHit::m_history(class MCRichDigitHistoryCode value){ m_obj->data.m_history = value;}
void MCRichDigitHit::m_mcRichHit(ConstMCRichHit value) { if (m_obj->m_m_mcRichHit != nullptr) delete m_obj->m_m_mcRichHit; m_obj->m_m_mcRichHit = new ConstMCRichHit(value); };


bool  MCRichDigitHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCRichDigitHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCRichDigitHit::operator==(const ConstMCRichDigitHit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCRichDigitHit& p1, const MCRichDigitHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
