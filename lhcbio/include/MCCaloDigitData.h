#ifndef MCCaloDigitDATA_H
#define MCCaloDigitDATA_H

// /< Monte Carlo active energy deposition in the given cell
// author: Vanya Belyaev Ivan.Belyaev@itep.ru



class MCCaloDigitData {
public:
  double m_activeE; ////< Monte Carlo active energy deposition in the given cell 
  unsigned int m_hits_begin; 
  unsigned int m_hits_end; 

};

#endif
