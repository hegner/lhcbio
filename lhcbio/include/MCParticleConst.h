#ifndef ConstMCParticle_H
#define ConstMCParticle_H
#include "MCParticleData.h"
#include "LorentzVector.h"
#include "ParticleID.h"
#include <vector>
#include "MCVertex.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit-packed information on how this MCParticle was produced
// author: Gloria Corti, revised by P. Koppenburg

//forward declarations
class MCParticleObj;
class MCParticle;
class MCParticleCollection;
class MCParticleCollectionIterator;
class MCVertex;
class ConstMCVertex;


#include "MCParticleObj.h"

class ConstMCParticle {

  friend MCParticle;
  friend MCParticleCollection;
  friend MCParticleCollectionIterator;

public:

  /// default constructor
  ConstMCParticle();
  ConstMCParticle(LorentzVector m_momentum,ParticleID m_particleID,unsigned m_flags);

  /// constructor from existing MCParticleObj
  ConstMCParticle(MCParticleObj* obj);
  /// copy constructor
  ConstMCParticle(const ConstMCParticle& other);
  /// copy-assignment operator
  ConstMCParticle& operator=(const ConstMCParticle& other);
  /// support cloning (deep-copy)
  ConstMCParticle clone() const;
  /// destructor
  ~ConstMCParticle();


public:

  const LorentzVector& m_momentum() const;
  const ParticleID& m_particleID() const;
  const unsigned& m_flags() const;
  const ConstMCVertex m_originVertex() const;

  unsigned int m_endVertices_size() const;
  ConstMCVertex m_endVertices(unsigned int) const;
  std::vector<ConstMCVertex>::const_iterator m_endVertices_begin() const;
  std::vector<ConstMCVertex>::const_iterator m_endVertices_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCParticleObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCParticle& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCParticle& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCParticle& p1,
//       const MCParticle& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCParticleObj* m_obj;

};

#endif
