#ifndef ConstMCVPDigit_H
#define ConstMCVPDigit_H
#include "MCVPDigitData.h"
#include <vector>
#include "MCHit.h"
#include <vector>

#include <vector>
#include "podio/ObjectID.h"

// Charge deposits in a given pixel
// author: Marcin Kucharczyk

//forward declarations
class MCVPDigitObj;
class MCVPDigit;
class MCVPDigitCollection;
class MCVPDigitCollectionIterator;


#include "MCVPDigitObj.h"

class ConstMCVPDigit {

  friend MCVPDigit;
  friend MCVPDigitCollection;
  friend MCVPDigitCollectionIterator;

public:

  /// default constructor
  ConstMCVPDigit();
  
  /// constructor from existing MCVPDigitObj
  ConstMCVPDigit(MCVPDigitObj* obj);
  /// copy constructor
  ConstMCVPDigit(const ConstMCVPDigit& other);
  /// copy-assignment operator
  ConstMCVPDigit& operator=(const ConstMCVPDigit& other);
  /// support cloning (deep-copy)
  ConstMCVPDigit clone() const;
  /// destructor
  ~ConstMCVPDigit();


public:


  unsigned int m_mcHits_size() const;
  ConstMCHit m_mcHits(unsigned int) const;
  std::vector<ConstMCHit>::const_iterator m_mcHits_begin() const;
  std::vector<ConstMCHit>::const_iterator m_mcHits_end() const;
  unsigned int deposits_size() const;
  double deposits(unsigned int) const;
  std::vector<double>::const_iterator deposits_begin() const;
  std::vector<double>::const_iterator deposits_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCVPDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCVPDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCVPDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCVPDigit& p1,
//       const MCVPDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCVPDigitObj* m_obj;

};

#endif
