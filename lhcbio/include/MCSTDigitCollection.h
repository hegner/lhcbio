//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCSTDigitCollection_H
#define  MCSTDigitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCSTDigitData.h"
#include "MCSTDigit.h"
#include "MCSTDigitObj.h"

typedef std::vector<MCSTDigitData> MCSTDigitDataContainer;
typedef std::deque<MCSTDigitObj*> MCSTDigitObjPointerContainer;

class MCSTDigitCollectionIterator {

  public:
    MCSTDigitCollectionIterator(int index, const MCSTDigitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCSTDigitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCSTDigit operator*() const;
    const MCSTDigit* operator->() const;
    const MCSTDigitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCSTDigit m_object;
    const MCSTDigitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCSTDigitCollection : public podio::CollectionBase {

public:
  typedef const MCSTDigitCollectionIterator const_iterator;

  MCSTDigitCollection();
//  MCSTDigitCollection(const MCSTDigitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCSTDigitCollection(MCSTDigitVector* data, int collectionID);
  ~MCSTDigitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCSTDigit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCSTDigit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCSTDigit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCSTDigit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCSTDigit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCSTDigitData>* _getBuffer() { return m_data;};

   

private:
  int m_collectionID;
  MCSTDigitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCSTDeposit>* m_rel_m_mcDeposit; //relation buffer for r/w
  std::vector<std::vector<ConstMCSTDeposit>*> m_rel_m_mcDeposit_tmp;
 
  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCSTDigitDataContainer* m_data;
};

template<typename... Args>
MCSTDigit  MCSTDigitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCSTDigitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCSTDigit(obj);
}



#endif
