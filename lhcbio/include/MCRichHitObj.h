#ifndef MCRichHitOBJ_H
#define MCRichHitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCRichHitData.h"



// forward declarations
class MCRichHit;
class ConstMCRichHit;
class ConstMCParticle;


class MCRichHitObj : public podio::ObjBase {
public:
  /// constructor
  MCRichHitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCRichHitObj(const MCRichHitObj&);
  /// constructor from ObjectID and MCRichHitData
  /// does not initialize the internal relation containers
  MCRichHitObj(const podio::ObjectID id, MCRichHitData data);
  virtual ~MCRichHitObj();

public:
  MCRichHitData data;
  ConstMCParticle* m_m_MCParticle;


};


#endif
