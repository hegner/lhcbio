#ifndef MCVeloFEDATA_H
#define MCVeloFEDATA_H

// /< common mode noise added in Si/FE chip simulation in units of electrons
// author: Chris Parkes, update Tomasz Szumlak



class MCVeloFEData {
public:
  double m_addedSignal; ////< Added Signal in units of electrons 
  double m_addedPedestal; ////< pedestal added in Si/FE chip simulation in units of electrons 
  double m_addedNoise; ////< noise added in Si/FE chip simulation in units of electrons 
  double m_addedCMNoise; ////< common mode noise added in Si/FE chip simulation in units of electrons 
  unsigned int MCHitsCharge_begin; 
  unsigned int MCHitsCharge_end; 
  unsigned int m_MCHits_begin; 
  unsigned int m_MCHits_end; 

};

#endif
