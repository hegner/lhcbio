#ifndef MCRichDigitSummaryDATA_H
#define MCRichDigitSummaryDATA_H

// /< RichSmartID channel identifier
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

#include "MCRichDigitHistoryCode.h"
#include "RichSmartID.h"


class MCRichDigitSummaryData {
public:
  MCRichDigitHistoryCode m_history; ////< Bit-packed history information 
  RichSmartID m_richSmartID; ////< RichSmartID channel identifier 

};

#endif
