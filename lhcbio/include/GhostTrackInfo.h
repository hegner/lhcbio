#ifndef GhostTrackInfo_H
#define GhostTrackInfo_H
#include "GhostTrackInfoData.h"
#include "LinkMap.h"
#include "Classification.h"

#include <vector>
#include "podio/ObjectID.h"

// /<
// author: M. Needham

//forward declarations
class GhostTrackInfoCollection;
class GhostTrackInfoCollectionIterator;
class ConstGhostTrackInfo;


#include "GhostTrackInfoConst.h"
#include "GhostTrackInfoObj.h"

class GhostTrackInfo {

  friend GhostTrackInfoCollection;
  friend GhostTrackInfoCollectionIterator;
  friend ConstGhostTrackInfo;

public:

  /// default constructor
  GhostTrackInfo();
    GhostTrackInfo(LinkMap m_linkMap,Classification m_classification);

  /// constructor from existing GhostTrackInfoObj
  GhostTrackInfo(GhostTrackInfoObj* obj);
  /// copy constructor
  GhostTrackInfo(const GhostTrackInfo& other);
  /// copy-assignment operator
  GhostTrackInfo& operator=(const GhostTrackInfo& other);
  /// support cloning (deep-copy)
  GhostTrackInfo clone() const;
  /// destructor
  ~GhostTrackInfo();

  /// conversion to const object
  operator ConstGhostTrackInfo () const;

public:

  const LinkMap& m_linkMap() const;
  const Classification& m_classification() const;

  LinkMap& m_linkMap();
  void m_linkMap(class LinkMap value);
  Classification& m_classification();
  void m_classification(class Classification value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from GhostTrackInfoObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const GhostTrackInfo& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstGhostTrackInfo& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const GhostTrackInfo& p1,
//       const GhostTrackInfo& p2 );

  const podio::ObjectID getObjectID() const;

private:
  GhostTrackInfoObj* m_obj;

};

#endif
