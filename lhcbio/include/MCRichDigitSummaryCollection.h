//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCRichDigitSummaryCollection_H
#define  MCRichDigitSummaryCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCRichDigitSummaryData.h"
#include "MCRichDigitSummary.h"
#include "MCRichDigitSummaryObj.h"

typedef std::vector<MCRichDigitSummaryData> MCRichDigitSummaryDataContainer;
typedef std::deque<MCRichDigitSummaryObj*> MCRichDigitSummaryObjPointerContainer;

class MCRichDigitSummaryCollectionIterator {

  public:
    MCRichDigitSummaryCollectionIterator(int index, const MCRichDigitSummaryObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCRichDigitSummaryCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCRichDigitSummary operator*() const;
    const MCRichDigitSummary* operator->() const;
    const MCRichDigitSummaryCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCRichDigitSummary m_object;
    const MCRichDigitSummaryObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCRichDigitSummaryCollection : public podio::CollectionBase {

public:
  typedef const MCRichDigitSummaryCollectionIterator const_iterator;

  MCRichDigitSummaryCollection();
//  MCRichDigitSummaryCollection(const MCRichDigitSummaryCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCRichDigitSummaryCollection(MCRichDigitSummaryVector* data, int collectionID);
  ~MCRichDigitSummaryCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCRichDigitSummary create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCRichDigitSummary create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCRichDigitSummary operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCRichDigitSummary at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCRichDigitSummary object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCRichDigitSummaryData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<MCRichDigitHistoryCode,arraysize> m_history() const;
  template<size_t arraysize>  
  const std::array<RichSmartID,arraysize> m_richSmartID() const;


private:
  int m_collectionID;
  MCRichDigitSummaryObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCParticle>* m_rel_m_MCParticle; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCRichDigitSummaryDataContainer* m_data;
};

template<typename... Args>
MCRichDigitSummary  MCRichDigitSummaryCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCRichDigitSummaryObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCRichDigitSummary(obj);
}

template<size_t arraysize>
const std::array<MCRichDigitHistoryCode,arraysize> MCRichDigitSummaryCollection::m_history() const {
  std::array<MCRichDigitHistoryCode,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_history;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<RichSmartID,arraysize> MCRichDigitSummaryCollection::m_richSmartID() const {
  std::array<RichSmartID,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_richSmartID;
 }
 return tmp;
}


#endif
