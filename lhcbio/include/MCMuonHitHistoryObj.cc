#include "MCMuonHitHistoryObj.h"


MCMuonHitHistoryObj::MCMuonHitHistoryObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    
    { }

MCMuonHitHistoryObj::MCMuonHitHistoryObj(const podio::ObjectID id, MCMuonHitHistoryData data) :
    ObjBase{id,0},
    data(data)
    { }

MCMuonHitHistoryObj::MCMuonHitHistoryObj(const MCMuonHitHistoryObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCMuonHitHistoryObj::~MCMuonHitHistoryObj() {
  if (id.index == podio::ObjectID::untracked) {

  }
}
