// datamodel specific includes
#include "MCMuonDigitInfo.h"
#include "MCMuonDigitInfoConst.h"
#include "MCMuonDigitInfoObj.h"
#include "MCMuonDigitInfoData.h"
#include "MCMuonDigitInfoCollection.h"
#include <iostream>

ConstMCMuonDigitInfo::ConstMCMuonDigitInfo() : m_obj(new MCMuonDigitInfoObj()){
 m_obj->acquire();
};

ConstMCMuonDigitInfo::ConstMCMuonDigitInfo(unsigned m_DigitInfo) : m_obj(new MCMuonDigitInfoObj()){
 m_obj->acquire();
   m_obj->data.m_DigitInfo = m_DigitInfo;
};


ConstMCMuonDigitInfo::ConstMCMuonDigitInfo(const ConstMCMuonDigitInfo& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCMuonDigitInfo& ConstMCMuonDigitInfo::operator=(const ConstMCMuonDigitInfo& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCMuonDigitInfo::ConstMCMuonDigitInfo(MCMuonDigitInfoObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCMuonDigitInfo ConstMCMuonDigitInfo::clone() const {
  return {new MCMuonDigitInfoObj(*m_obj)};
}

ConstMCMuonDigitInfo::~ConstMCMuonDigitInfo(){
  if ( m_obj != nullptr) m_obj->release();
}



bool  ConstMCMuonDigitInfo::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCMuonDigitInfo::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCMuonDigitInfo::operator==(const MCMuonDigitInfo& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCMuonDigitInfo& p1, const MCMuonDigitInfo& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
