#ifndef ConstMCRichHit_H
#define ConstMCRichHit_H
#include "MCRichHitData.h"
#include "XYZPoint.h"
#include "RichSmartID.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit packed field containing RICH specific information
// author: Chris Jones    Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichHitObj;
class MCRichHit;
class MCRichHitCollection;
class MCRichHitCollectionIterator;
class MCParticle;
class ConstMCParticle;


#include "MCRichHitObj.h"

class ConstMCRichHit {

  friend MCRichHit;
  friend MCRichHitCollection;
  friend MCRichHitCollectionIterator;

public:

  /// default constructor
  ConstMCRichHit();
  ConstMCRichHit(XYZPoint m_entry,double m_energy,double m_timeOfFlight,RichSmartID m_sensDetID,unsigned m_historyCode);

  /// constructor from existing MCRichHitObj
  ConstMCRichHit(MCRichHitObj* obj);
  /// copy constructor
  ConstMCRichHit(const ConstMCRichHit& other);
  /// copy-assignment operator
  ConstMCRichHit& operator=(const ConstMCRichHit& other);
  /// support cloning (deep-copy)
  ConstMCRichHit clone() const;
  /// destructor
  ~ConstMCRichHit();


public:

  const XYZPoint& m_entry() const;
  const double& m_energy() const;
  const double& m_timeOfFlight() const;
  const RichSmartID& m_sensDetID() const;
  const unsigned& m_historyCode() const;
  const ConstMCParticle m_MCParticle() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCRichHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCRichHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichHit& p1,
//       const MCRichHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichHitObj* m_obj;

};

#endif
