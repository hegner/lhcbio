#include "MCOTTimeObj.h"


MCOTTimeObj::MCOTTimeObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_deposits(new std::vector<ConstMCOTDeposit>())
    { }

MCOTTimeObj::MCOTTimeObj(const podio::ObjectID id, MCOTTimeData data) :
    ObjBase{id,0},
    data(data)
    { }

MCOTTimeObj::MCOTTimeObj(const MCOTTimeObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_deposits(new std::vector<ConstMCOTDeposit>(*(other.m_m_deposits)))
    { }

MCOTTimeObj::~MCOTTimeObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_deposits;

  }
}
