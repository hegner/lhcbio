#ifndef ConstMCSTDigit_H
#define ConstMCSTDigit_H
#include "MCSTDigitData.h"
#include <vector>
#include "MCSTDeposit.h"

#include <vector>
#include "podio/ObjectID.h"

// This class turns represents total charge deposited on strip in an event
// author: Matthew Needham

//forward declarations
class MCSTDigitObj;
class MCSTDigit;
class MCSTDigitCollection;
class MCSTDigitCollectionIterator;


#include "MCSTDigitObj.h"

class ConstMCSTDigit {

  friend MCSTDigit;
  friend MCSTDigitCollection;
  friend MCSTDigitCollectionIterator;

public:

  /// default constructor
  ConstMCSTDigit();
  
  /// constructor from existing MCSTDigitObj
  ConstMCSTDigit(MCSTDigitObj* obj);
  /// copy constructor
  ConstMCSTDigit(const ConstMCSTDigit& other);
  /// copy-assignment operator
  ConstMCSTDigit& operator=(const ConstMCSTDigit& other);
  /// support cloning (deep-copy)
  ConstMCSTDigit clone() const;
  /// destructor
  ~ConstMCSTDigit();


public:


  unsigned int m_mcDeposit_size() const;
  ConstMCSTDeposit m_mcDeposit(unsigned int) const;
  std::vector<ConstMCSTDeposit>::const_iterator m_mcDeposit_begin() const;
  std::vector<ConstMCSTDeposit>::const_iterator m_mcDeposit_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCSTDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCSTDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCSTDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCSTDigit& p1,
//       const MCSTDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCSTDigitObj* m_obj;

};

#endif
