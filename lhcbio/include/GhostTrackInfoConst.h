#ifndef ConstGhostTrackInfo_H
#define ConstGhostTrackInfo_H
#include "GhostTrackInfoData.h"
#include "LinkMap.h"
#include "Classification.h"

#include <vector>
#include "podio/ObjectID.h"

// /<
// author: M. Needham

//forward declarations
class GhostTrackInfoObj;
class GhostTrackInfo;
class GhostTrackInfoCollection;
class GhostTrackInfoCollectionIterator;


#include "GhostTrackInfoObj.h"

class ConstGhostTrackInfo {

  friend GhostTrackInfo;
  friend GhostTrackInfoCollection;
  friend GhostTrackInfoCollectionIterator;

public:

  /// default constructor
  ConstGhostTrackInfo();
  ConstGhostTrackInfo(LinkMap m_linkMap,Classification m_classification);

  /// constructor from existing GhostTrackInfoObj
  ConstGhostTrackInfo(GhostTrackInfoObj* obj);
  /// copy constructor
  ConstGhostTrackInfo(const ConstGhostTrackInfo& other);
  /// copy-assignment operator
  ConstGhostTrackInfo& operator=(const ConstGhostTrackInfo& other);
  /// support cloning (deep-copy)
  ConstGhostTrackInfo clone() const;
  /// destructor
  ~ConstGhostTrackInfo();


public:

  const LinkMap& m_linkMap() const;
  const Classification& m_classification() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from GhostTrackInfoObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstGhostTrackInfo& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const GhostTrackInfo& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const GhostTrackInfo& p1,
//       const GhostTrackInfo& p2 );

  const podio::ObjectID getObjectID() const;

private:
  GhostTrackInfoObj* m_obj;

};

#endif
