// datamodel specific includes
#include "MCVPDigit.h"
#include "MCVPDigitConst.h"
#include "MCVPDigitObj.h"
#include "MCVPDigitData.h"
#include "MCVPDigitCollection.h"
#include <iostream>

ConstMCVPDigit::ConstMCVPDigit() : m_obj(new MCVPDigitObj()){
 m_obj->acquire();
};



ConstMCVPDigit::ConstMCVPDigit(const ConstMCVPDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCVPDigit& ConstMCVPDigit::operator=(const ConstMCVPDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCVPDigit::ConstMCVPDigit(MCVPDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCVPDigit ConstMCVPDigit::clone() const {
  return {new MCVPDigitObj(*m_obj)};
}

ConstMCVPDigit::~ConstMCVPDigit(){
  if ( m_obj != nullptr) m_obj->release();
}


std::vector<ConstMCHit>::const_iterator ConstMCVPDigit::m_mcHits_begin() const {
  auto ret_value = m_obj->m_m_mcHits->begin();
  std::advance(ret_value, m_obj->data.m_mcHits_begin);
  return ret_value;
}

std::vector<ConstMCHit>::const_iterator ConstMCVPDigit::m_mcHits_end() const {
  auto ret_value = m_obj->m_m_mcHits->begin();
  std::advance(ret_value, m_obj->data.m_mcHits_end-1);
  return ++ret_value;
}

unsigned int ConstMCVPDigit::m_mcHits_size() const {
  return (m_obj->data.m_mcHits_end-m_obj->data.m_mcHits_begin);
}

ConstMCHit ConstMCVPDigit::m_mcHits(unsigned int index) const {
  if (m_mcHits_size() > index) {
    return m_obj->m_m_mcHits->at(m_obj->data.m_mcHits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<double>::const_iterator ConstMCVPDigit::deposits_begin() const {
  auto ret_value = m_obj->m_deposits->begin();
  std::advance(ret_value, m_obj->data.deposits_begin);
  return ret_value;
}

std::vector<double>::const_iterator ConstMCVPDigit::deposits_end() const {
  auto ret_value = m_obj->m_deposits->begin();
  std::advance(ret_value, m_obj->data.deposits_end-1);
  return ++ret_value;
}

unsigned int ConstMCVPDigit::deposits_size() const {
  return (m_obj->data.deposits_end-m_obj->data.deposits_begin);
}

double ConstMCVPDigit::deposits(unsigned int index) const {
  if (deposits_size() > index) {
    return m_obj->m_deposits->at(m_obj->data.deposits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCVPDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCVPDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCVPDigit::operator==(const MCVPDigit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCVPDigit& p1, const MCVPDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
