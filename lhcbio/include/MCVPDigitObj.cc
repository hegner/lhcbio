#include "MCVPDigitObj.h"


MCVPDigitObj::MCVPDigitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_mcHits(new std::vector<ConstMCHit>()),m_deposits(new std::vector<double>())
    { }

MCVPDigitObj::MCVPDigitObj(const podio::ObjectID id, MCVPDigitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCVPDigitObj::MCVPDigitObj(const MCVPDigitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_mcHits(new std::vector<ConstMCHit>(*(other.m_m_mcHits))),m_deposits(new std::vector<double>(*(other.m_deposits)))
    { }

MCVPDigitObj::~MCVPDigitObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_mcHits;
delete m_deposits;

  }
}
