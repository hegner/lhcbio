//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCHeaderCollection_H
#define  MCHeaderCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCHeaderData.h"
#include "MCHeader.h"
#include "MCHeaderObj.h"

typedef std::vector<MCHeaderData> MCHeaderDataContainer;
typedef std::deque<MCHeaderObj*> MCHeaderObjPointerContainer;

class MCHeaderCollectionIterator {

  public:
    MCHeaderCollectionIterator(int index, const MCHeaderObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCHeaderCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCHeader operator*() const;
    const MCHeader* operator->() const;
    const MCHeaderCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCHeader m_object;
    const MCHeaderObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCHeaderCollection : public podio::CollectionBase {

public:
  typedef const MCHeaderCollectionIterator const_iterator;

  MCHeaderCollection();
//  MCHeaderCollection(const MCHeaderCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCHeaderCollection(MCHeaderVector* data, int collectionID);
  ~MCHeaderCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCHeader create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCHeader create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCHeader operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCHeader at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCHeader object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCHeaderData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<int,arraysize> m_evtNumber() const;
  template<size_t arraysize>  
  const std::array<unsigned,arraysize> m_evtTime() const;


private:
  int m_collectionID;
  MCHeaderObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCVertex>* m_rel_m_primaryVertices; //relation buffer for r/w
  std::vector<std::vector<ConstMCVertex>*> m_rel_m_primaryVertices_tmp;
 
  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCHeaderDataContainer* m_data;
};

template<typename... Args>
MCHeader  MCHeaderCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCHeaderObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCHeader(obj);
}

template<size_t arraysize>
const std::array<int,arraysize> MCHeaderCollection::m_evtNumber() const {
  std::array<int,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_evtNumber;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<unsigned,arraysize> MCHeaderCollection::m_evtTime() const {
  std::array<unsigned,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_evtTime;
 }
 return tmp;
}


#endif
