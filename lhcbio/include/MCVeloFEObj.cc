#include "MCVeloFEObj.h"


MCVeloFEObj::MCVeloFEObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_MCHits(new std::vector<ConstMCHit>()),m_MCHitsCharge(new std::vector<double>())
    { }

MCVeloFEObj::MCVeloFEObj(const podio::ObjectID id, MCVeloFEData data) :
    ObjBase{id,0},
    data(data)
    { }

MCVeloFEObj::MCVeloFEObj(const MCVeloFEObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_MCHits(new std::vector<ConstMCHit>(*(other.m_m_MCHits))),m_MCHitsCharge(new std::vector<double>(*(other.m_MCHitsCharge)))
    { }

MCVeloFEObj::~MCVeloFEObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_MCHits;
delete m_MCHitsCharge;

  }
}
