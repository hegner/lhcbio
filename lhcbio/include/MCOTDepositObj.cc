#include "MCOTDepositObj.h"
#include "MCHitConst.h"


MCOTDepositObj::MCOTDepositObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_mcHit(nullptr)

    { }

MCOTDepositObj::MCOTDepositObj(const podio::ObjectID id, MCOTDepositData data) :
    ObjBase{id,0},
    data(data)
    { }

MCOTDepositObj::MCOTDepositObj(const MCOTDepositObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCOTDepositObj::~MCOTDepositObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_mcHit;

  }
}
