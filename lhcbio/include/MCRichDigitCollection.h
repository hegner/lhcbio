//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCRichDigitCollection_H
#define  MCRichDigitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCRichDigitData.h"
#include "MCRichDigit.h"
#include "MCRichDigitObj.h"

typedef std::vector<MCRichDigitData> MCRichDigitDataContainer;
typedef std::deque<MCRichDigitObj*> MCRichDigitObjPointerContainer;

class MCRichDigitCollectionIterator {

  public:
    MCRichDigitCollectionIterator(int index, const MCRichDigitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCRichDigitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCRichDigit operator*() const;
    const MCRichDigit* operator->() const;
    const MCRichDigitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCRichDigit m_object;
    const MCRichDigitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCRichDigitCollection : public podio::CollectionBase {

public:
  typedef const MCRichDigitCollectionIterator const_iterator;

  MCRichDigitCollection();
//  MCRichDigitCollection(const MCRichDigitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCRichDigitCollection(MCRichDigitVector* data, int collectionID);
  ~MCRichDigitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCRichDigit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCRichDigit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCRichDigit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCRichDigit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCRichDigit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCRichDigitData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<int,arraysize> m_hits() const;
  template<size_t arraysize>  
  const std::array<MCRichDigitHistoryCode,arraysize> m_history() const;


private:
  int m_collectionID;
  MCRichDigitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCRichDigitDataContainer* m_data;
};

template<typename... Args>
MCRichDigit  MCRichDigitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCRichDigitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCRichDigit(obj);
}

template<size_t arraysize>
const std::array<int,arraysize> MCRichDigitCollection::m_hits() const {
  std::array<int,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_hits;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<MCRichDigitHistoryCode,arraysize> MCRichDigitCollection::m_history() const {
  std::array<MCRichDigitHistoryCode,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_history;
 }
 return tmp;
}


#endif
