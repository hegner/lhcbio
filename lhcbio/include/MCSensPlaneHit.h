#ifndef MCSensPlaneHit_H
#define MCSensPlaneHit_H
#include "MCSensPlaneHitData.h"
#include "LorentzVector.h"
#include "LorentzVector.h"

#include <vector>
#include "podio/ObjectID.h"

// /< true particle Geant identifier
// author: Vanya Belyaev, revised by V.Romanovski and G.Corti

//forward declarations
class MCSensPlaneHitCollection;
class MCSensPlaneHitCollectionIterator;
class ConstMCSensPlaneHit;
class MCParticle;
class ConstMCParticle;


#include "MCSensPlaneHitConst.h"
#include "MCSensPlaneHitObj.h"

class MCSensPlaneHit {

  friend MCSensPlaneHitCollection;
  friend MCSensPlaneHitCollectionIterator;
  friend ConstMCSensPlaneHit;

public:

  /// default constructor
  MCSensPlaneHit();
    MCSensPlaneHit(LorentzVector m_position,LorentzVector m_momentum,int m_type);

  /// constructor from existing MCSensPlaneHitObj
  MCSensPlaneHit(MCSensPlaneHitObj* obj);
  /// copy constructor
  MCSensPlaneHit(const MCSensPlaneHit& other);
  /// copy-assignment operator
  MCSensPlaneHit& operator=(const MCSensPlaneHit& other);
  /// support cloning (deep-copy)
  MCSensPlaneHit clone() const;
  /// destructor
  ~MCSensPlaneHit();

  /// conversion to const object
  operator ConstMCSensPlaneHit () const;

public:

  const LorentzVector& m_position() const;
  const LorentzVector& m_momentum() const;
  const int& m_type() const;
  const ConstMCParticle m_particle() const;

  LorentzVector& m_position();
  void m_position(class LorentzVector value);
  LorentzVector& m_momentum();
  void m_momentum(class LorentzVector value);
  void m_type(int value);
  void m_particle(ConstMCParticle value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCSensPlaneHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCSensPlaneHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCSensPlaneHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCSensPlaneHit& p1,
//       const MCSensPlaneHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCSensPlaneHitObj* m_obj;

};

#endif
