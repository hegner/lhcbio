#ifndef ConstMCSTDeposit_H
#define ConstMCSTDeposit_H
#include "MCSTDepositData.h"
#include "STChannelID.h"

#include <vector>
#include "podio/ObjectID.h"

// /< channelID
// author: Matthew Needham

//forward declarations
class MCSTDepositObj;
class MCSTDeposit;
class MCSTDepositCollection;
class MCSTDepositCollectionIterator;
class MCHit;
class ConstMCHit;


#include "MCSTDepositObj.h"

class ConstMCSTDeposit {

  friend MCSTDeposit;
  friend MCSTDepositCollection;
  friend MCSTDepositCollectionIterator;

public:

  /// default constructor
  ConstMCSTDeposit();
  ConstMCSTDeposit(double m_depositedCharge,STChannelID m_channelID);

  /// constructor from existing MCSTDepositObj
  ConstMCSTDeposit(MCSTDepositObj* obj);
  /// copy constructor
  ConstMCSTDeposit(const ConstMCSTDeposit& other);
  /// copy-assignment operator
  ConstMCSTDeposit& operator=(const ConstMCSTDeposit& other);
  /// support cloning (deep-copy)
  ConstMCSTDeposit clone() const;
  /// destructor
  ~ConstMCSTDeposit();


public:

  const double& m_depositedCharge() const;
  const STChannelID& m_channelID() const;
  const ConstMCHit m_mcHit() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCSTDepositObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCSTDeposit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCSTDeposit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCSTDeposit& p1,
//       const MCSTDeposit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCSTDepositObj* m_obj;

};

#endif
