#ifndef MCSensPlaneHitDATA_H
#define MCSensPlaneHitDATA_H

// /< true particle Geant identifier
// author: Vanya Belyaev, revised by V.Romanovski and G.Corti

#include "LorentzVector.h"
#include "LorentzVector.h"


class MCSensPlaneHitData {
public:
  LorentzVector m_position; ////< Position of the hit in the sensitive plane 
  LorentzVector m_momentum; ////< Particle momentum for the hit in the sensitive plane 
  int m_type; ////< true particle Geant identifier 

};

#endif
