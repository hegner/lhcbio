#ifndef MCRichDigitHitDATA_H
#define MCRichDigitHitDATA_H

// /< Bit-packed word containing the overall history information for this hit. A summary of both how the hit was produced and also how it was used in the digitisation
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

#include "MCRichDigitHistoryCode.h"


class MCRichDigitHitData {
public:
  MCRichDigitHistoryCode m_history; ////< Bit-packed word containing the overall history information for this hit. A summary of both how the hit was produced and also how it was used in the digitisation 

};

#endif
