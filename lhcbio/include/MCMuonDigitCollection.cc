// standard includes
#include <stdexcept>
#include "MCHitCollection.h" 


#include "MCMuonDigitCollection.h"

MCMuonDigitCollection::MCMuonDigitCollection() : m_collectionID(0), m_entries() ,m_rel_m_MCHits(new std::vector<ConstMCHit>()),m_refCollections(nullptr), m_data(new MCMuonDigitDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCMuonDigit MCMuonDigitCollection::operator[](unsigned int index) const {
  return MCMuonDigit(m_entries[index]);
}

const MCMuonDigit MCMuonDigitCollection::at(unsigned int index) const {
  return MCMuonDigit(m_entries.at(index));
}

int  MCMuonDigitCollection::size() const {
  return m_entries.size();
}

MCMuonDigit MCMuonDigitCollection::create(){
  auto obj = new MCMuonDigitObj();
  m_entries.emplace_back(obj);
  m_rel_m_MCHits_tmp.push_back(obj->m_m_MCHits);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCMuonDigit(obj);
}

void MCMuonDigitCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  // clear relations to m_MCHits. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_MCHits_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_MCHits_tmp.clear();
  for (auto& item : (*m_rel_m_MCHits)) {item.unlink(); }
  m_rel_m_MCHits->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCMuonDigitCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
     (*m_data)[i].m_MCHits_begin=index;
   (*m_data)[i].m_MCHits_end+=index;
   index = (*m_data)[index].m_MCHits_end;
   for(auto it : (*m_rel_m_MCHits_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[0]->emplace_back(it.getObjectID());
     m_rel_m_MCHits->push_back(it);
   }

  }
  
}

void MCMuonDigitCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCMuonDigitObj({index,m_collectionID}, data);
        obj->m_m_MCHits = m_rel_m_MCHits;
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCMuonDigitCollection::setReferences(const podio::ICollectionProvider* collectionProvider){
  for(unsigned int i=0, size=(*m_refCollections)[0]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCHitCollection* tmp_coll = static_cast<MCHitCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_MCHits->emplace_back(tmp);
  }


  return true; //TODO: check success
}

void MCMuonDigitCollection::push_back(ConstMCMuonDigit object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
          m_rel_m_MCHits_tmp.push_back(obj->m_m_MCHits);

    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCMuonDigitCollection::setBuffer(void* address){
  m_data = static_cast<MCMuonDigitDataContainer*>(address);
}


const MCMuonDigit MCMuonDigitCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCMuonDigit* MCMuonDigitCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCMuonDigitCollectionIterator& MCMuonDigitCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
