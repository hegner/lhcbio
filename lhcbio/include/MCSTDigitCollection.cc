// standard includes
#include <stdexcept>
#include "MCSTDepositCollection.h" 


#include "MCSTDigitCollection.h"

MCSTDigitCollection::MCSTDigitCollection() : m_collectionID(0), m_entries() ,m_rel_m_mcDeposit(new std::vector<ConstMCSTDeposit>()),m_refCollections(nullptr), m_data(new MCSTDigitDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCSTDigit MCSTDigitCollection::operator[](unsigned int index) const {
  return MCSTDigit(m_entries[index]);
}

const MCSTDigit MCSTDigitCollection::at(unsigned int index) const {
  return MCSTDigit(m_entries.at(index));
}

int  MCSTDigitCollection::size() const {
  return m_entries.size();
}

MCSTDigit MCSTDigitCollection::create(){
  auto obj = new MCSTDigitObj();
  m_entries.emplace_back(obj);
  m_rel_m_mcDeposit_tmp.push_back(obj->m_m_mcDeposit);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCSTDigit(obj);
}

void MCSTDigitCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  // clear relations to m_mcDeposit. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_mcDeposit_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_mcDeposit_tmp.clear();
  for (auto& item : (*m_rel_m_mcDeposit)) {item.unlink(); }
  m_rel_m_mcDeposit->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCSTDigitCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
     (*m_data)[i].m_mcDeposit_begin=index;
   (*m_data)[i].m_mcDeposit_end+=index;
   index = (*m_data)[index].m_mcDeposit_end;
   for(auto it : (*m_rel_m_mcDeposit_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[0]->emplace_back(it.getObjectID());
     m_rel_m_mcDeposit->push_back(it);
   }

  }
  
}

void MCSTDigitCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCSTDigitObj({index,m_collectionID}, data);
        obj->m_m_mcDeposit = m_rel_m_mcDeposit;
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCSTDigitCollection::setReferences(const podio::ICollectionProvider* collectionProvider){
  for(unsigned int i=0, size=(*m_refCollections)[0]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCSTDepositCollection* tmp_coll = static_cast<MCSTDepositCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_mcDeposit->emplace_back(tmp);
  }


  return true; //TODO: check success
}

void MCSTDigitCollection::push_back(ConstMCSTDigit object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
          m_rel_m_mcDeposit_tmp.push_back(obj->m_m_mcDeposit);

    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCSTDigitCollection::setBuffer(void* address){
  m_data = static_cast<MCSTDigitDataContainer*>(address);
}


const MCSTDigit MCSTDigitCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCSTDigit* MCSTDigitCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCSTDigitCollectionIterator& MCSTDigitCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
