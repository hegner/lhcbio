// datamodel specific includes
#include "MCCaloDigit.h"
#include "MCCaloDigitConst.h"
#include "MCCaloDigitObj.h"
#include "MCCaloDigitData.h"
#include "MCCaloDigitCollection.h"
#include <iostream>

ConstMCCaloDigit::ConstMCCaloDigit() : m_obj(new MCCaloDigitObj()){
 m_obj->acquire();
};

ConstMCCaloDigit::ConstMCCaloDigit(double m_activeE) : m_obj(new MCCaloDigitObj()){
 m_obj->acquire();
   m_obj->data.m_activeE = m_activeE;
};


ConstMCCaloDigit::ConstMCCaloDigit(const ConstMCCaloDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCCaloDigit& ConstMCCaloDigit::operator=(const ConstMCCaloDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCCaloDigit::ConstMCCaloDigit(MCCaloDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCCaloDigit ConstMCCaloDigit::clone() const {
  return {new MCCaloDigitObj(*m_obj)};
}

ConstMCCaloDigit::~ConstMCCaloDigit(){
  if ( m_obj != nullptr) m_obj->release();
}


std::vector<ConstMCCaloHit>::const_iterator ConstMCCaloDigit::m_hits_begin() const {
  auto ret_value = m_obj->m_m_hits->begin();
  std::advance(ret_value, m_obj->data.m_hits_begin);
  return ret_value;
}

std::vector<ConstMCCaloHit>::const_iterator ConstMCCaloDigit::m_hits_end() const {
  auto ret_value = m_obj->m_m_hits->begin();
  std::advance(ret_value, m_obj->data.m_hits_end-1);
  return ++ret_value;
}

unsigned int ConstMCCaloDigit::m_hits_size() const {
  return (m_obj->data.m_hits_end-m_obj->data.m_hits_begin);
}

ConstMCCaloHit ConstMCCaloDigit::m_hits(unsigned int index) const {
  if (m_hits_size() > index) {
    return m_obj->m_m_hits->at(m_obj->data.m_hits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCCaloDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCCaloDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCCaloDigit::operator==(const MCCaloDigit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCCaloDigit& p1, const MCCaloDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
