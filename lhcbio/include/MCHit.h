#ifndef MCHit_H
#define MCHit_H
#include "MCHitData.h"
#include "XYZPoint.h"
#include "XYZVector.h"

#include <vector>
#include "podio/ObjectID.h"

// /< |P| of particle at the entry point (MeV)
// author: Marco Cattaneo, revised by G.Corti

//forward declarations
class MCHitCollection;
class MCHitCollectionIterator;
class ConstMCHit;
class MCParticle;
class ConstMCParticle;


#include "MCHitConst.h"
#include "MCHitObj.h"

class MCHit {

  friend MCHitCollection;
  friend MCHitCollectionIterator;
  friend ConstMCHit;

public:

  /// default constructor
  MCHit();
    MCHit(int m_sensDetID,XYZPoint m_entry,XYZVector m_displacement,double m_energy,double m_time,double m_p);

  /// constructor from existing MCHitObj
  MCHit(MCHitObj* obj);
  /// copy constructor
  MCHit(const MCHit& other);
  /// copy-assignment operator
  MCHit& operator=(const MCHit& other);
  /// support cloning (deep-copy)
  MCHit clone() const;
  /// destructor
  ~MCHit();

  /// conversion to const object
  operator ConstMCHit () const;

public:

  const int& m_sensDetID() const;
  const XYZPoint& m_entry() const;
  const XYZVector& m_displacement() const;
  const double& m_energy() const;
  const double& m_time() const;
  const double& m_p() const;
  const ConstMCParticle m_MCParticle() const;

  void m_sensDetID(int value);
  XYZPoint& m_entry();
  void m_entry(class XYZPoint value);
  XYZVector& m_displacement();
  void m_displacement(class XYZVector value);
  void m_energy(double value);
  void m_time(double value);
  void m_p(double value);
  void m_MCParticle(ConstMCParticle value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCHit& p1,
//       const MCHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCHitObj* m_obj;

};

#endif
