#ifndef MCExtendedHitDATA_H
#define MCExtendedHitDATA_H

// /< momentum-vector of particle when entering the detector
// author: G.Corti

#include "XYZVector.h"


class MCExtendedHitData {
public:
  XYZVector m_momentum; ////< momentum-vector of particle when entering the detector 

};

#endif
