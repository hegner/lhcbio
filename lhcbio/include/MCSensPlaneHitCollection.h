//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCSensPlaneHitCollection_H
#define  MCSensPlaneHitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCSensPlaneHitData.h"
#include "MCSensPlaneHit.h"
#include "MCSensPlaneHitObj.h"

typedef std::vector<MCSensPlaneHitData> MCSensPlaneHitDataContainer;
typedef std::deque<MCSensPlaneHitObj*> MCSensPlaneHitObjPointerContainer;

class MCSensPlaneHitCollectionIterator {

  public:
    MCSensPlaneHitCollectionIterator(int index, const MCSensPlaneHitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCSensPlaneHitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCSensPlaneHit operator*() const;
    const MCSensPlaneHit* operator->() const;
    const MCSensPlaneHitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCSensPlaneHit m_object;
    const MCSensPlaneHitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCSensPlaneHitCollection : public podio::CollectionBase {

public:
  typedef const MCSensPlaneHitCollectionIterator const_iterator;

  MCSensPlaneHitCollection();
//  MCSensPlaneHitCollection(const MCSensPlaneHitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCSensPlaneHitCollection(MCSensPlaneHitVector* data, int collectionID);
  ~MCSensPlaneHitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCSensPlaneHit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCSensPlaneHit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCSensPlaneHit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCSensPlaneHit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCSensPlaneHit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCSensPlaneHitData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<LorentzVector,arraysize> m_position() const;
  template<size_t arraysize>  
  const std::array<LorentzVector,arraysize> m_momentum() const;
  template<size_t arraysize>  
  const std::array<int,arraysize> m_type() const;


private:
  int m_collectionID;
  MCSensPlaneHitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCParticle>* m_rel_m_particle; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCSensPlaneHitDataContainer* m_data;
};

template<typename... Args>
MCSensPlaneHit  MCSensPlaneHitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCSensPlaneHitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCSensPlaneHit(obj);
}

template<size_t arraysize>
const std::array<LorentzVector,arraysize> MCSensPlaneHitCollection::m_position() const {
  std::array<LorentzVector,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_position;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<LorentzVector,arraysize> MCSensPlaneHitCollection::m_momentum() const {
  std::array<LorentzVector,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_momentum;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<int,arraysize> MCSensPlaneHitCollection::m_type() const {
  std::array<int,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_type;
 }
 return tmp;
}


#endif
