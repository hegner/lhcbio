#ifndef MCCaloHitDATA_H
#define MCCaloHitDATA_H

// /< Time slot# for energy deposition(in 25ns units, 0 means 'current BX')
// author: Olivier Callot (Olivier.Callot@cern.ch) & Vanya Belyaev(Ivan.Belyaev@itep.ru)

#include "Time.h"


class MCCaloHitData {
public:
  double m_activeE; ////< Monte Carlo energy deposition in the active media of given cell 
  int m_sensDetID; ////< Sensitive detector identifier - the identifier of calorimeter cell 
  Time m_time; ////< Time slot# for energy deposition(in 25ns units, 0 means 'current BX') 

};

#endif
