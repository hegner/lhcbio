#ifndef MCRichDigitHistoryCodeDATA_H
#define MCRichDigitHistoryCodeDATA_H

// /< Bit-packed history information
// author: Chris Jones   Christopher.Rob.Jones@cern.ch



class MCRichDigitHistoryCodeData {
public:
  unsigned m_historyCode; ////< Bit-packed history information 

};

#endif
