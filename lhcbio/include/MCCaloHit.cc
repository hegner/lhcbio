// datamodel specific includes
#include "MCCaloHit.h"
#include "MCCaloHitConst.h"
#include "MCCaloHitObj.h"
#include "MCCaloHitData.h"
#include "MCCaloHitCollection.h"
#include <iostream>
#include "MCParticle.h"

MCCaloHit::MCCaloHit() : m_obj(new MCCaloHitObj()){
 m_obj->acquire();
};

MCCaloHit::MCCaloHit(double m_activeE,int m_sensDetID,Time m_time) : m_obj(new MCCaloHitObj()){
 m_obj->acquire();
   m_obj->data.m_activeE = m_activeE;  m_obj->data.m_sensDetID = m_sensDetID;  m_obj->data.m_time = m_time;
};

MCCaloHit::MCCaloHit(const MCCaloHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCCaloHit& MCCaloHit::operator=(const MCCaloHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCCaloHit::MCCaloHit(MCCaloHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCCaloHit MCCaloHit::clone() const {
  return {new MCCaloHitObj(*m_obj)};
}

MCCaloHit::~MCCaloHit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCCaloHit::operator ConstMCCaloHit() const {return ConstMCCaloHit(m_obj);};

  const double& MCCaloHit::m_activeE() const { return m_obj->data.m_activeE; };
  const int& MCCaloHit::m_sensDetID() const { return m_obj->data.m_sensDetID; };
  const Time& MCCaloHit::m_time() const { return m_obj->data.m_time; };
  const ConstMCParticle MCCaloHit::m_particle() const { if (m_obj->m_m_particle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_particle));};

void MCCaloHit::m_activeE(double value){ m_obj->data.m_activeE = value;}
void MCCaloHit::m_sensDetID(int value){ m_obj->data.m_sensDetID = value;}
  Time& MCCaloHit::m_time() { return m_obj->data.m_time; };
void MCCaloHit::m_time(class Time value){ m_obj->data.m_time = value;}
void MCCaloHit::m_particle(ConstMCParticle value) { if (m_obj->m_m_particle != nullptr) delete m_obj->m_m_particle; m_obj->m_m_particle = new ConstMCParticle(value); };


bool  MCCaloHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCCaloHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCCaloHit::operator==(const ConstMCCaloHit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCCaloHit& p1, const MCCaloHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
