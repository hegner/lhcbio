// datamodel specific includes
#include "MCRichOpticalPhoton.h"
#include "MCRichOpticalPhotonConst.h"
#include "MCRichOpticalPhotonObj.h"
#include "MCRichOpticalPhotonData.h"
#include "MCRichOpticalPhotonCollection.h"
#include <iostream>
#include "MCRichHit.h"

MCRichOpticalPhoton::MCRichOpticalPhoton() : m_obj(new MCRichOpticalPhotonObj()){
 m_obj->acquire();
};

MCRichOpticalPhoton::MCRichOpticalPhoton(XYZPoint m_pdIncidencePoint,XYZPoint m_sphericalMirrorReflectPoint,XYZPoint m_flatMirrorReflectPoint,XYZPoint m_aerogelExitPoint,float m_cherenkovTheta,float m_cherenkovPhi,XYZPoint m_emissionPoint,float m_energyAtProduction,XYZVector m_parentMomentum,XYZPoint m_hpdQWIncidencePoint) : m_obj(new MCRichOpticalPhotonObj()){
 m_obj->acquire();
   m_obj->data.m_pdIncidencePoint = m_pdIncidencePoint;  m_obj->data.m_sphericalMirrorReflectPoint = m_sphericalMirrorReflectPoint;  m_obj->data.m_flatMirrorReflectPoint = m_flatMirrorReflectPoint;  m_obj->data.m_aerogelExitPoint = m_aerogelExitPoint;  m_obj->data.m_cherenkovTheta = m_cherenkovTheta;  m_obj->data.m_cherenkovPhi = m_cherenkovPhi;  m_obj->data.m_emissionPoint = m_emissionPoint;  m_obj->data.m_energyAtProduction = m_energyAtProduction;  m_obj->data.m_parentMomentum = m_parentMomentum;  m_obj->data.m_hpdQWIncidencePoint = m_hpdQWIncidencePoint;
};

MCRichOpticalPhoton::MCRichOpticalPhoton(const MCRichOpticalPhoton& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCRichOpticalPhoton& MCRichOpticalPhoton::operator=(const MCRichOpticalPhoton& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCRichOpticalPhoton::MCRichOpticalPhoton(MCRichOpticalPhotonObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCRichOpticalPhoton MCRichOpticalPhoton::clone() const {
  return {new MCRichOpticalPhotonObj(*m_obj)};
}

MCRichOpticalPhoton::~MCRichOpticalPhoton(){
  if ( m_obj != nullptr) m_obj->release();
}

MCRichOpticalPhoton::operator ConstMCRichOpticalPhoton() const {return ConstMCRichOpticalPhoton(m_obj);};

  const XYZPoint& MCRichOpticalPhoton::m_pdIncidencePoint() const { return m_obj->data.m_pdIncidencePoint; };
  const XYZPoint& MCRichOpticalPhoton::m_sphericalMirrorReflectPoint() const { return m_obj->data.m_sphericalMirrorReflectPoint; };
  const XYZPoint& MCRichOpticalPhoton::m_flatMirrorReflectPoint() const { return m_obj->data.m_flatMirrorReflectPoint; };
  const XYZPoint& MCRichOpticalPhoton::m_aerogelExitPoint() const { return m_obj->data.m_aerogelExitPoint; };
  const float& MCRichOpticalPhoton::m_cherenkovTheta() const { return m_obj->data.m_cherenkovTheta; };
  const float& MCRichOpticalPhoton::m_cherenkovPhi() const { return m_obj->data.m_cherenkovPhi; };
  const XYZPoint& MCRichOpticalPhoton::m_emissionPoint() const { return m_obj->data.m_emissionPoint; };
  const float& MCRichOpticalPhoton::m_energyAtProduction() const { return m_obj->data.m_energyAtProduction; };
  const XYZVector& MCRichOpticalPhoton::m_parentMomentum() const { return m_obj->data.m_parentMomentum; };
  const XYZPoint& MCRichOpticalPhoton::m_hpdQWIncidencePoint() const { return m_obj->data.m_hpdQWIncidencePoint; };
  const ConstMCRichHit MCRichOpticalPhoton::m_mcRichHit() const { if (m_obj->m_m_mcRichHit == nullptr) {
 return ConstMCRichHit(nullptr);}
 return ConstMCRichHit(*(m_obj->m_m_mcRichHit));};

  XYZPoint& MCRichOpticalPhoton::m_pdIncidencePoint() { return m_obj->data.m_pdIncidencePoint; };
void MCRichOpticalPhoton::m_pdIncidencePoint(class XYZPoint value){ m_obj->data.m_pdIncidencePoint = value;}
  XYZPoint& MCRichOpticalPhoton::m_sphericalMirrorReflectPoint() { return m_obj->data.m_sphericalMirrorReflectPoint; };
void MCRichOpticalPhoton::m_sphericalMirrorReflectPoint(class XYZPoint value){ m_obj->data.m_sphericalMirrorReflectPoint = value;}
  XYZPoint& MCRichOpticalPhoton::m_flatMirrorReflectPoint() { return m_obj->data.m_flatMirrorReflectPoint; };
void MCRichOpticalPhoton::m_flatMirrorReflectPoint(class XYZPoint value){ m_obj->data.m_flatMirrorReflectPoint = value;}
  XYZPoint& MCRichOpticalPhoton::m_aerogelExitPoint() { return m_obj->data.m_aerogelExitPoint; };
void MCRichOpticalPhoton::m_aerogelExitPoint(class XYZPoint value){ m_obj->data.m_aerogelExitPoint = value;}
void MCRichOpticalPhoton::m_cherenkovTheta(float value){ m_obj->data.m_cherenkovTheta = value;}
void MCRichOpticalPhoton::m_cherenkovPhi(float value){ m_obj->data.m_cherenkovPhi = value;}
  XYZPoint& MCRichOpticalPhoton::m_emissionPoint() { return m_obj->data.m_emissionPoint; };
void MCRichOpticalPhoton::m_emissionPoint(class XYZPoint value){ m_obj->data.m_emissionPoint = value;}
void MCRichOpticalPhoton::m_energyAtProduction(float value){ m_obj->data.m_energyAtProduction = value;}
  XYZVector& MCRichOpticalPhoton::m_parentMomentum() { return m_obj->data.m_parentMomentum; };
void MCRichOpticalPhoton::m_parentMomentum(class XYZVector value){ m_obj->data.m_parentMomentum = value;}
  XYZPoint& MCRichOpticalPhoton::m_hpdQWIncidencePoint() { return m_obj->data.m_hpdQWIncidencePoint; };
void MCRichOpticalPhoton::m_hpdQWIncidencePoint(class XYZPoint value){ m_obj->data.m_hpdQWIncidencePoint = value;}
void MCRichOpticalPhoton::m_mcRichHit(ConstMCRichHit value) { if (m_obj->m_m_mcRichHit != nullptr) delete m_obj->m_m_mcRichHit; m_obj->m_m_mcRichHit = new ConstMCRichHit(value); };


bool  MCRichOpticalPhoton::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCRichOpticalPhoton::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCRichOpticalPhoton::operator==(const ConstMCRichOpticalPhoton& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCRichOpticalPhoton& p1, const MCRichOpticalPhoton& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
