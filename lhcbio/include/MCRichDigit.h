#ifndef MCRichDigit_H
#define MCRichDigit_H
#include "MCRichDigitData.h"
#include "MCRichDigitHistoryCode.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit-packed word containing the overall history information for this digit
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichDigitCollection;
class MCRichDigitCollectionIterator;
class ConstMCRichDigit;


#include "MCRichDigitConst.h"
#include "MCRichDigitObj.h"

class MCRichDigit {

  friend MCRichDigitCollection;
  friend MCRichDigitCollectionIterator;
  friend ConstMCRichDigit;

public:

  /// default constructor
  MCRichDigit();
    MCRichDigit(int m_hits,MCRichDigitHistoryCode m_history);

  /// constructor from existing MCRichDigitObj
  MCRichDigit(MCRichDigitObj* obj);
  /// copy constructor
  MCRichDigit(const MCRichDigit& other);
  /// copy-assignment operator
  MCRichDigit& operator=(const MCRichDigit& other);
  /// support cloning (deep-copy)
  MCRichDigit clone() const;
  /// destructor
  ~MCRichDigit();

  /// conversion to const object
  operator ConstMCRichDigit () const;

public:

  const int& m_hits() const;
  const MCRichDigitHistoryCode& m_history() const;

  void m_hits(int value);
  MCRichDigitHistoryCode& m_history();
  void m_history(class MCRichDigitHistoryCode value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCRichDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCRichDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichDigit& p1,
//       const MCRichDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichDigitObj* m_obj;

};

#endif
