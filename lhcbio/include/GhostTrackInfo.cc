// datamodel specific includes
#include "GhostTrackInfo.h"
#include "GhostTrackInfoConst.h"
#include "GhostTrackInfoObj.h"
#include "GhostTrackInfoData.h"
#include "GhostTrackInfoCollection.h"
#include <iostream>

GhostTrackInfo::GhostTrackInfo() : m_obj(new GhostTrackInfoObj()){
 m_obj->acquire();
};

GhostTrackInfo::GhostTrackInfo(LinkMap m_linkMap,Classification m_classification) : m_obj(new GhostTrackInfoObj()){
 m_obj->acquire();
   m_obj->data.m_linkMap = m_linkMap;  m_obj->data.m_classification = m_classification;
};

GhostTrackInfo::GhostTrackInfo(const GhostTrackInfo& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

GhostTrackInfo& GhostTrackInfo::operator=(const GhostTrackInfo& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

GhostTrackInfo::GhostTrackInfo(GhostTrackInfoObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

GhostTrackInfo GhostTrackInfo::clone() const {
  return {new GhostTrackInfoObj(*m_obj)};
}

GhostTrackInfo::~GhostTrackInfo(){
  if ( m_obj != nullptr) m_obj->release();
}

GhostTrackInfo::operator ConstGhostTrackInfo() const {return ConstGhostTrackInfo(m_obj);};

  const LinkMap& GhostTrackInfo::m_linkMap() const { return m_obj->data.m_linkMap; };
  const Classification& GhostTrackInfo::m_classification() const { return m_obj->data.m_classification; };

  LinkMap& GhostTrackInfo::m_linkMap() { return m_obj->data.m_linkMap; };
void GhostTrackInfo::m_linkMap(class LinkMap value){ m_obj->data.m_linkMap = value;}
  Classification& GhostTrackInfo::m_classification() { return m_obj->data.m_classification; };
void GhostTrackInfo::m_classification(class Classification value){ m_obj->data.m_classification = value;}


bool  GhostTrackInfo::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID GhostTrackInfo::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool GhostTrackInfo::operator==(const ConstGhostTrackInfo& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const GhostTrackInfo& p1, const GhostTrackInfo& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
