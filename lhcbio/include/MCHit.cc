// datamodel specific includes
#include "MCHit.h"
#include "MCHitConst.h"
#include "MCHitObj.h"
#include "MCHitData.h"
#include "MCHitCollection.h"
#include <iostream>
#include "MCParticle.h"

MCHit::MCHit() : m_obj(new MCHitObj()){
 m_obj->acquire();
};

MCHit::MCHit(int m_sensDetID,XYZPoint m_entry,XYZVector m_displacement,double m_energy,double m_time,double m_p) : m_obj(new MCHitObj()){
 m_obj->acquire();
   m_obj->data.m_sensDetID = m_sensDetID;  m_obj->data.m_entry = m_entry;  m_obj->data.m_displacement = m_displacement;  m_obj->data.m_energy = m_energy;  m_obj->data.m_time = m_time;  m_obj->data.m_p = m_p;
};

MCHit::MCHit(const MCHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCHit& MCHit::operator=(const MCHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCHit::MCHit(MCHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCHit MCHit::clone() const {
  return {new MCHitObj(*m_obj)};
}

MCHit::~MCHit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCHit::operator ConstMCHit() const {return ConstMCHit(m_obj);};

  const int& MCHit::m_sensDetID() const { return m_obj->data.m_sensDetID; };
  const XYZPoint& MCHit::m_entry() const { return m_obj->data.m_entry; };
  const XYZVector& MCHit::m_displacement() const { return m_obj->data.m_displacement; };
  const double& MCHit::m_energy() const { return m_obj->data.m_energy; };
  const double& MCHit::m_time() const { return m_obj->data.m_time; };
  const double& MCHit::m_p() const { return m_obj->data.m_p; };
  const ConstMCParticle MCHit::m_MCParticle() const { if (m_obj->m_m_MCParticle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_MCParticle));};

void MCHit::m_sensDetID(int value){ m_obj->data.m_sensDetID = value;}
  XYZPoint& MCHit::m_entry() { return m_obj->data.m_entry; };
void MCHit::m_entry(class XYZPoint value){ m_obj->data.m_entry = value;}
  XYZVector& MCHit::m_displacement() { return m_obj->data.m_displacement; };
void MCHit::m_displacement(class XYZVector value){ m_obj->data.m_displacement = value;}
void MCHit::m_energy(double value){ m_obj->data.m_energy = value;}
void MCHit::m_time(double value){ m_obj->data.m_time = value;}
void MCHit::m_p(double value){ m_obj->data.m_p = value;}
void MCHit::m_MCParticle(ConstMCParticle value) { if (m_obj->m_m_MCParticle != nullptr) delete m_obj->m_m_MCParticle; m_obj->m_m_MCParticle = new ConstMCParticle(value); };


bool  MCHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCHit::operator==(const ConstMCHit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCHit& p1, const MCHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
