#include "MCRichDigitSummaryObj.h"
#include "MCParticleConst.h"


MCRichDigitSummaryObj::MCRichDigitSummaryObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_MCParticle(nullptr)

    { }

MCRichDigitSummaryObj::MCRichDigitSummaryObj(const podio::ObjectID id, MCRichDigitSummaryData data) :
    ObjBase{id,0},
    data(data)
    { }

MCRichDigitSummaryObj::MCRichDigitSummaryObj(const MCRichDigitSummaryObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCRichDigitSummaryObj::~MCRichDigitSummaryObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_MCParticle;

  }
}
