#ifndef MCMuonDigitInfo_H
#define MCMuonDigitInfo_H
#include "MCMuonDigitInfoData.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Packed information of the origin of the hit   generating the digit, the earliest, and the fate of the digit
// author: Alessia Satta

//forward declarations
class MCMuonDigitInfoCollection;
class MCMuonDigitInfoCollectionIterator;
class ConstMCMuonDigitInfo;


#include "MCMuonDigitInfoConst.h"
#include "MCMuonDigitInfoObj.h"

class MCMuonDigitInfo {

  friend MCMuonDigitInfoCollection;
  friend MCMuonDigitInfoCollectionIterator;
  friend ConstMCMuonDigitInfo;

public:

  /// default constructor
  MCMuonDigitInfo();
    MCMuonDigitInfo(unsigned m_DigitInfo);

  /// constructor from existing MCMuonDigitInfoObj
  MCMuonDigitInfo(MCMuonDigitInfoObj* obj);
  /// copy constructor
  MCMuonDigitInfo(const MCMuonDigitInfo& other);
  /// copy-assignment operator
  MCMuonDigitInfo& operator=(const MCMuonDigitInfo& other);
  /// support cloning (deep-copy)
  MCMuonDigitInfo clone() const;
  /// destructor
  ~MCMuonDigitInfo();

  /// conversion to const object
  operator ConstMCMuonDigitInfo () const;

public:

  const unsigned& m_DigitInfo() const;

  void m_DigitInfo(unsigned value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCMuonDigitInfoObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCMuonDigitInfo& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCMuonDigitInfo& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCMuonDigitInfo& p1,
//       const MCMuonDigitInfo& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCMuonDigitInfoObj* m_obj;

};

#endif
