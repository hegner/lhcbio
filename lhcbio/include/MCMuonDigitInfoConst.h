#ifndef ConstMCMuonDigitInfo_H
#define ConstMCMuonDigitInfo_H
#include "MCMuonDigitInfoData.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Packed information of the origin of the hit   generating the digit, the earliest, and the fate of the digit
// author: Alessia Satta

//forward declarations
class MCMuonDigitInfoObj;
class MCMuonDigitInfo;
class MCMuonDigitInfoCollection;
class MCMuonDigitInfoCollectionIterator;


#include "MCMuonDigitInfoObj.h"

class ConstMCMuonDigitInfo {

  friend MCMuonDigitInfo;
  friend MCMuonDigitInfoCollection;
  friend MCMuonDigitInfoCollectionIterator;

public:

  /// default constructor
  ConstMCMuonDigitInfo();
  ConstMCMuonDigitInfo(unsigned m_DigitInfo);

  /// constructor from existing MCMuonDigitInfoObj
  ConstMCMuonDigitInfo(MCMuonDigitInfoObj* obj);
  /// copy constructor
  ConstMCMuonDigitInfo(const ConstMCMuonDigitInfo& other);
  /// copy-assignment operator
  ConstMCMuonDigitInfo& operator=(const ConstMCMuonDigitInfo& other);
  /// support cloning (deep-copy)
  ConstMCMuonDigitInfo clone() const;
  /// destructor
  ~ConstMCMuonDigitInfo();


public:

  const unsigned& m_DigitInfo() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCMuonDigitInfoObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCMuonDigitInfo& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCMuonDigitInfo& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCMuonDigitInfo& p1,
//       const MCMuonDigitInfo& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCMuonDigitInfoObj* m_obj;

};

#endif
