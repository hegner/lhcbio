// datamodel specific includes
#include "MCRichDigitSummary.h"
#include "MCRichDigitSummaryConst.h"
#include "MCRichDigitSummaryObj.h"
#include "MCRichDigitSummaryData.h"
#include "MCRichDigitSummaryCollection.h"
#include <iostream>
#include "MCParticle.h"

ConstMCRichDigitSummary::ConstMCRichDigitSummary() : m_obj(new MCRichDigitSummaryObj()){
 m_obj->acquire();
};

ConstMCRichDigitSummary::ConstMCRichDigitSummary(MCRichDigitHistoryCode m_history,RichSmartID m_richSmartID) : m_obj(new MCRichDigitSummaryObj()){
 m_obj->acquire();
   m_obj->data.m_history = m_history;  m_obj->data.m_richSmartID = m_richSmartID;
};


ConstMCRichDigitSummary::ConstMCRichDigitSummary(const ConstMCRichDigitSummary& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCRichDigitSummary& ConstMCRichDigitSummary::operator=(const ConstMCRichDigitSummary& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCRichDigitSummary::ConstMCRichDigitSummary(MCRichDigitSummaryObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCRichDigitSummary ConstMCRichDigitSummary::clone() const {
  return {new MCRichDigitSummaryObj(*m_obj)};
}

ConstMCRichDigitSummary::~ConstMCRichDigitSummary(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCParticle ConstMCRichDigitSummary::m_MCParticle() const { if (m_obj->m_m_MCParticle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_MCParticle));};


bool  ConstMCRichDigitSummary::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCRichDigitSummary::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCRichDigitSummary::operator==(const MCRichDigitSummary& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCRichDigitSummary& p1, const MCRichDigitSummary& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
