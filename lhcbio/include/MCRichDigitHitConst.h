#ifndef ConstMCRichDigitHit_H
#define ConstMCRichDigitHit_H
#include "MCRichDigitHitData.h"
#include "MCRichDigitHistoryCode.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit-packed word containing the overall history information for this hit. A summary of both how the hit was produced and also how it was used in the digitisation
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichDigitHitObj;
class MCRichDigitHit;
class MCRichDigitHitCollection;
class MCRichDigitHitCollectionIterator;
class MCRichHit;
class ConstMCRichHit;


#include "MCRichDigitHitObj.h"

class ConstMCRichDigitHit {

  friend MCRichDigitHit;
  friend MCRichDigitHitCollection;
  friend MCRichDigitHitCollectionIterator;

public:

  /// default constructor
  ConstMCRichDigitHit();
  ConstMCRichDigitHit(MCRichDigitHistoryCode m_history);

  /// constructor from existing MCRichDigitHitObj
  ConstMCRichDigitHit(MCRichDigitHitObj* obj);
  /// copy constructor
  ConstMCRichDigitHit(const ConstMCRichDigitHit& other);
  /// copy-assignment operator
  ConstMCRichDigitHit& operator=(const ConstMCRichDigitHit& other);
  /// support cloning (deep-copy)
  ConstMCRichDigitHit clone() const;
  /// destructor
  ~ConstMCRichDigitHit();


public:

  const MCRichDigitHistoryCode& m_history() const;
  const ConstMCRichHit m_mcRichHit() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichDigitHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCRichDigitHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCRichDigitHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichDigitHit& p1,
//       const MCRichDigitHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichDigitHitObj* m_obj;

};

#endif
