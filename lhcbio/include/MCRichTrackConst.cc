// datamodel specific includes
#include "MCRichTrack.h"
#include "MCRichTrackConst.h"
#include "MCRichTrackObj.h"
#include "MCRichTrackData.h"
#include "MCRichTrackCollection.h"
#include <iostream>
#include "MCParticle.h"

ConstMCRichTrack::ConstMCRichTrack() : m_obj(new MCRichTrackObj()){
 m_obj->acquire();
};



ConstMCRichTrack::ConstMCRichTrack(const ConstMCRichTrack& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCRichTrack& ConstMCRichTrack::operator=(const ConstMCRichTrack& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCRichTrack::ConstMCRichTrack(MCRichTrackObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCRichTrack ConstMCRichTrack::clone() const {
  return {new MCRichTrackObj(*m_obj)};
}

ConstMCRichTrack::~ConstMCRichTrack(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCParticle ConstMCRichTrack::m_mcParticle() const { if (m_obj->m_m_mcParticle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_mcParticle));};

std::vector<ConstMCRichSegment>::const_iterator ConstMCRichTrack::m_mcSegments_begin() const {
  auto ret_value = m_obj->m_m_mcSegments->begin();
  std::advance(ret_value, m_obj->data.m_mcSegments_begin);
  return ret_value;
}

std::vector<ConstMCRichSegment>::const_iterator ConstMCRichTrack::m_mcSegments_end() const {
  auto ret_value = m_obj->m_m_mcSegments->begin();
  std::advance(ret_value, m_obj->data.m_mcSegments_end-1);
  return ++ret_value;
}

unsigned int ConstMCRichTrack::m_mcSegments_size() const {
  return (m_obj->data.m_mcSegments_end-m_obj->data.m_mcSegments_begin);
}

ConstMCRichSegment ConstMCRichTrack::m_mcSegments(unsigned int index) const {
  if (m_mcSegments_size() > index) {
    return m_obj->m_m_mcSegments->at(m_obj->data.m_mcSegments_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCRichTrack::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCRichTrack::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCRichTrack::operator==(const MCRichTrack& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCRichTrack& p1, const MCRichTrack& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
