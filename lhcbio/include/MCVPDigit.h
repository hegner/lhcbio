#ifndef MCVPDigit_H
#define MCVPDigit_H
#include "MCVPDigitData.h"
#include <vector>
#include "MCHit.h"
#include <vector>

#include <vector>
#include "podio/ObjectID.h"

// Charge deposits in a given pixel
// author: Marcin Kucharczyk

//forward declarations
class MCVPDigitCollection;
class MCVPDigitCollectionIterator;
class ConstMCVPDigit;


#include "MCVPDigitConst.h"
#include "MCVPDigitObj.h"

class MCVPDigit {

  friend MCVPDigitCollection;
  friend MCVPDigitCollectionIterator;
  friend ConstMCVPDigit;

public:

  /// default constructor
  MCVPDigit();
  
  /// constructor from existing MCVPDigitObj
  MCVPDigit(MCVPDigitObj* obj);
  /// copy constructor
  MCVPDigit(const MCVPDigit& other);
  /// copy-assignment operator
  MCVPDigit& operator=(const MCVPDigit& other);
  /// support cloning (deep-copy)
  MCVPDigit clone() const;
  /// destructor
  ~MCVPDigit();

  /// conversion to const object
  operator ConstMCVPDigit () const;

public:



  void addm_mcHits(ConstMCHit);
  unsigned int m_mcHits_size() const;
  ConstMCHit m_mcHits(unsigned int) const;
  std::vector<ConstMCHit>::const_iterator m_mcHits_begin() const;
  std::vector<ConstMCHit>::const_iterator m_mcHits_end() const;
  void adddeposits(double);
  unsigned int deposits_size() const;
  double deposits(unsigned int) const;
  std::vector<double>::const_iterator deposits_begin() const;
  std::vector<double>::const_iterator deposits_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCVPDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCVPDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCVPDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCVPDigit& p1,
//       const MCVPDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCVPDigitObj* m_obj;

};

#endif
