// datamodel specific includes
#include "MCVeloFE.h"
#include "MCVeloFEConst.h"
#include "MCVeloFEObj.h"
#include "MCVeloFEData.h"
#include "MCVeloFECollection.h"
#include <iostream>

MCVeloFE::MCVeloFE() : m_obj(new MCVeloFEObj()){
 m_obj->acquire();
};

MCVeloFE::MCVeloFE(double m_addedSignal,double m_addedPedestal,double m_addedNoise,double m_addedCMNoise) : m_obj(new MCVeloFEObj()){
 m_obj->acquire();
   m_obj->data.m_addedSignal = m_addedSignal;  m_obj->data.m_addedPedestal = m_addedPedestal;  m_obj->data.m_addedNoise = m_addedNoise;  m_obj->data.m_addedCMNoise = m_addedCMNoise;
};

MCVeloFE::MCVeloFE(const MCVeloFE& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCVeloFE& MCVeloFE::operator=(const MCVeloFE& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCVeloFE::MCVeloFE(MCVeloFEObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCVeloFE MCVeloFE::clone() const {
  return {new MCVeloFEObj(*m_obj)};
}

MCVeloFE::~MCVeloFE(){
  if ( m_obj != nullptr) m_obj->release();
}

MCVeloFE::operator ConstMCVeloFE() const {return ConstMCVeloFE(m_obj);};

  const double& MCVeloFE::m_addedSignal() const { return m_obj->data.m_addedSignal; };
  const double& MCVeloFE::m_addedPedestal() const { return m_obj->data.m_addedPedestal; };
  const double& MCVeloFE::m_addedNoise() const { return m_obj->data.m_addedNoise; };
  const double& MCVeloFE::m_addedCMNoise() const { return m_obj->data.m_addedCMNoise; };

void MCVeloFE::m_addedSignal(double value){ m_obj->data.m_addedSignal = value;}
void MCVeloFE::m_addedPedestal(double value){ m_obj->data.m_addedPedestal = value;}
void MCVeloFE::m_addedNoise(double value){ m_obj->data.m_addedNoise = value;}
void MCVeloFE::m_addedCMNoise(double value){ m_obj->data.m_addedCMNoise = value;}

std::vector<ConstMCHit>::const_iterator MCVeloFE::m_MCHits_begin() const {
  auto ret_value = m_obj->m_m_MCHits->begin();
  std::advance(ret_value, m_obj->data.m_MCHits_begin);
  return ret_value;
}

std::vector<ConstMCHit>::const_iterator MCVeloFE::m_MCHits_end() const {
  auto ret_value = m_obj->m_m_MCHits->begin();
  std::advance(ret_value, m_obj->data.m_MCHits_end-1);
  return ++ret_value;
}

void MCVeloFE::addm_MCHits(ConstMCHit component) {
  m_obj->m_m_MCHits->push_back(component);
  m_obj->data.m_MCHits_end++;
}

unsigned int MCVeloFE::m_MCHits_size() const {
  return (m_obj->data.m_MCHits_end-m_obj->data.m_MCHits_begin);
}

ConstMCHit MCVeloFE::m_MCHits(unsigned int index) const {
  if (m_MCHits_size() > index) {
    return m_obj->m_m_MCHits->at(m_obj->data.m_MCHits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<double>::const_iterator MCVeloFE::MCHitsCharge_begin() const {
  auto ret_value = m_obj->m_MCHitsCharge->begin();
  std::advance(ret_value, m_obj->data.MCHitsCharge_begin);
  return ret_value;
}

std::vector<double>::const_iterator MCVeloFE::MCHitsCharge_end() const {
  auto ret_value = m_obj->m_MCHitsCharge->begin();
  std::advance(ret_value, m_obj->data.MCHitsCharge_end-1);
  return ++ret_value;
}

void MCVeloFE::addMCHitsCharge(double component) {
  m_obj->m_MCHitsCharge->push_back(component);
  m_obj->data.MCHitsCharge_end++;
}

unsigned int MCVeloFE::MCHitsCharge_size() const {
  return (m_obj->data.MCHitsCharge_end-m_obj->data.MCHitsCharge_begin);
}

double MCVeloFE::MCHitsCharge(unsigned int index) const {
  if (MCHitsCharge_size() > index) {
    return m_obj->m_MCHitsCharge->at(m_obj->data.MCHitsCharge_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCVeloFE::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCVeloFE::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCVeloFE::operator==(const ConstMCVeloFE& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCVeloFE& p1, const MCVeloFE& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
