#ifndef MCVPDigitOBJ_H
#define MCVPDigitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCVPDigitData.h"

#include <vector>
#include "MCHit.h"


// forward declarations
class MCVPDigit;
class ConstMCVPDigit;


class MCVPDigitObj : public podio::ObjBase {
public:
  /// constructor
  MCVPDigitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCVPDigitObj(const MCVPDigitObj&);
  /// constructor from ObjectID and MCVPDigitData
  /// does not initialize the internal relation containers
  MCVPDigitObj(const podio::ObjectID id, MCVPDigitData data);
  virtual ~MCVPDigitObj();

public:
  MCVPDigitData data;
  std::vector<ConstMCHit>* m_m_mcHits;
  std::vector<double>* m_deposits;


};


#endif
