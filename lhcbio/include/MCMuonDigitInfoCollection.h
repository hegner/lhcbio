//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCMuonDigitInfoCollection_H
#define  MCMuonDigitInfoCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCMuonDigitInfoData.h"
#include "MCMuonDigitInfo.h"
#include "MCMuonDigitInfoObj.h"

typedef std::vector<MCMuonDigitInfoData> MCMuonDigitInfoDataContainer;
typedef std::deque<MCMuonDigitInfoObj*> MCMuonDigitInfoObjPointerContainer;

class MCMuonDigitInfoCollectionIterator {

  public:
    MCMuonDigitInfoCollectionIterator(int index, const MCMuonDigitInfoObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCMuonDigitInfoCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCMuonDigitInfo operator*() const;
    const MCMuonDigitInfo* operator->() const;
    const MCMuonDigitInfoCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCMuonDigitInfo m_object;
    const MCMuonDigitInfoObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCMuonDigitInfoCollection : public podio::CollectionBase {

public:
  typedef const MCMuonDigitInfoCollectionIterator const_iterator;

  MCMuonDigitInfoCollection();
//  MCMuonDigitInfoCollection(const MCMuonDigitInfoCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCMuonDigitInfoCollection(MCMuonDigitInfoVector* data, int collectionID);
  ~MCMuonDigitInfoCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCMuonDigitInfo create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCMuonDigitInfo create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCMuonDigitInfo operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCMuonDigitInfo at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCMuonDigitInfo object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCMuonDigitInfoData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<unsigned,arraysize> m_DigitInfo() const;


private:
  int m_collectionID;
  MCMuonDigitInfoObjPointerContainer m_entries;
  // members to handle 1-to-N-relations

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCMuonDigitInfoDataContainer* m_data;
};

template<typename... Args>
MCMuonDigitInfo  MCMuonDigitInfoCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCMuonDigitInfoObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCMuonDigitInfo(obj);
}

template<size_t arraysize>
const std::array<unsigned,arraysize> MCMuonDigitInfoCollection::m_DigitInfo() const {
  std::array<unsigned,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_DigitInfo;
 }
 return tmp;
}


#endif
