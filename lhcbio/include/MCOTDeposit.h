#ifndef MCOTDeposit_H
#define MCOTDeposit_H
#include "MCOTDepositData.h"
#include "OTChannelID.h"

#include <vector>
#include "podio/ObjectID.h"

// /< MC solution for the correct left/right ambiguity
// author: Jeroen van Tilburg

//forward declarations
class MCOTDepositCollection;
class MCOTDepositCollectionIterator;
class ConstMCOTDeposit;
class MCHit;
class ConstMCHit;


#include "MCOTDepositConst.h"
#include "MCOTDepositObj.h"

class MCOTDeposit {

  friend MCOTDepositCollection;
  friend MCOTDepositCollectionIterator;
  friend ConstMCOTDeposit;

public:

  /// default constructor
  MCOTDeposit();
    MCOTDeposit(int m_type,OTChannelID m_channel,double m_time,double m_driftDistance,int m_ambiguity);

  /// constructor from existing MCOTDepositObj
  MCOTDeposit(MCOTDepositObj* obj);
  /// copy constructor
  MCOTDeposit(const MCOTDeposit& other);
  /// copy-assignment operator
  MCOTDeposit& operator=(const MCOTDeposit& other);
  /// support cloning (deep-copy)
  MCOTDeposit clone() const;
  /// destructor
  ~MCOTDeposit();

  /// conversion to const object
  operator ConstMCOTDeposit () const;

public:

  const int& m_type() const;
  const OTChannelID& m_channel() const;
  const double& m_time() const;
  const double& m_driftDistance() const;
  const int& m_ambiguity() const;
  const ConstMCHit m_mcHit() const;

  void m_type(int value);
  OTChannelID& m_channel();
  void m_channel(class OTChannelID value);
  void m_time(double value);
  void m_driftDistance(double value);
  void m_ambiguity(int value);
  void m_mcHit(ConstMCHit value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCOTDepositObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCOTDeposit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCOTDeposit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCOTDeposit& p1,
//       const MCOTDeposit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCOTDepositObj* m_obj;

};

#endif
