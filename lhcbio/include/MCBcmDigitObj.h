#ifndef MCBcmDigitOBJ_H
#define MCBcmDigitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCBcmDigitData.h"

#include <vector>


// forward declarations
class MCBcmDigit;
class ConstMCBcmDigit;


class MCBcmDigitObj : public podio::ObjBase {
public:
  /// constructor
  MCBcmDigitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCBcmDigitObj(const MCBcmDigitObj&);
  /// constructor from ObjectID and MCBcmDigitData
  /// does not initialize the internal relation containers
  MCBcmDigitObj(const podio::ObjectID id, MCBcmDigitData data);
  virtual ~MCBcmDigitObj();

public:
  MCBcmDigitData data;
  std::vector<int>* m_MCHitKeys;


};


#endif
