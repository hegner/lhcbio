// datamodel specific includes
#include "MCRichTrack.h"
#include "MCRichTrackConst.h"
#include "MCRichTrackObj.h"
#include "MCRichTrackData.h"
#include "MCRichTrackCollection.h"
#include <iostream>
#include "MCParticle.h"

MCRichTrack::MCRichTrack() : m_obj(new MCRichTrackObj()){
 m_obj->acquire();
};



MCRichTrack::MCRichTrack(const MCRichTrack& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCRichTrack& MCRichTrack::operator=(const MCRichTrack& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCRichTrack::MCRichTrack(MCRichTrackObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCRichTrack MCRichTrack::clone() const {
  return {new MCRichTrackObj(*m_obj)};
}

MCRichTrack::~MCRichTrack(){
  if ( m_obj != nullptr) m_obj->release();
}

MCRichTrack::operator ConstMCRichTrack() const {return ConstMCRichTrack(m_obj);};

  const ConstMCParticle MCRichTrack::m_mcParticle() const { if (m_obj->m_m_mcParticle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_mcParticle));};

void MCRichTrack::m_mcParticle(ConstMCParticle value) { if (m_obj->m_m_mcParticle != nullptr) delete m_obj->m_m_mcParticle; m_obj->m_m_mcParticle = new ConstMCParticle(value); };

std::vector<ConstMCRichSegment>::const_iterator MCRichTrack::m_mcSegments_begin() const {
  auto ret_value = m_obj->m_m_mcSegments->begin();
  std::advance(ret_value, m_obj->data.m_mcSegments_begin);
  return ret_value;
}

std::vector<ConstMCRichSegment>::const_iterator MCRichTrack::m_mcSegments_end() const {
  auto ret_value = m_obj->m_m_mcSegments->begin();
  std::advance(ret_value, m_obj->data.m_mcSegments_end-1);
  return ++ret_value;
}

void MCRichTrack::addm_mcSegments(ConstMCRichSegment component) {
  m_obj->m_m_mcSegments->push_back(component);
  m_obj->data.m_mcSegments_end++;
}

unsigned int MCRichTrack::m_mcSegments_size() const {
  return (m_obj->data.m_mcSegments_end-m_obj->data.m_mcSegments_begin);
}

ConstMCRichSegment MCRichTrack::m_mcSegments(unsigned int index) const {
  if (m_mcSegments_size() > index) {
    return m_obj->m_m_mcSegments->at(m_obj->data.m_mcSegments_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCRichTrack::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCRichTrack::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCRichTrack::operator==(const ConstMCRichTrack& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCRichTrack& p1, const MCRichTrack& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
