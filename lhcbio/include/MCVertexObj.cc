#include "MCVertexObj.h"
#include "MCParticleConst.h"


MCVertexObj::MCVertexObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_mother(nullptr)
,m_m_products(new std::vector<ConstMCParticle>())
    { }

MCVertexObj::MCVertexObj(const podio::ObjectID id, MCVertexData data) :
    ObjBase{id,0},
    data(data)
    { }

MCVertexObj::MCVertexObj(const MCVertexObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_products(new std::vector<ConstMCParticle>(*(other.m_m_products)))
    { }

MCVertexObj::~MCVertexObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_mother;
delete m_m_products;

  }
}
