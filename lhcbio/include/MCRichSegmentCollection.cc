// standard includes
#include <stdexcept>
#include "MCRichOpticalPhotonCollection.h" 
#include "MCRichHitCollection.h" 
#include "MCParticleCollection.h" 
#include "MCRichTrackCollection.h" 


#include "MCRichSegmentCollection.h"

MCRichSegmentCollection::MCRichSegmentCollection() : m_collectionID(0), m_entries() ,m_rel_m_MCRichOpticalPhotons(new std::vector<ConstMCRichOpticalPhoton>()),m_rel_m_MCRichHits(new std::vector<ConstMCRichHit>()),m_rel_m_mcParticle(new std::vector<ConstMCParticle>()),m_rel_m_MCRichTrack(new std::vector<ConstMCRichTrack>()),m_refCollections(nullptr), m_data(new MCRichSegmentDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());
  m_refCollections->push_back(new std::vector<podio::ObjectID>());
  m_refCollections->push_back(new std::vector<podio::ObjectID>());
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCRichSegment MCRichSegmentCollection::operator[](unsigned int index) const {
  return MCRichSegment(m_entries[index]);
}

const MCRichSegment MCRichSegmentCollection::at(unsigned int index) const {
  return MCRichSegment(m_entries.at(index));
}

int  MCRichSegmentCollection::size() const {
  return m_entries.size();
}

MCRichSegment MCRichSegmentCollection::create(){
  auto obj = new MCRichSegmentObj();
  m_entries.emplace_back(obj);
  m_rel_m_MCRichOpticalPhotons_tmp.push_back(obj->m_m_MCRichOpticalPhotons);
  m_rel_m_MCRichHits_tmp.push_back(obj->m_m_MCRichHits);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCRichSegment(obj);
}

void MCRichSegmentCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  // clear relations to m_MCRichOpticalPhotons. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_MCRichOpticalPhotons_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_MCRichOpticalPhotons_tmp.clear();
  for (auto& item : (*m_rel_m_MCRichOpticalPhotons)) {item.unlink(); }
  m_rel_m_MCRichOpticalPhotons->clear();
  // clear relations to m_MCRichHits. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_MCRichHits_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_MCRichHits_tmp.clear();
  for (auto& item : (*m_rel_m_MCRichHits)) {item.unlink(); }
  m_rel_m_MCRichHits->clear();
  for (auto& item : (*m_rel_m_mcParticle)) {item.unlink(); }
  m_rel_m_mcParticle->clear();
  for (auto& item : (*m_rel_m_MCRichTrack)) {item.unlink(); }
  m_rel_m_MCRichTrack->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCRichSegmentCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
     (*m_data)[i].m_MCRichOpticalPhotons_begin=index;
   (*m_data)[i].m_MCRichOpticalPhotons_end+=index;
   index = (*m_data)[index].m_MCRichOpticalPhotons_end;
   for(auto it : (*m_rel_m_MCRichOpticalPhotons_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[0]->emplace_back(it.getObjectID());
     m_rel_m_MCRichOpticalPhotons->push_back(it);
   }
   (*m_data)[i].m_MCRichHits_begin=index;
   (*m_data)[i].m_MCRichHits_end+=index;
   index = (*m_data)[index].m_MCRichHits_end;
   for(auto it : (*m_rel_m_MCRichHits_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[1]->emplace_back(it.getObjectID());
     m_rel_m_MCRichHits->push_back(it);
   }

  }
    for (auto& obj : m_entries) {
if (obj->m_m_mcParticle != nullptr){
(*m_refCollections)[2]->emplace_back(obj->m_m_mcParticle->getObjectID());} else {(*m_refCollections)[2]->push_back({-2,-2}); } };
  for (auto& obj : m_entries) {
if (obj->m_m_MCRichTrack != nullptr){
(*m_refCollections)[3]->emplace_back(obj->m_m_MCRichTrack->getObjectID());} else {(*m_refCollections)[3]->push_back({-2,-2}); } };

}

void MCRichSegmentCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCRichSegmentObj({index,m_collectionID}, data);
        obj->m_m_MCRichOpticalPhotons = m_rel_m_MCRichOpticalPhotons;    obj->m_m_MCRichHits = m_rel_m_MCRichHits;
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCRichSegmentCollection::setReferences(const podio::ICollectionProvider* collectionProvider){
  for(unsigned int i=0, size=(*m_refCollections)[0]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCRichOpticalPhotonCollection* tmp_coll = static_cast<MCRichOpticalPhotonCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_MCRichOpticalPhotons->emplace_back(tmp);
  }
  for(unsigned int i=0, size=(*m_refCollections)[1]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[1])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCRichHitCollection* tmp_coll = static_cast<MCRichHitCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_MCRichHits->emplace_back(tmp);
  }

  for(unsigned int i=0, size=m_entries.size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[2])[i];
    if (id.index != podio::ObjectID::invalid) {
      CollectionBase* coll = nullptr;
      collectionProvider->get(id.collectionID,coll);
      MCParticleCollection* tmp_coll = static_cast<MCParticleCollection*>(coll);
      m_entries[i]->m_m_mcParticle = new ConstMCParticle((*tmp_coll)[id.index]);
    } else {
      m_entries[i]->m_m_mcParticle = nullptr;
    }
  }
  for(unsigned int i=0, size=m_entries.size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[3])[i];
    if (id.index != podio::ObjectID::invalid) {
      CollectionBase* coll = nullptr;
      collectionProvider->get(id.collectionID,coll);
      MCRichTrackCollection* tmp_coll = static_cast<MCRichTrackCollection*>(coll);
      m_entries[i]->m_m_MCRichTrack = new ConstMCRichTrack((*tmp_coll)[id.index]);
    } else {
      m_entries[i]->m_m_MCRichTrack = nullptr;
    }
  }

  return true; //TODO: check success
}

void MCRichSegmentCollection::push_back(ConstMCRichSegment object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
          m_rel_m_MCRichOpticalPhotons_tmp.push_back(obj->m_m_MCRichOpticalPhotons);
  m_rel_m_MCRichHits_tmp.push_back(obj->m_m_MCRichHits);

    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCRichSegmentCollection::setBuffer(void* address){
  m_data = static_cast<MCRichSegmentDataContainer*>(address);
}


const MCRichSegment MCRichSegmentCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCRichSegment* MCRichSegmentCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCRichSegmentCollectionIterator& MCRichSegmentCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
