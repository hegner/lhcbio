#include "MCCaloDigitObj.h"


MCCaloDigitObj::MCCaloDigitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_hits(new std::vector<ConstMCCaloHit>())
    { }

MCCaloDigitObj::MCCaloDigitObj(const podio::ObjectID id, MCCaloDigitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCCaloDigitObj::MCCaloDigitObj(const MCCaloDigitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_hits(new std::vector<ConstMCCaloHit>(*(other.m_m_hits)))
    { }

MCCaloDigitObj::~MCCaloDigitObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_hits;

  }
}
