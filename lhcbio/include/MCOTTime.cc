// datamodel specific includes
#include "MCOTTime.h"
#include "MCOTTimeConst.h"
#include "MCOTTimeObj.h"
#include "MCOTTimeData.h"
#include "MCOTTimeCollection.h"
#include <iostream>

MCOTTime::MCOTTime() : m_obj(new MCOTTimeObj()){
 m_obj->acquire();
};



MCOTTime::MCOTTime(const MCOTTime& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCOTTime& MCOTTime::operator=(const MCOTTime& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCOTTime::MCOTTime(MCOTTimeObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCOTTime MCOTTime::clone() const {
  return {new MCOTTimeObj(*m_obj)};
}

MCOTTime::~MCOTTime(){
  if ( m_obj != nullptr) m_obj->release();
}

MCOTTime::operator ConstMCOTTime() const {return ConstMCOTTime(m_obj);};



std::vector<ConstMCOTDeposit>::const_iterator MCOTTime::m_deposits_begin() const {
  auto ret_value = m_obj->m_m_deposits->begin();
  std::advance(ret_value, m_obj->data.m_deposits_begin);
  return ret_value;
}

std::vector<ConstMCOTDeposit>::const_iterator MCOTTime::m_deposits_end() const {
  auto ret_value = m_obj->m_m_deposits->begin();
  std::advance(ret_value, m_obj->data.m_deposits_end-1);
  return ++ret_value;
}

void MCOTTime::addm_deposits(ConstMCOTDeposit component) {
  m_obj->m_m_deposits->push_back(component);
  m_obj->data.m_deposits_end++;
}

unsigned int MCOTTime::m_deposits_size() const {
  return (m_obj->data.m_deposits_end-m_obj->data.m_deposits_begin);
}

ConstMCOTDeposit MCOTTime::m_deposits(unsigned int index) const {
  if (m_deposits_size() > index) {
    return m_obj->m_m_deposits->at(m_obj->data.m_deposits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCOTTime::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCOTTime::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCOTTime::operator==(const ConstMCOTTime& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCOTTime& p1, const MCOTTime& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
