#ifndef MCVertex_H
#define MCVertex_H
#include "MCVertexData.h"
#include "XYZPoint.h"
#include "MCVertexType.h"
#include <vector>
#include "MCParticle.h"

#include <vector>
#include "podio/ObjectID.h"

// /< How the vertex was made
// author: Gloria Corti, revised by P. Koppenburg

//forward declarations
class MCVertexCollection;
class MCVertexCollectionIterator;
class ConstMCVertex;
class MCParticle;
class ConstMCParticle;


#include "MCVertexConst.h"
#include "MCVertexObj.h"

class MCVertex {

  friend MCVertexCollection;
  friend MCVertexCollectionIterator;
  friend ConstMCVertex;

public:

  /// default constructor
  MCVertex();
    MCVertex(XYZPoint m_position,double m_time,MCVertexType m_type);

  /// constructor from existing MCVertexObj
  MCVertex(MCVertexObj* obj);
  /// copy constructor
  MCVertex(const MCVertex& other);
  /// copy-assignment operator
  MCVertex& operator=(const MCVertex& other);
  /// support cloning (deep-copy)
  MCVertex clone() const;
  /// destructor
  ~MCVertex();

  /// conversion to const object
  operator ConstMCVertex () const;

public:

  const XYZPoint& m_position() const;
  const double& m_time() const;
  const MCVertexType& m_type() const;
  const ConstMCParticle m_mother() const;

  XYZPoint& m_position();
  void m_position(class XYZPoint value);
  void m_time(double value);
  MCVertexType& m_type();
  void m_type(class MCVertexType value);
  void m_mother(ConstMCParticle value);

  void addm_products(ConstMCParticle);
  unsigned int m_products_size() const;
  ConstMCParticle m_products(unsigned int) const;
  std::vector<ConstMCParticle>::const_iterator m_products_begin() const;
  std::vector<ConstMCParticle>::const_iterator m_products_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCVertexObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCVertex& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCVertex& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCVertex& p1,
//       const MCVertex& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCVertexObj* m_obj;

};

#endif
