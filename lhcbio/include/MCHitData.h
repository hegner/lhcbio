#ifndef MCHitDATA_H
#define MCHitDATA_H

// /< |P| of particle at the entry point (MeV)
// author: Marco Cattaneo, revised by G.Corti

#include "XYZPoint.h"
#include "XYZVector.h"


class MCHitData {
public:
  int m_sensDetID; ////< Sensitive detector identifier, its meaning is different for each sub-detector 
  XYZPoint m_entry; ////< Entry point in LHCb reference system (mm) 
  XYZVector m_displacement; ////< Difference between exit and entry points (mm) 
  double m_energy; ////< Energy deposited in step (MeV) 
  double m_time; ////< Time (ns) relative to the origin collision 
  double m_p; ////< |P| of particle at the entry point (MeV) 

};

#endif
