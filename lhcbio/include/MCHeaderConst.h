#ifndef ConstMCHeader_H
#define ConstMCHeader_H
#include "MCHeaderData.h"
#include <vector>
#include "MCVertex.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Event time
// author: P. Koppenburg, revised by G.Corti

//forward declarations
class MCHeaderObj;
class MCHeader;
class MCHeaderCollection;
class MCHeaderCollectionIterator;


#include "MCHeaderObj.h"

class ConstMCHeader {

  friend MCHeader;
  friend MCHeaderCollection;
  friend MCHeaderCollectionIterator;

public:

  /// default constructor
  ConstMCHeader();
  ConstMCHeader(int m_evtNumber,unsigned m_evtTime);

  /// constructor from existing MCHeaderObj
  ConstMCHeader(MCHeaderObj* obj);
  /// copy constructor
  ConstMCHeader(const ConstMCHeader& other);
  /// copy-assignment operator
  ConstMCHeader& operator=(const ConstMCHeader& other);
  /// support cloning (deep-copy)
  ConstMCHeader clone() const;
  /// destructor
  ~ConstMCHeader();


public:

  const int& m_evtNumber() const;
  const unsigned& m_evtTime() const;

  unsigned int m_primaryVertices_size() const;
  ConstMCVertex m_primaryVertices(unsigned int) const;
  std::vector<ConstMCVertex>::const_iterator m_primaryVertices_begin() const;
  std::vector<ConstMCVertex>::const_iterator m_primaryVertices_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCHeaderObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCHeader& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCHeader& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCHeader& p1,
//       const MCHeader& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCHeaderObj* m_obj;

};

#endif
