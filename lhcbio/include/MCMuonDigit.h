#ifndef MCMuonDigit_H
#define MCMuonDigit_H
#include "MCMuonDigitData.h"
#include "MCMuonDigitInfo.h"
#include <vector>
#include "MCHit.h"
#include <vector>
#include "MCMuonHitHistory.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Firing Time of the hit
// author: Alessia Satta

//forward declarations
class MCMuonDigitCollection;
class MCMuonDigitCollectionIterator;
class ConstMCMuonDigit;


#include "MCMuonDigitConst.h"
#include "MCMuonDigitObj.h"

class MCMuonDigit {

  friend MCMuonDigitCollection;
  friend MCMuonDigitCollectionIterator;
  friend ConstMCMuonDigit;

public:

  /// default constructor
  MCMuonDigit();
    MCMuonDigit(MCMuonDigitInfo m_DigitInfo,double m_firingTime);

  /// constructor from existing MCMuonDigitObj
  MCMuonDigit(MCMuonDigitObj* obj);
  /// copy constructor
  MCMuonDigit(const MCMuonDigit& other);
  /// copy-assignment operator
  MCMuonDigit& operator=(const MCMuonDigit& other);
  /// support cloning (deep-copy)
  MCMuonDigit clone() const;
  /// destructor
  ~MCMuonDigit();

  /// conversion to const object
  operator ConstMCMuonDigit () const;

public:

  const MCMuonDigitInfo& m_DigitInfo() const;
  const double& m_firingTime() const;

  MCMuonDigitInfo& m_DigitInfo();
  void m_DigitInfo(class MCMuonDigitInfo value);
  void m_firingTime(double value);

  void addm_MCHits(ConstMCHit);
  unsigned int m_MCHits_size() const;
  ConstMCHit m_MCHits(unsigned int) const;
  std::vector<ConstMCHit>::const_iterator m_MCHits_begin() const;
  std::vector<ConstMCHit>::const_iterator m_MCHits_end() const;
  void addHitsHistory(MCMuonHitHistory);
  unsigned int HitsHistory_size() const;
  MCMuonHitHistory HitsHistory(unsigned int) const;
  std::vector<MCMuonHitHistory>::const_iterator HitsHistory_begin() const;
  std::vector<MCMuonHitHistory>::const_iterator HitsHistory_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCMuonDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCMuonDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCMuonDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCMuonDigit& p1,
//       const MCMuonDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCMuonDigitObj* m_obj;

};

#endif
