#ifndef MCMuonDigitInfoDATA_H
#define MCMuonDigitInfoDATA_H

// /< Packed information of the origin of the hit   generating the digit, the earliest, and the fate of the digit
// author: Alessia Satta



class MCMuonDigitInfoData {
public:
  unsigned m_DigitInfo; ////< Packed information of the origin of the hit   generating the digit, the earliest, and the fate of the digit 

};

#endif
