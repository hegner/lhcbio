#include "MCRichHitObj.h"
#include "MCParticleConst.h"


MCRichHitObj::MCRichHitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_MCParticle(nullptr)

    { }

MCRichHitObj::MCRichHitObj(const podio::ObjectID id, MCRichHitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCRichHitObj::MCRichHitObj(const MCRichHitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCRichHitObj::~MCRichHitObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_MCParticle;

  }
}
