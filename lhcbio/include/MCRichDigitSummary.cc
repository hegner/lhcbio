// datamodel specific includes
#include "MCRichDigitSummary.h"
#include "MCRichDigitSummaryConst.h"
#include "MCRichDigitSummaryObj.h"
#include "MCRichDigitSummaryData.h"
#include "MCRichDigitSummaryCollection.h"
#include <iostream>
#include "MCParticle.h"

MCRichDigitSummary::MCRichDigitSummary() : m_obj(new MCRichDigitSummaryObj()){
 m_obj->acquire();
};

MCRichDigitSummary::MCRichDigitSummary(MCRichDigitHistoryCode m_history,RichSmartID m_richSmartID) : m_obj(new MCRichDigitSummaryObj()){
 m_obj->acquire();
   m_obj->data.m_history = m_history;  m_obj->data.m_richSmartID = m_richSmartID;
};

MCRichDigitSummary::MCRichDigitSummary(const MCRichDigitSummary& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCRichDigitSummary& MCRichDigitSummary::operator=(const MCRichDigitSummary& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCRichDigitSummary::MCRichDigitSummary(MCRichDigitSummaryObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCRichDigitSummary MCRichDigitSummary::clone() const {
  return {new MCRichDigitSummaryObj(*m_obj)};
}

MCRichDigitSummary::~MCRichDigitSummary(){
  if ( m_obj != nullptr) m_obj->release();
}

MCRichDigitSummary::operator ConstMCRichDigitSummary() const {return ConstMCRichDigitSummary(m_obj);};

  const MCRichDigitHistoryCode& MCRichDigitSummary::m_history() const { return m_obj->data.m_history; };
  const RichSmartID& MCRichDigitSummary::m_richSmartID() const { return m_obj->data.m_richSmartID; };
  const ConstMCParticle MCRichDigitSummary::m_MCParticle() const { if (m_obj->m_m_MCParticle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_MCParticle));};

  MCRichDigitHistoryCode& MCRichDigitSummary::m_history() { return m_obj->data.m_history; };
void MCRichDigitSummary::m_history(class MCRichDigitHistoryCode value){ m_obj->data.m_history = value;}
  RichSmartID& MCRichDigitSummary::m_richSmartID() { return m_obj->data.m_richSmartID; };
void MCRichDigitSummary::m_richSmartID(class RichSmartID value){ m_obj->data.m_richSmartID = value;}
void MCRichDigitSummary::m_MCParticle(ConstMCParticle value) { if (m_obj->m_m_MCParticle != nullptr) delete m_obj->m_m_MCParticle; m_obj->m_m_MCParticle = new ConstMCParticle(value); };


bool  MCRichDigitSummary::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCRichDigitSummary::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCRichDigitSummary::operator==(const ConstMCRichDigitSummary& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCRichDigitSummary& p1, const MCRichDigitSummary& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
