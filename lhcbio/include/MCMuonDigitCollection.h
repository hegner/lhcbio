//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCMuonDigitCollection_H
#define  MCMuonDigitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCMuonDigitData.h"
#include "MCMuonDigit.h"
#include "MCMuonDigitObj.h"

typedef std::vector<MCMuonDigitData> MCMuonDigitDataContainer;
typedef std::deque<MCMuonDigitObj*> MCMuonDigitObjPointerContainer;

class MCMuonDigitCollectionIterator {

  public:
    MCMuonDigitCollectionIterator(int index, const MCMuonDigitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCMuonDigitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCMuonDigit operator*() const;
    const MCMuonDigit* operator->() const;
    const MCMuonDigitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCMuonDigit m_object;
    const MCMuonDigitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCMuonDigitCollection : public podio::CollectionBase {

public:
  typedef const MCMuonDigitCollectionIterator const_iterator;

  MCMuonDigitCollection();
//  MCMuonDigitCollection(const MCMuonDigitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCMuonDigitCollection(MCMuonDigitVector* data, int collectionID);
  ~MCMuonDigitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCMuonDigit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCMuonDigit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCMuonDigit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCMuonDigit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCMuonDigit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCMuonDigitData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<MCMuonDigitInfo,arraysize> m_DigitInfo() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_firingTime() const;


private:
  int m_collectionID;
  MCMuonDigitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCHit>* m_rel_m_MCHits; //relation buffer for r/w
  std::vector<std::vector<ConstMCHit>*> m_rel_m_MCHits_tmp;
 
  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCMuonDigitDataContainer* m_data;
};

template<typename... Args>
MCMuonDigit  MCMuonDigitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCMuonDigitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCMuonDigit(obj);
}

template<size_t arraysize>
const std::array<MCMuonDigitInfo,arraysize> MCMuonDigitCollection::m_DigitInfo() const {
  std::array<MCMuonDigitInfo,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_DigitInfo;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCMuonDigitCollection::m_firingTime() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_firingTime;
 }
 return tmp;
}


#endif
