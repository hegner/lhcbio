#ifndef ConstMCOTDeposit_H
#define ConstMCOTDeposit_H
#include "MCOTDepositData.h"
#include "OTChannelID.h"

#include <vector>
#include "podio/ObjectID.h"

// /< MC solution for the correct left/right ambiguity
// author: Jeroen van Tilburg

//forward declarations
class MCOTDepositObj;
class MCOTDeposit;
class MCOTDepositCollection;
class MCOTDepositCollectionIterator;
class MCHit;
class ConstMCHit;


#include "MCOTDepositObj.h"

class ConstMCOTDeposit {

  friend MCOTDeposit;
  friend MCOTDepositCollection;
  friend MCOTDepositCollectionIterator;

public:

  /// default constructor
  ConstMCOTDeposit();
  ConstMCOTDeposit(int m_type,OTChannelID m_channel,double m_time,double m_driftDistance,int m_ambiguity);

  /// constructor from existing MCOTDepositObj
  ConstMCOTDeposit(MCOTDepositObj* obj);
  /// copy constructor
  ConstMCOTDeposit(const ConstMCOTDeposit& other);
  /// copy-assignment operator
  ConstMCOTDeposit& operator=(const ConstMCOTDeposit& other);
  /// support cloning (deep-copy)
  ConstMCOTDeposit clone() const;
  /// destructor
  ~ConstMCOTDeposit();


public:

  const int& m_type() const;
  const OTChannelID& m_channel() const;
  const double& m_time() const;
  const double& m_driftDistance() const;
  const int& m_ambiguity() const;
  const ConstMCHit m_mcHit() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCOTDepositObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCOTDeposit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCOTDeposit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCOTDeposit& p1,
//       const MCOTDeposit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCOTDepositObj* m_obj;

};

#endif
