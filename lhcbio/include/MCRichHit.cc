// datamodel specific includes
#include "MCRichHit.h"
#include "MCRichHitConst.h"
#include "MCRichHitObj.h"
#include "MCRichHitData.h"
#include "MCRichHitCollection.h"
#include <iostream>
#include "MCParticle.h"

MCRichHit::MCRichHit() : m_obj(new MCRichHitObj()){
 m_obj->acquire();
};

MCRichHit::MCRichHit(XYZPoint m_entry,double m_energy,double m_timeOfFlight,RichSmartID m_sensDetID,unsigned m_historyCode) : m_obj(new MCRichHitObj()){
 m_obj->acquire();
   m_obj->data.m_entry = m_entry;  m_obj->data.m_energy = m_energy;  m_obj->data.m_timeOfFlight = m_timeOfFlight;  m_obj->data.m_sensDetID = m_sensDetID;  m_obj->data.m_historyCode = m_historyCode;
};

MCRichHit::MCRichHit(const MCRichHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCRichHit& MCRichHit::operator=(const MCRichHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCRichHit::MCRichHit(MCRichHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCRichHit MCRichHit::clone() const {
  return {new MCRichHitObj(*m_obj)};
}

MCRichHit::~MCRichHit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCRichHit::operator ConstMCRichHit() const {return ConstMCRichHit(m_obj);};

  const XYZPoint& MCRichHit::m_entry() const { return m_obj->data.m_entry; };
  const double& MCRichHit::m_energy() const { return m_obj->data.m_energy; };
  const double& MCRichHit::m_timeOfFlight() const { return m_obj->data.m_timeOfFlight; };
  const RichSmartID& MCRichHit::m_sensDetID() const { return m_obj->data.m_sensDetID; };
  const unsigned& MCRichHit::m_historyCode() const { return m_obj->data.m_historyCode; };
  const ConstMCParticle MCRichHit::m_MCParticle() const { if (m_obj->m_m_MCParticle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_MCParticle));};

  XYZPoint& MCRichHit::m_entry() { return m_obj->data.m_entry; };
void MCRichHit::m_entry(class XYZPoint value){ m_obj->data.m_entry = value;}
void MCRichHit::m_energy(double value){ m_obj->data.m_energy = value;}
void MCRichHit::m_timeOfFlight(double value){ m_obj->data.m_timeOfFlight = value;}
  RichSmartID& MCRichHit::m_sensDetID() { return m_obj->data.m_sensDetID; };
void MCRichHit::m_sensDetID(class RichSmartID value){ m_obj->data.m_sensDetID = value;}
void MCRichHit::m_historyCode(unsigned value){ m_obj->data.m_historyCode = value;}
void MCRichHit::m_MCParticle(ConstMCParticle value) { if (m_obj->m_m_MCParticle != nullptr) delete m_obj->m_m_MCParticle; m_obj->m_m_MCParticle = new ConstMCParticle(value); };


bool  MCRichHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCRichHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCRichHit::operator==(const ConstMCRichHit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCRichHit& p1, const MCRichHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
