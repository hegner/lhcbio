#ifndef MCOTDepositOBJ_H
#define MCOTDepositOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCOTDepositData.h"



// forward declarations
class MCOTDeposit;
class ConstMCOTDeposit;
class ConstMCHit;


class MCOTDepositObj : public podio::ObjBase {
public:
  /// constructor
  MCOTDepositObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCOTDepositObj(const MCOTDepositObj&);
  /// constructor from ObjectID and MCOTDepositData
  /// does not initialize the internal relation containers
  MCOTDepositObj(const podio::ObjectID id, MCOTDepositData data);
  virtual ~MCOTDepositObj();

public:
  MCOTDepositData data;
  ConstMCHit* m_m_mcHit;


};


#endif
