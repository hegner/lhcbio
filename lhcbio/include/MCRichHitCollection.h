//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCRichHitCollection_H
#define  MCRichHitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCRichHitData.h"
#include "MCRichHit.h"
#include "MCRichHitObj.h"

typedef std::vector<MCRichHitData> MCRichHitDataContainer;
typedef std::deque<MCRichHitObj*> MCRichHitObjPointerContainer;

class MCRichHitCollectionIterator {

  public:
    MCRichHitCollectionIterator(int index, const MCRichHitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCRichHitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCRichHit operator*() const;
    const MCRichHit* operator->() const;
    const MCRichHitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCRichHit m_object;
    const MCRichHitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCRichHitCollection : public podio::CollectionBase {

public:
  typedef const MCRichHitCollectionIterator const_iterator;

  MCRichHitCollection();
//  MCRichHitCollection(const MCRichHitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCRichHitCollection(MCRichHitVector* data, int collectionID);
  ~MCRichHitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCRichHit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCRichHit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCRichHit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCRichHit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCRichHit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCRichHitData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<XYZPoint,arraysize> m_entry() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_energy() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_timeOfFlight() const;
  template<size_t arraysize>  
  const std::array<RichSmartID,arraysize> m_sensDetID() const;
  template<size_t arraysize>  
  const std::array<unsigned,arraysize> m_historyCode() const;


private:
  int m_collectionID;
  MCRichHitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCParticle>* m_rel_m_MCParticle; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCRichHitDataContainer* m_data;
};

template<typename... Args>
MCRichHit  MCRichHitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCRichHitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCRichHit(obj);
}

template<size_t arraysize>
const std::array<XYZPoint,arraysize> MCRichHitCollection::m_entry() const {
  std::array<XYZPoint,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_entry;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCRichHitCollection::m_energy() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_energy;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCRichHitCollection::m_timeOfFlight() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_timeOfFlight;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<RichSmartID,arraysize> MCRichHitCollection::m_sensDetID() const {
  std::array<RichSmartID,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_sensDetID;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<unsigned,arraysize> MCRichHitCollection::m_historyCode() const {
  std::array<unsigned,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_historyCode;
 }
 return tmp;
}


#endif
