#ifndef ConstMCRichDigitSummary_H
#define ConstMCRichDigitSummary_H
#include "MCRichDigitSummaryData.h"
#include "MCRichDigitHistoryCode.h"
#include "RichSmartID.h"

#include <vector>
#include "podio/ObjectID.h"

// /< RichSmartID channel identifier
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichDigitSummaryObj;
class MCRichDigitSummary;
class MCRichDigitSummaryCollection;
class MCRichDigitSummaryCollectionIterator;
class MCParticle;
class ConstMCParticle;


#include "MCRichDigitSummaryObj.h"

class ConstMCRichDigitSummary {

  friend MCRichDigitSummary;
  friend MCRichDigitSummaryCollection;
  friend MCRichDigitSummaryCollectionIterator;

public:

  /// default constructor
  ConstMCRichDigitSummary();
  ConstMCRichDigitSummary(MCRichDigitHistoryCode m_history,RichSmartID m_richSmartID);

  /// constructor from existing MCRichDigitSummaryObj
  ConstMCRichDigitSummary(MCRichDigitSummaryObj* obj);
  /// copy constructor
  ConstMCRichDigitSummary(const ConstMCRichDigitSummary& other);
  /// copy-assignment operator
  ConstMCRichDigitSummary& operator=(const ConstMCRichDigitSummary& other);
  /// support cloning (deep-copy)
  ConstMCRichDigitSummary clone() const;
  /// destructor
  ~ConstMCRichDigitSummary();


public:

  const MCRichDigitHistoryCode& m_history() const;
  const RichSmartID& m_richSmartID() const;
  const ConstMCParticle m_MCParticle() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichDigitSummaryObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCRichDigitSummary& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCRichDigitSummary& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichDigitSummary& p1,
//       const MCRichDigitSummary& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichDigitSummaryObj* m_obj;

};

#endif
