#ifndef MCRichTrack_H
#define MCRichTrack_H
#include "MCRichTrackData.h"
#include <vector>
#include "MCRichSegment.h"

#include <vector>
#include "podio/ObjectID.h"

// Complimentary object to a single MCParticle with addition RICH information
// author: Christopher Jones   Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichTrackCollection;
class MCRichTrackCollectionIterator;
class ConstMCRichTrack;
class MCParticle;
class ConstMCParticle;


#include "MCRichTrackConst.h"
#include "MCRichTrackObj.h"

class MCRichTrack {

  friend MCRichTrackCollection;
  friend MCRichTrackCollectionIterator;
  friend ConstMCRichTrack;

public:

  /// default constructor
  MCRichTrack();
  
  /// constructor from existing MCRichTrackObj
  MCRichTrack(MCRichTrackObj* obj);
  /// copy constructor
  MCRichTrack(const MCRichTrack& other);
  /// copy-assignment operator
  MCRichTrack& operator=(const MCRichTrack& other);
  /// support cloning (deep-copy)
  MCRichTrack clone() const;
  /// destructor
  ~MCRichTrack();

  /// conversion to const object
  operator ConstMCRichTrack () const;

public:

  const ConstMCParticle m_mcParticle() const;

  void m_mcParticle(ConstMCParticle value);

  void addm_mcSegments(ConstMCRichSegment);
  unsigned int m_mcSegments_size() const;
  ConstMCRichSegment m_mcSegments(unsigned int) const;
  std::vector<ConstMCRichSegment>::const_iterator m_mcSegments_begin() const;
  std::vector<ConstMCRichSegment>::const_iterator m_mcSegments_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichTrackObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCRichTrack& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCRichTrack& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichTrack& p1,
//       const MCRichTrack& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichTrackObj* m_obj;

};

#endif
