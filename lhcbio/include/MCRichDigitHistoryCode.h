#ifndef MCRichDigitHistoryCode_H
#define MCRichDigitHistoryCode_H
#include "MCRichDigitHistoryCodeData.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit-packed history information
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichDigitHistoryCodeCollection;
class MCRichDigitHistoryCodeCollectionIterator;
class ConstMCRichDigitHistoryCode;


#include "MCRichDigitHistoryCodeConst.h"
#include "MCRichDigitHistoryCodeObj.h"

class MCRichDigitHistoryCode {

  friend MCRichDigitHistoryCodeCollection;
  friend MCRichDigitHistoryCodeCollectionIterator;
  friend ConstMCRichDigitHistoryCode;

public:

  /// default constructor
  MCRichDigitHistoryCode();
    MCRichDigitHistoryCode(unsigned m_historyCode);

  /// constructor from existing MCRichDigitHistoryCodeObj
  MCRichDigitHistoryCode(MCRichDigitHistoryCodeObj* obj);
  /// copy constructor
  MCRichDigitHistoryCode(const MCRichDigitHistoryCode& other);
  /// copy-assignment operator
  MCRichDigitHistoryCode& operator=(const MCRichDigitHistoryCode& other);
  /// support cloning (deep-copy)
  MCRichDigitHistoryCode clone() const;
  /// destructor
  ~MCRichDigitHistoryCode();

  /// conversion to const object
  operator ConstMCRichDigitHistoryCode () const;

public:

  const unsigned& m_historyCode() const;

  void m_historyCode(unsigned value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichDigitHistoryCodeObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCRichDigitHistoryCode& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCRichDigitHistoryCode& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichDigitHistoryCode& p1,
//       const MCRichDigitHistoryCode& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichDigitHistoryCodeObj* m_obj;

};

#endif
