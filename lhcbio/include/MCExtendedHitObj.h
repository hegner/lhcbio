#ifndef MCExtendedHitOBJ_H
#define MCExtendedHitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCExtendedHitData.h"



// forward declarations
class MCExtendedHit;
class ConstMCExtendedHit;


class MCExtendedHitObj : public podio::ObjBase {
public:
  /// constructor
  MCExtendedHitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCExtendedHitObj(const MCExtendedHitObj&);
  /// constructor from ObjectID and MCExtendedHitData
  /// does not initialize the internal relation containers
  MCExtendedHitObj(const podio::ObjectID id, MCExtendedHitData data);
  virtual ~MCExtendedHitObj();

public:
  MCExtendedHitData data;


};


#endif
