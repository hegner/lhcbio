// datamodel specific includes
#include "MCVertex.h"
#include "MCVertexConst.h"
#include "MCVertexObj.h"
#include "MCVertexData.h"
#include "MCVertexCollection.h"
#include <iostream>
#include "MCParticle.h"

MCVertex::MCVertex() : m_obj(new MCVertexObj()){
 m_obj->acquire();
};

MCVertex::MCVertex(XYZPoint m_position,double m_time,MCVertexType m_type) : m_obj(new MCVertexObj()){
 m_obj->acquire();
   m_obj->data.m_position = m_position;  m_obj->data.m_time = m_time;  m_obj->data.m_type = m_type;
};

MCVertex::MCVertex(const MCVertex& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCVertex& MCVertex::operator=(const MCVertex& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCVertex::MCVertex(MCVertexObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCVertex MCVertex::clone() const {
  return {new MCVertexObj(*m_obj)};
}

MCVertex::~MCVertex(){
  if ( m_obj != nullptr) m_obj->release();
}

MCVertex::operator ConstMCVertex() const {return ConstMCVertex(m_obj);};

  const XYZPoint& MCVertex::m_position() const { return m_obj->data.m_position; };
  const double& MCVertex::m_time() const { return m_obj->data.m_time; };
  const MCVertexType& MCVertex::m_type() const { return m_obj->data.m_type; };
  const ConstMCParticle MCVertex::m_mother() const { if (m_obj->m_m_mother == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_mother));};

  XYZPoint& MCVertex::m_position() { return m_obj->data.m_position; };
void MCVertex::m_position(class XYZPoint value){ m_obj->data.m_position = value;}
void MCVertex::m_time(double value){ m_obj->data.m_time = value;}
  MCVertexType& MCVertex::m_type() { return m_obj->data.m_type; };
void MCVertex::m_type(class MCVertexType value){ m_obj->data.m_type = value;}
void MCVertex::m_mother(ConstMCParticle value) { if (m_obj->m_m_mother != nullptr) delete m_obj->m_m_mother; m_obj->m_m_mother = new ConstMCParticle(value); };

std::vector<ConstMCParticle>::const_iterator MCVertex::m_products_begin() const {
  auto ret_value = m_obj->m_m_products->begin();
  std::advance(ret_value, m_obj->data.m_products_begin);
  return ret_value;
}

std::vector<ConstMCParticle>::const_iterator MCVertex::m_products_end() const {
  auto ret_value = m_obj->m_m_products->begin();
  std::advance(ret_value, m_obj->data.m_products_end-1);
  return ++ret_value;
}

void MCVertex::addm_products(ConstMCParticle component) {
  m_obj->m_m_products->push_back(component);
  m_obj->data.m_products_end++;
}

unsigned int MCVertex::m_products_size() const {
  return (m_obj->data.m_products_end-m_obj->data.m_products_begin);
}

ConstMCParticle MCVertex::m_products(unsigned int index) const {
  if (m_products_size() > index) {
    return m_obj->m_m_products->at(m_obj->data.m_products_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCVertex::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCVertex::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCVertex::operator==(const ConstMCVertex& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCVertex& p1, const MCVertex& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
