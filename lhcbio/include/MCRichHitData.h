#ifndef MCRichHitDATA_H
#define MCRichHitDATA_H

// /< Bit packed field containing RICH specific information
// author: Chris Jones    Christopher.Rob.Jones@cern.ch

#include "XYZPoint.h"
#include "RichSmartID.h"


class MCRichHitData {
public:
  XYZPoint m_entry; ////< Entry point to the HPD silicon wafer (mm) 
  double m_energy; ////< Energy deposited in the HPD silicon wafer (MeV) 
  double m_timeOfFlight; ////< Time of flight (ns) 
  RichSmartID m_sensDetID; ////< Sensitive detector ID number. Uniquely identifies the hit HPD pixel using the RichSmartID channel encoding 
  unsigned m_historyCode; ////< Bit packed field containing RICH specific information 

};

#endif
