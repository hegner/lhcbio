//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCRichDigitHitCollection_H
#define  MCRichDigitHitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCRichDigitHitData.h"
#include "MCRichDigitHit.h"
#include "MCRichDigitHitObj.h"

typedef std::vector<MCRichDigitHitData> MCRichDigitHitDataContainer;
typedef std::deque<MCRichDigitHitObj*> MCRichDigitHitObjPointerContainer;

class MCRichDigitHitCollectionIterator {

  public:
    MCRichDigitHitCollectionIterator(int index, const MCRichDigitHitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCRichDigitHitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCRichDigitHit operator*() const;
    const MCRichDigitHit* operator->() const;
    const MCRichDigitHitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCRichDigitHit m_object;
    const MCRichDigitHitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCRichDigitHitCollection : public podio::CollectionBase {

public:
  typedef const MCRichDigitHitCollectionIterator const_iterator;

  MCRichDigitHitCollection();
//  MCRichDigitHitCollection(const MCRichDigitHitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCRichDigitHitCollection(MCRichDigitHitVector* data, int collectionID);
  ~MCRichDigitHitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCRichDigitHit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCRichDigitHit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCRichDigitHit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCRichDigitHit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCRichDigitHit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCRichDigitHitData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<MCRichDigitHistoryCode,arraysize> m_history() const;


private:
  int m_collectionID;
  MCRichDigitHitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCRichHit>* m_rel_m_mcRichHit; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCRichDigitHitDataContainer* m_data;
};

template<typename... Args>
MCRichDigitHit  MCRichDigitHitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCRichDigitHitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCRichDigitHit(obj);
}

template<size_t arraysize>
const std::array<MCRichDigitHistoryCode,arraysize> MCRichDigitHitCollection::m_history() const {
  std::array<MCRichDigitHistoryCode,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_history;
 }
 return tmp;
}


#endif
