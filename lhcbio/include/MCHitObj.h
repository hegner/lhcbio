#ifndef MCHitOBJ_H
#define MCHitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCHitData.h"



// forward declarations
class MCHit;
class ConstMCHit;
class ConstMCParticle;


class MCHitObj : public podio::ObjBase {
public:
  /// constructor
  MCHitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCHitObj(const MCHitObj&);
  /// constructor from ObjectID and MCHitData
  /// does not initialize the internal relation containers
  MCHitObj(const podio::ObjectID id, MCHitData data);
  virtual ~MCHitObj();

public:
  MCHitData data;
  ConstMCParticle* m_m_MCParticle;


};


#endif
