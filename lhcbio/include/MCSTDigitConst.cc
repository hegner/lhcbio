// datamodel specific includes
#include "MCSTDigit.h"
#include "MCSTDigitConst.h"
#include "MCSTDigitObj.h"
#include "MCSTDigitData.h"
#include "MCSTDigitCollection.h"
#include <iostream>

ConstMCSTDigit::ConstMCSTDigit() : m_obj(new MCSTDigitObj()){
 m_obj->acquire();
};



ConstMCSTDigit::ConstMCSTDigit(const ConstMCSTDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCSTDigit& ConstMCSTDigit::operator=(const ConstMCSTDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCSTDigit::ConstMCSTDigit(MCSTDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCSTDigit ConstMCSTDigit::clone() const {
  return {new MCSTDigitObj(*m_obj)};
}

ConstMCSTDigit::~ConstMCSTDigit(){
  if ( m_obj != nullptr) m_obj->release();
}


std::vector<ConstMCSTDeposit>::const_iterator ConstMCSTDigit::m_mcDeposit_begin() const {
  auto ret_value = m_obj->m_m_mcDeposit->begin();
  std::advance(ret_value, m_obj->data.m_mcDeposit_begin);
  return ret_value;
}

std::vector<ConstMCSTDeposit>::const_iterator ConstMCSTDigit::m_mcDeposit_end() const {
  auto ret_value = m_obj->m_m_mcDeposit->begin();
  std::advance(ret_value, m_obj->data.m_mcDeposit_end-1);
  return ++ret_value;
}

unsigned int ConstMCSTDigit::m_mcDeposit_size() const {
  return (m_obj->data.m_mcDeposit_end-m_obj->data.m_mcDeposit_begin);
}

ConstMCSTDeposit ConstMCSTDigit::m_mcDeposit(unsigned int index) const {
  if (m_mcDeposit_size() > index) {
    return m_obj->m_m_mcDeposit->at(m_obj->data.m_mcDeposit_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCSTDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCSTDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCSTDigit::operator==(const MCSTDigit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCSTDigit& p1, const MCSTDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
