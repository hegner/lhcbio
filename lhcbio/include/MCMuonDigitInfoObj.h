#ifndef MCMuonDigitInfoOBJ_H
#define MCMuonDigitInfoOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCMuonDigitInfoData.h"



// forward declarations
class MCMuonDigitInfo;
class ConstMCMuonDigitInfo;


class MCMuonDigitInfoObj : public podio::ObjBase {
public:
  /// constructor
  MCMuonDigitInfoObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCMuonDigitInfoObj(const MCMuonDigitInfoObj&);
  /// constructor from ObjectID and MCMuonDigitInfoData
  /// does not initialize the internal relation containers
  MCMuonDigitInfoObj(const podio::ObjectID id, MCMuonDigitInfoData data);
  virtual ~MCMuonDigitInfoObj();

public:
  MCMuonDigitInfoData data;


};


#endif
