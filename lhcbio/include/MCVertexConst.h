#ifndef ConstMCVertex_H
#define ConstMCVertex_H
#include "MCVertexData.h"
#include "XYZPoint.h"
#include "MCVertexType.h"
#include <vector>
#include "MCParticle.h"

#include <vector>
#include "podio/ObjectID.h"

// /< How the vertex was made
// author: Gloria Corti, revised by P. Koppenburg

//forward declarations
class MCVertexObj;
class MCVertex;
class MCVertexCollection;
class MCVertexCollectionIterator;
class MCParticle;
class ConstMCParticle;


#include "MCVertexObj.h"

class ConstMCVertex {

  friend MCVertex;
  friend MCVertexCollection;
  friend MCVertexCollectionIterator;

public:

  /// default constructor
  ConstMCVertex();
  ConstMCVertex(XYZPoint m_position,double m_time,MCVertexType m_type);

  /// constructor from existing MCVertexObj
  ConstMCVertex(MCVertexObj* obj);
  /// copy constructor
  ConstMCVertex(const ConstMCVertex& other);
  /// copy-assignment operator
  ConstMCVertex& operator=(const ConstMCVertex& other);
  /// support cloning (deep-copy)
  ConstMCVertex clone() const;
  /// destructor
  ~ConstMCVertex();


public:

  const XYZPoint& m_position() const;
  const double& m_time() const;
  const MCVertexType& m_type() const;
  const ConstMCParticle m_mother() const;

  unsigned int m_products_size() const;
  ConstMCParticle m_products(unsigned int) const;
  std::vector<ConstMCParticle>::const_iterator m_products_begin() const;
  std::vector<ConstMCParticle>::const_iterator m_products_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCVertexObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCVertex& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCVertex& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCVertex& p1,
//       const MCVertex& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCVertexObj* m_obj;

};

#endif
