// datamodel specific includes
#include "MCCaloDigit.h"
#include "MCCaloDigitConst.h"
#include "MCCaloDigitObj.h"
#include "MCCaloDigitData.h"
#include "MCCaloDigitCollection.h"
#include <iostream>

MCCaloDigit::MCCaloDigit() : m_obj(new MCCaloDigitObj()){
 m_obj->acquire();
};

MCCaloDigit::MCCaloDigit(double m_activeE) : m_obj(new MCCaloDigitObj()){
 m_obj->acquire();
   m_obj->data.m_activeE = m_activeE;
};

MCCaloDigit::MCCaloDigit(const MCCaloDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCCaloDigit& MCCaloDigit::operator=(const MCCaloDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCCaloDigit::MCCaloDigit(MCCaloDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCCaloDigit MCCaloDigit::clone() const {
  return {new MCCaloDigitObj(*m_obj)};
}

MCCaloDigit::~MCCaloDigit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCCaloDigit::operator ConstMCCaloDigit() const {return ConstMCCaloDigit(m_obj);};

  const double& MCCaloDigit::m_activeE() const { return m_obj->data.m_activeE; };

void MCCaloDigit::m_activeE(double value){ m_obj->data.m_activeE = value;}

std::vector<ConstMCCaloHit>::const_iterator MCCaloDigit::m_hits_begin() const {
  auto ret_value = m_obj->m_m_hits->begin();
  std::advance(ret_value, m_obj->data.m_hits_begin);
  return ret_value;
}

std::vector<ConstMCCaloHit>::const_iterator MCCaloDigit::m_hits_end() const {
  auto ret_value = m_obj->m_m_hits->begin();
  std::advance(ret_value, m_obj->data.m_hits_end-1);
  return ++ret_value;
}

void MCCaloDigit::addm_hits(ConstMCCaloHit component) {
  m_obj->m_m_hits->push_back(component);
  m_obj->data.m_hits_end++;
}

unsigned int MCCaloDigit::m_hits_size() const {
  return (m_obj->data.m_hits_end-m_obj->data.m_hits_begin);
}

ConstMCCaloHit MCCaloDigit::m_hits(unsigned int index) const {
  if (m_hits_size() > index) {
    return m_obj->m_m_hits->at(m_obj->data.m_hits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCCaloDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCCaloDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCCaloDigit::operator==(const ConstMCCaloDigit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCCaloDigit& p1, const MCCaloDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
