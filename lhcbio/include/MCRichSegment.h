#ifndef MCRichSegment_H
#define MCRichSegment_H
#include "MCRichSegmentData.h"
#include <vector>
#include "MCRichOpticalPhoton.h"
#include "MCRichHit.h"
#include <vector>
#include "XYZPoint.h"
#include "XYZVector.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit packed field containing detector information
// author: Christopher Jones    Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichSegmentCollection;
class MCRichSegmentCollectionIterator;
class ConstMCRichSegment;
class MCParticle;
class ConstMCParticle;
class MCRichTrack;
class ConstMCRichTrack;


#include "MCRichSegmentConst.h"
#include "MCRichSegmentObj.h"

class MCRichSegment {

  friend MCRichSegmentCollection;
  friend MCRichSegmentCollectionIterator;
  friend ConstMCRichSegment;

public:

  /// default constructor
  MCRichSegment();
    MCRichSegment(unsigned m_historyCode);

  /// constructor from existing MCRichSegmentObj
  MCRichSegment(MCRichSegmentObj* obj);
  /// copy constructor
  MCRichSegment(const MCRichSegment& other);
  /// copy-assignment operator
  MCRichSegment& operator=(const MCRichSegment& other);
  /// support cloning (deep-copy)
  MCRichSegment clone() const;
  /// destructor
  ~MCRichSegment();

  /// conversion to const object
  operator ConstMCRichSegment () const;

public:

  const unsigned& m_historyCode() const;
  const ConstMCParticle m_mcParticle() const;
  const ConstMCRichTrack m_MCRichTrack() const;

  void m_historyCode(unsigned value);
  void m_mcParticle(ConstMCParticle value);
  void m_MCRichTrack(ConstMCRichTrack value);

  void addm_MCRichOpticalPhotons(ConstMCRichOpticalPhoton);
  unsigned int m_MCRichOpticalPhotons_size() const;
  ConstMCRichOpticalPhoton m_MCRichOpticalPhotons(unsigned int) const;
  std::vector<ConstMCRichOpticalPhoton>::const_iterator m_MCRichOpticalPhotons_begin() const;
  std::vector<ConstMCRichOpticalPhoton>::const_iterator m_MCRichOpticalPhotons_end() const;
  void addm_MCRichHits(ConstMCRichHit);
  unsigned int m_MCRichHits_size() const;
  ConstMCRichHit m_MCRichHits(unsigned int) const;
  std::vector<ConstMCRichHit>::const_iterator m_MCRichHits_begin() const;
  std::vector<ConstMCRichHit>::const_iterator m_MCRichHits_end() const;
  void addtrajectoryPoints(XYZPoint);
  unsigned int trajectoryPoints_size() const;
  XYZPoint trajectoryPoints(unsigned int) const;
  std::vector<XYZPoint>::const_iterator trajectoryPoints_begin() const;
  std::vector<XYZPoint>::const_iterator trajectoryPoints_end() const;
  void addtrajectoryMomenta(XYZVector);
  unsigned int trajectoryMomenta_size() const;
  XYZVector trajectoryMomenta(unsigned int) const;
  std::vector<XYZVector>::const_iterator trajectoryMomenta_begin() const;
  std::vector<XYZVector>::const_iterator trajectoryMomenta_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichSegmentObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCRichSegment& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCRichSegment& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichSegment& p1,
//       const MCRichSegment& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichSegmentObj* m_obj;

};

#endif
