#include "MCHitObj.h"
#include "MCParticleConst.h"


MCHitObj::MCHitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_MCParticle(nullptr)

    { }

MCHitObj::MCHitObj(const podio::ObjectID id, MCHitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCHitObj::MCHitObj(const MCHitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCHitObj::~MCHitObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_MCParticle;

  }
}
