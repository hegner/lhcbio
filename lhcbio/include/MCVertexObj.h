#ifndef MCVertexOBJ_H
#define MCVertexOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCVertexData.h"

#include <vector>
#include "MCParticle.h"


// forward declarations
class MCVertex;
class ConstMCVertex;
class ConstMCParticle;


class MCVertexObj : public podio::ObjBase {
public:
  /// constructor
  MCVertexObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCVertexObj(const MCVertexObj&);
  /// constructor from ObjectID and MCVertexData
  /// does not initialize the internal relation containers
  MCVertexObj(const podio::ObjectID id, MCVertexData data);
  virtual ~MCVertexObj();

public:
  MCVertexData data;
  ConstMCParticle* m_m_mother;
  std::vector<ConstMCParticle>* m_m_products;


};


#endif
