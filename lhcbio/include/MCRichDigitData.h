#ifndef MCRichDigitDATA_H
#define MCRichDigitDATA_H

// /< Bit-packed word containing the overall history information for this digit
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

#include "MCRichDigitHistoryCode.h"


class MCRichDigitData {
public:
  int m_hits; ////< MCRichHits contributing to this digitisation object, together with their history code 
  MCRichDigitHistoryCode m_history; ////< Bit-packed word containing the overall history information for this digit 

};

#endif
