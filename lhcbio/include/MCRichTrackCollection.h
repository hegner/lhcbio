//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCRichTrackCollection_H
#define  MCRichTrackCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCRichTrackData.h"
#include "MCRichTrack.h"
#include "MCRichTrackObj.h"

typedef std::vector<MCRichTrackData> MCRichTrackDataContainer;
typedef std::deque<MCRichTrackObj*> MCRichTrackObjPointerContainer;

class MCRichTrackCollectionIterator {

  public:
    MCRichTrackCollectionIterator(int index, const MCRichTrackObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCRichTrackCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCRichTrack operator*() const;
    const MCRichTrack* operator->() const;
    const MCRichTrackCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCRichTrack m_object;
    const MCRichTrackObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCRichTrackCollection : public podio::CollectionBase {

public:
  typedef const MCRichTrackCollectionIterator const_iterator;

  MCRichTrackCollection();
//  MCRichTrackCollection(const MCRichTrackCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCRichTrackCollection(MCRichTrackVector* data, int collectionID);
  ~MCRichTrackCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCRichTrack create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCRichTrack create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCRichTrack operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCRichTrack at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCRichTrack object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCRichTrackData>* _getBuffer() { return m_data;};

   

private:
  int m_collectionID;
  MCRichTrackObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCRichSegment>* m_rel_m_mcSegments; //relation buffer for r/w
  std::vector<std::vector<ConstMCRichSegment>*> m_rel_m_mcSegments_tmp;
   std::vector<ConstMCParticle>* m_rel_m_mcParticle; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCRichTrackDataContainer* m_data;
};

template<typename... Args>
MCRichTrack  MCRichTrackCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCRichTrackObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCRichTrack(obj);
}



#endif
