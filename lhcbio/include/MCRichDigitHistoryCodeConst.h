#ifndef ConstMCRichDigitHistoryCode_H
#define ConstMCRichDigitHistoryCode_H
#include "MCRichDigitHistoryCodeData.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit-packed history information
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichDigitHistoryCodeObj;
class MCRichDigitHistoryCode;
class MCRichDigitHistoryCodeCollection;
class MCRichDigitHistoryCodeCollectionIterator;


#include "MCRichDigitHistoryCodeObj.h"

class ConstMCRichDigitHistoryCode {

  friend MCRichDigitHistoryCode;
  friend MCRichDigitHistoryCodeCollection;
  friend MCRichDigitHistoryCodeCollectionIterator;

public:

  /// default constructor
  ConstMCRichDigitHistoryCode();
  ConstMCRichDigitHistoryCode(unsigned m_historyCode);

  /// constructor from existing MCRichDigitHistoryCodeObj
  ConstMCRichDigitHistoryCode(MCRichDigitHistoryCodeObj* obj);
  /// copy constructor
  ConstMCRichDigitHistoryCode(const ConstMCRichDigitHistoryCode& other);
  /// copy-assignment operator
  ConstMCRichDigitHistoryCode& operator=(const ConstMCRichDigitHistoryCode& other);
  /// support cloning (deep-copy)
  ConstMCRichDigitHistoryCode clone() const;
  /// destructor
  ~ConstMCRichDigitHistoryCode();


public:

  const unsigned& m_historyCode() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichDigitHistoryCodeObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCRichDigitHistoryCode& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCRichDigitHistoryCode& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichDigitHistoryCode& p1,
//       const MCRichDigitHistoryCode& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichDigitHistoryCodeObj* m_obj;

};

#endif
