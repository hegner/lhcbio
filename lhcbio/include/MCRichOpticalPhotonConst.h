#ifndef ConstMCRichOpticalPhoton_H
#define ConstMCRichOpticalPhoton_H
#include "MCRichOpticalPhotonData.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZVector.h"
#include "XYZPoint.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Photon incidence point on the external surface of the HPD Quartz Window
// author: Chris Jones   (Christopher.Rob.Jones@cern.ch)

//forward declarations
class MCRichOpticalPhotonObj;
class MCRichOpticalPhoton;
class MCRichOpticalPhotonCollection;
class MCRichOpticalPhotonCollectionIterator;
class MCRichHit;
class ConstMCRichHit;


#include "MCRichOpticalPhotonObj.h"

class ConstMCRichOpticalPhoton {

  friend MCRichOpticalPhoton;
  friend MCRichOpticalPhotonCollection;
  friend MCRichOpticalPhotonCollectionIterator;

public:

  /// default constructor
  ConstMCRichOpticalPhoton();
  ConstMCRichOpticalPhoton(XYZPoint m_pdIncidencePoint,XYZPoint m_sphericalMirrorReflectPoint,XYZPoint m_flatMirrorReflectPoint,XYZPoint m_aerogelExitPoint,float m_cherenkovTheta,float m_cherenkovPhi,XYZPoint m_emissionPoint,float m_energyAtProduction,XYZVector m_parentMomentum,XYZPoint m_hpdQWIncidencePoint);

  /// constructor from existing MCRichOpticalPhotonObj
  ConstMCRichOpticalPhoton(MCRichOpticalPhotonObj* obj);
  /// copy constructor
  ConstMCRichOpticalPhoton(const ConstMCRichOpticalPhoton& other);
  /// copy-assignment operator
  ConstMCRichOpticalPhoton& operator=(const ConstMCRichOpticalPhoton& other);
  /// support cloning (deep-copy)
  ConstMCRichOpticalPhoton clone() const;
  /// destructor
  ~ConstMCRichOpticalPhoton();


public:

  const XYZPoint& m_pdIncidencePoint() const;
  const XYZPoint& m_sphericalMirrorReflectPoint() const;
  const XYZPoint& m_flatMirrorReflectPoint() const;
  const XYZPoint& m_aerogelExitPoint() const;
  const float& m_cherenkovTheta() const;
  const float& m_cherenkovPhi() const;
  const XYZPoint& m_emissionPoint() const;
  const float& m_energyAtProduction() const;
  const XYZVector& m_parentMomentum() const;
  const XYZPoint& m_hpdQWIncidencePoint() const;
  const ConstMCRichHit m_mcRichHit() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichOpticalPhotonObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCRichOpticalPhoton& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCRichOpticalPhoton& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichOpticalPhoton& p1,
//       const MCRichOpticalPhoton& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichOpticalPhotonObj* m_obj;

};

#endif
