// standard includes
#include <stdexcept>
#include "MCHitCollection.h" 


#include "MCVPDigitCollection.h"

MCVPDigitCollection::MCVPDigitCollection() : m_collectionID(0), m_entries() ,m_rel_m_mcHits(new std::vector<ConstMCHit>()),m_refCollections(nullptr), m_data(new MCVPDigitDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCVPDigit MCVPDigitCollection::operator[](unsigned int index) const {
  return MCVPDigit(m_entries[index]);
}

const MCVPDigit MCVPDigitCollection::at(unsigned int index) const {
  return MCVPDigit(m_entries.at(index));
}

int  MCVPDigitCollection::size() const {
  return m_entries.size();
}

MCVPDigit MCVPDigitCollection::create(){
  auto obj = new MCVPDigitObj();
  m_entries.emplace_back(obj);
  m_rel_m_mcHits_tmp.push_back(obj->m_m_mcHits);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCVPDigit(obj);
}

void MCVPDigitCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  // clear relations to m_mcHits. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_mcHits_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_mcHits_tmp.clear();
  for (auto& item : (*m_rel_m_mcHits)) {item.unlink(); }
  m_rel_m_mcHits->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCVPDigitCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
     (*m_data)[i].m_mcHits_begin=index;
   (*m_data)[i].m_mcHits_end+=index;
   index = (*m_data)[index].m_mcHits_end;
   for(auto it : (*m_rel_m_mcHits_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[0]->emplace_back(it.getObjectID());
     m_rel_m_mcHits->push_back(it);
   }

  }
  
}

void MCVPDigitCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCVPDigitObj({index,m_collectionID}, data);
        obj->m_m_mcHits = m_rel_m_mcHits;
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCVPDigitCollection::setReferences(const podio::ICollectionProvider* collectionProvider){
  for(unsigned int i=0, size=(*m_refCollections)[0]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCHitCollection* tmp_coll = static_cast<MCHitCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_mcHits->emplace_back(tmp);
  }


  return true; //TODO: check success
}

void MCVPDigitCollection::push_back(ConstMCVPDigit object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
          m_rel_m_mcHits_tmp.push_back(obj->m_m_mcHits);

    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCVPDigitCollection::setBuffer(void* address){
  m_data = static_cast<MCVPDigitDataContainer*>(address);
}


const MCVPDigit MCVPDigitCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCVPDigit* MCVPDigitCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCVPDigitCollectionIterator& MCVPDigitCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
