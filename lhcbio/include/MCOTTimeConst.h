#ifndef ConstMCOTTime_H
#define ConstMCOTTime_H
#include "MCOTTimeData.h"
#include <vector>
#include "MCOTDeposit.h"

#include <vector>
#include "podio/ObjectID.h"

// MCOTTime are constucted from the MCOTDeposits
// author: Jeroen van Tilburg and Jacopo Nardulli

//forward declarations
class MCOTTimeObj;
class MCOTTime;
class MCOTTimeCollection;
class MCOTTimeCollectionIterator;


#include "MCOTTimeObj.h"

class ConstMCOTTime {

  friend MCOTTime;
  friend MCOTTimeCollection;
  friend MCOTTimeCollectionIterator;

public:

  /// default constructor
  ConstMCOTTime();
  
  /// constructor from existing MCOTTimeObj
  ConstMCOTTime(MCOTTimeObj* obj);
  /// copy constructor
  ConstMCOTTime(const ConstMCOTTime& other);
  /// copy-assignment operator
  ConstMCOTTime& operator=(const ConstMCOTTime& other);
  /// support cloning (deep-copy)
  ConstMCOTTime clone() const;
  /// destructor
  ~ConstMCOTTime();


public:


  unsigned int m_deposits_size() const;
  ConstMCOTDeposit m_deposits(unsigned int) const;
  std::vector<ConstMCOTDeposit>::const_iterator m_deposits_begin() const;
  std::vector<ConstMCOTDeposit>::const_iterator m_deposits_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCOTTimeObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCOTTime& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCOTTime& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCOTTime& p1,
//       const MCOTTime& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCOTTimeObj* m_obj;

};

#endif
