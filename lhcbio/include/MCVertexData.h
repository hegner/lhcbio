#ifndef MCVertexDATA_H
#define MCVertexDATA_H

// /< How the vertex was made
// author: Gloria Corti, revised by P. Koppenburg

#include "XYZPoint.h"
#include "MCVertexType.h"


class MCVertexData {
public:
  XYZPoint m_position; ////< Position in LHCb reference system 
  double m_time; ////< Time since pp interaction 
  MCVertexType m_type; ////< How the vertex was made 
  unsigned int m_products_begin; 
  unsigned int m_products_end; 

};

#endif
