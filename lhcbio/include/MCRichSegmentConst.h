#ifndef ConstMCRichSegment_H
#define ConstMCRichSegment_H
#include "MCRichSegmentData.h"
#include <vector>
#include "MCRichOpticalPhoton.h"
#include "MCRichHit.h"
#include <vector>
#include "XYZPoint.h"
#include "XYZVector.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit packed field containing detector information
// author: Christopher Jones    Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichSegmentObj;
class MCRichSegment;
class MCRichSegmentCollection;
class MCRichSegmentCollectionIterator;
class MCParticle;
class ConstMCParticle;
class MCRichTrack;
class ConstMCRichTrack;


#include "MCRichSegmentObj.h"

class ConstMCRichSegment {

  friend MCRichSegment;
  friend MCRichSegmentCollection;
  friend MCRichSegmentCollectionIterator;

public:

  /// default constructor
  ConstMCRichSegment();
  ConstMCRichSegment(unsigned m_historyCode);

  /// constructor from existing MCRichSegmentObj
  ConstMCRichSegment(MCRichSegmentObj* obj);
  /// copy constructor
  ConstMCRichSegment(const ConstMCRichSegment& other);
  /// copy-assignment operator
  ConstMCRichSegment& operator=(const ConstMCRichSegment& other);
  /// support cloning (deep-copy)
  ConstMCRichSegment clone() const;
  /// destructor
  ~ConstMCRichSegment();


public:

  const unsigned& m_historyCode() const;
  const ConstMCParticle m_mcParticle() const;
  const ConstMCRichTrack m_MCRichTrack() const;

  unsigned int m_MCRichOpticalPhotons_size() const;
  ConstMCRichOpticalPhoton m_MCRichOpticalPhotons(unsigned int) const;
  std::vector<ConstMCRichOpticalPhoton>::const_iterator m_MCRichOpticalPhotons_begin() const;
  std::vector<ConstMCRichOpticalPhoton>::const_iterator m_MCRichOpticalPhotons_end() const;
  unsigned int m_MCRichHits_size() const;
  ConstMCRichHit m_MCRichHits(unsigned int) const;
  std::vector<ConstMCRichHit>::const_iterator m_MCRichHits_begin() const;
  std::vector<ConstMCRichHit>::const_iterator m_MCRichHits_end() const;
  unsigned int trajectoryPoints_size() const;
  XYZPoint trajectoryPoints(unsigned int) const;
  std::vector<XYZPoint>::const_iterator trajectoryPoints_begin() const;
  std::vector<XYZPoint>::const_iterator trajectoryPoints_end() const;
  unsigned int trajectoryMomenta_size() const;
  XYZVector trajectoryMomenta(unsigned int) const;
  std::vector<XYZVector>::const_iterator trajectoryMomenta_begin() const;
  std::vector<XYZVector>::const_iterator trajectoryMomenta_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichSegmentObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCRichSegment& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCRichSegment& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichSegment& p1,
//       const MCRichSegment& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichSegmentObj* m_obj;

};

#endif
