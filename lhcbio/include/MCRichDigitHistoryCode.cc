// datamodel specific includes
#include "MCRichDigitHistoryCode.h"
#include "MCRichDigitHistoryCodeConst.h"
#include "MCRichDigitHistoryCodeObj.h"
#include "MCRichDigitHistoryCodeData.h"
#include "MCRichDigitHistoryCodeCollection.h"
#include <iostream>

MCRichDigitHistoryCode::MCRichDigitHistoryCode() : m_obj(new MCRichDigitHistoryCodeObj()){
 m_obj->acquire();
};

MCRichDigitHistoryCode::MCRichDigitHistoryCode(unsigned m_historyCode) : m_obj(new MCRichDigitHistoryCodeObj()){
 m_obj->acquire();
   m_obj->data.m_historyCode = m_historyCode;
};

MCRichDigitHistoryCode::MCRichDigitHistoryCode(const MCRichDigitHistoryCode& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCRichDigitHistoryCode& MCRichDigitHistoryCode::operator=(const MCRichDigitHistoryCode& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCRichDigitHistoryCode::MCRichDigitHistoryCode(MCRichDigitHistoryCodeObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCRichDigitHistoryCode MCRichDigitHistoryCode::clone() const {
  return {new MCRichDigitHistoryCodeObj(*m_obj)};
}

MCRichDigitHistoryCode::~MCRichDigitHistoryCode(){
  if ( m_obj != nullptr) m_obj->release();
}

MCRichDigitHistoryCode::operator ConstMCRichDigitHistoryCode() const {return ConstMCRichDigitHistoryCode(m_obj);};

  const unsigned& MCRichDigitHistoryCode::m_historyCode() const { return m_obj->data.m_historyCode; };

void MCRichDigitHistoryCode::m_historyCode(unsigned value){ m_obj->data.m_historyCode = value;}


bool  MCRichDigitHistoryCode::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCRichDigitHistoryCode::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCRichDigitHistoryCode::operator==(const ConstMCRichDigitHistoryCode& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCRichDigitHistoryCode& p1, const MCRichDigitHistoryCode& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
