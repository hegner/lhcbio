//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCSTDepositCollection_H
#define  MCSTDepositCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCSTDepositData.h"
#include "MCSTDeposit.h"
#include "MCSTDepositObj.h"

typedef std::vector<MCSTDepositData> MCSTDepositDataContainer;
typedef std::deque<MCSTDepositObj*> MCSTDepositObjPointerContainer;

class MCSTDepositCollectionIterator {

  public:
    MCSTDepositCollectionIterator(int index, const MCSTDepositObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCSTDepositCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCSTDeposit operator*() const;
    const MCSTDeposit* operator->() const;
    const MCSTDepositCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCSTDeposit m_object;
    const MCSTDepositObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCSTDepositCollection : public podio::CollectionBase {

public:
  typedef const MCSTDepositCollectionIterator const_iterator;

  MCSTDepositCollection();
//  MCSTDepositCollection(const MCSTDepositCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCSTDepositCollection(MCSTDepositVector* data, int collectionID);
  ~MCSTDepositCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCSTDeposit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCSTDeposit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCSTDeposit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCSTDeposit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCSTDeposit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCSTDepositData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<double,arraysize> m_depositedCharge() const;
  template<size_t arraysize>  
  const std::array<STChannelID,arraysize> m_channelID() const;


private:
  int m_collectionID;
  MCSTDepositObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCHit>* m_rel_m_mcHit; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCSTDepositDataContainer* m_data;
};

template<typename... Args>
MCSTDeposit  MCSTDepositCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCSTDepositObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCSTDeposit(obj);
}

template<size_t arraysize>
const std::array<double,arraysize> MCSTDepositCollection::m_depositedCharge() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_depositedCharge;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<STChannelID,arraysize> MCSTDepositCollection::m_channelID() const {
  std::array<STChannelID,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_channelID;
 }
 return tmp;
}


#endif
