#ifndef MCParticleDATA_H
#define MCParticleDATA_H

// /< Bit-packed information on how this MCParticle was produced
// author: Gloria Corti, revised by P. Koppenburg

#include "LorentzVector.h"
#include "ParticleID.h"


class MCParticleData {
public:
  LorentzVector m_momentum; ////< 4-momentum-vector 
  ParticleID m_particleID; ////< Particle ID 
  unsigned m_flags; ////< Bit-packed information on how this MCParticle was produced 
  unsigned int m_endVertices_begin; 
  unsigned int m_endVertices_end; 

};

#endif
