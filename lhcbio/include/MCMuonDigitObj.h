#ifndef MCMuonDigitOBJ_H
#define MCMuonDigitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCMuonDigitData.h"

#include <vector>
#include "MCHit.h"
#include "MCMuonHitHistory.h"


// forward declarations
class MCMuonDigit;
class ConstMCMuonDigit;


class MCMuonDigitObj : public podio::ObjBase {
public:
  /// constructor
  MCMuonDigitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCMuonDigitObj(const MCMuonDigitObj&);
  /// constructor from ObjectID and MCMuonDigitData
  /// does not initialize the internal relation containers
  MCMuonDigitObj(const podio::ObjectID id, MCMuonDigitData data);
  virtual ~MCMuonDigitObj();

public:
  MCMuonDigitData data;
  std::vector<ConstMCHit>* m_m_MCHits;
  std::vector<MCMuonHitHistory>* m_HitsHistory;


};


#endif
