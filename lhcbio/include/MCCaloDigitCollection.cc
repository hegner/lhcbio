// standard includes
#include <stdexcept>
#include "MCCaloHitCollection.h" 


#include "MCCaloDigitCollection.h"

MCCaloDigitCollection::MCCaloDigitCollection() : m_collectionID(0), m_entries() ,m_rel_m_hits(new std::vector<ConstMCCaloHit>()),m_refCollections(nullptr), m_data(new MCCaloDigitDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCCaloDigit MCCaloDigitCollection::operator[](unsigned int index) const {
  return MCCaloDigit(m_entries[index]);
}

const MCCaloDigit MCCaloDigitCollection::at(unsigned int index) const {
  return MCCaloDigit(m_entries.at(index));
}

int  MCCaloDigitCollection::size() const {
  return m_entries.size();
}

MCCaloDigit MCCaloDigitCollection::create(){
  auto obj = new MCCaloDigitObj();
  m_entries.emplace_back(obj);
  m_rel_m_hits_tmp.push_back(obj->m_m_hits);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCCaloDigit(obj);
}

void MCCaloDigitCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  // clear relations to m_hits. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_hits_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_hits_tmp.clear();
  for (auto& item : (*m_rel_m_hits)) {item.unlink(); }
  m_rel_m_hits->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCCaloDigitCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
     (*m_data)[i].m_hits_begin=index;
   (*m_data)[i].m_hits_end+=index;
   index = (*m_data)[index].m_hits_end;
   for(auto it : (*m_rel_m_hits_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[0]->emplace_back(it.getObjectID());
     m_rel_m_hits->push_back(it);
   }

  }
  
}

void MCCaloDigitCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCCaloDigitObj({index,m_collectionID}, data);
        obj->m_m_hits = m_rel_m_hits;
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCCaloDigitCollection::setReferences(const podio::ICollectionProvider* collectionProvider){
  for(unsigned int i=0, size=(*m_refCollections)[0]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCCaloHitCollection* tmp_coll = static_cast<MCCaloHitCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_hits->emplace_back(tmp);
  }


  return true; //TODO: check success
}

void MCCaloDigitCollection::push_back(ConstMCCaloDigit object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
          m_rel_m_hits_tmp.push_back(obj->m_m_hits);

    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCCaloDigitCollection::setBuffer(void* address){
  m_data = static_cast<MCCaloDigitDataContainer*>(address);
}


const MCCaloDigit MCCaloDigitCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCCaloDigit* MCCaloDigitCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCCaloDigitCollectionIterator& MCCaloDigitCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
