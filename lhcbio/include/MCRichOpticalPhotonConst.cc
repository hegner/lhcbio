// datamodel specific includes
#include "MCRichOpticalPhoton.h"
#include "MCRichOpticalPhotonConst.h"
#include "MCRichOpticalPhotonObj.h"
#include "MCRichOpticalPhotonData.h"
#include "MCRichOpticalPhotonCollection.h"
#include <iostream>
#include "MCRichHit.h"

ConstMCRichOpticalPhoton::ConstMCRichOpticalPhoton() : m_obj(new MCRichOpticalPhotonObj()){
 m_obj->acquire();
};

ConstMCRichOpticalPhoton::ConstMCRichOpticalPhoton(XYZPoint m_pdIncidencePoint,XYZPoint m_sphericalMirrorReflectPoint,XYZPoint m_flatMirrorReflectPoint,XYZPoint m_aerogelExitPoint,float m_cherenkovTheta,float m_cherenkovPhi,XYZPoint m_emissionPoint,float m_energyAtProduction,XYZVector m_parentMomentum,XYZPoint m_hpdQWIncidencePoint) : m_obj(new MCRichOpticalPhotonObj()){
 m_obj->acquire();
   m_obj->data.m_pdIncidencePoint = m_pdIncidencePoint;  m_obj->data.m_sphericalMirrorReflectPoint = m_sphericalMirrorReflectPoint;  m_obj->data.m_flatMirrorReflectPoint = m_flatMirrorReflectPoint;  m_obj->data.m_aerogelExitPoint = m_aerogelExitPoint;  m_obj->data.m_cherenkovTheta = m_cherenkovTheta;  m_obj->data.m_cherenkovPhi = m_cherenkovPhi;  m_obj->data.m_emissionPoint = m_emissionPoint;  m_obj->data.m_energyAtProduction = m_energyAtProduction;  m_obj->data.m_parentMomentum = m_parentMomentum;  m_obj->data.m_hpdQWIncidencePoint = m_hpdQWIncidencePoint;
};


ConstMCRichOpticalPhoton::ConstMCRichOpticalPhoton(const ConstMCRichOpticalPhoton& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCRichOpticalPhoton& ConstMCRichOpticalPhoton::operator=(const ConstMCRichOpticalPhoton& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCRichOpticalPhoton::ConstMCRichOpticalPhoton(MCRichOpticalPhotonObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCRichOpticalPhoton ConstMCRichOpticalPhoton::clone() const {
  return {new MCRichOpticalPhotonObj(*m_obj)};
}

ConstMCRichOpticalPhoton::~ConstMCRichOpticalPhoton(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCRichHit ConstMCRichOpticalPhoton::m_mcRichHit() const { if (m_obj->m_m_mcRichHit == nullptr) {
 return ConstMCRichHit(nullptr);}
 return ConstMCRichHit(*(m_obj->m_m_mcRichHit));};


bool  ConstMCRichOpticalPhoton::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCRichOpticalPhoton::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCRichOpticalPhoton::operator==(const MCRichOpticalPhoton& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCRichOpticalPhoton& p1, const MCRichOpticalPhoton& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
