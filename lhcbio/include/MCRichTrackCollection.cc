// standard includes
#include <stdexcept>
#include "MCRichSegmentCollection.h" 
#include "MCParticleCollection.h" 


#include "MCRichTrackCollection.h"

MCRichTrackCollection::MCRichTrackCollection() : m_collectionID(0), m_entries() ,m_rel_m_mcSegments(new std::vector<ConstMCRichSegment>()),m_rel_m_mcParticle(new std::vector<ConstMCParticle>()),m_refCollections(nullptr), m_data(new MCRichTrackDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCRichTrack MCRichTrackCollection::operator[](unsigned int index) const {
  return MCRichTrack(m_entries[index]);
}

const MCRichTrack MCRichTrackCollection::at(unsigned int index) const {
  return MCRichTrack(m_entries.at(index));
}

int  MCRichTrackCollection::size() const {
  return m_entries.size();
}

MCRichTrack MCRichTrackCollection::create(){
  auto obj = new MCRichTrackObj();
  m_entries.emplace_back(obj);
  m_rel_m_mcSegments_tmp.push_back(obj->m_m_mcSegments);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCRichTrack(obj);
}

void MCRichTrackCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  // clear relations to m_mcSegments. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_mcSegments_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_mcSegments_tmp.clear();
  for (auto& item : (*m_rel_m_mcSegments)) {item.unlink(); }
  m_rel_m_mcSegments->clear();
  for (auto& item : (*m_rel_m_mcParticle)) {item.unlink(); }
  m_rel_m_mcParticle->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCRichTrackCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
     (*m_data)[i].m_mcSegments_begin=index;
   (*m_data)[i].m_mcSegments_end+=index;
   index = (*m_data)[index].m_mcSegments_end;
   for(auto it : (*m_rel_m_mcSegments_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[0]->emplace_back(it.getObjectID());
     m_rel_m_mcSegments->push_back(it);
   }

  }
    for (auto& obj : m_entries) {
if (obj->m_m_mcParticle != nullptr){
(*m_refCollections)[1]->emplace_back(obj->m_m_mcParticle->getObjectID());} else {(*m_refCollections)[1]->push_back({-2,-2}); } };

}

void MCRichTrackCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCRichTrackObj({index,m_collectionID}, data);
        obj->m_m_mcSegments = m_rel_m_mcSegments;
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCRichTrackCollection::setReferences(const podio::ICollectionProvider* collectionProvider){
  for(unsigned int i=0, size=(*m_refCollections)[0]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCRichSegmentCollection* tmp_coll = static_cast<MCRichSegmentCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_mcSegments->emplace_back(tmp);
  }

  for(unsigned int i=0, size=m_entries.size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[1])[i];
    if (id.index != podio::ObjectID::invalid) {
      CollectionBase* coll = nullptr;
      collectionProvider->get(id.collectionID,coll);
      MCParticleCollection* tmp_coll = static_cast<MCParticleCollection*>(coll);
      m_entries[i]->m_m_mcParticle = new ConstMCParticle((*tmp_coll)[id.index]);
    } else {
      m_entries[i]->m_m_mcParticle = nullptr;
    }
  }

  return true; //TODO: check success
}

void MCRichTrackCollection::push_back(ConstMCRichTrack object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
          m_rel_m_mcSegments_tmp.push_back(obj->m_m_mcSegments);

    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCRichTrackCollection::setBuffer(void* address){
  m_data = static_cast<MCRichTrackDataContainer*>(address);
}


const MCRichTrack MCRichTrackCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCRichTrack* MCRichTrackCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCRichTrackCollectionIterator& MCRichTrackCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
