#include "MCMuonDigitInfoObj.h"


MCMuonDigitInfoObj::MCMuonDigitInfoObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    
    { }

MCMuonDigitInfoObj::MCMuonDigitInfoObj(const podio::ObjectID id, MCMuonDigitInfoData data) :
    ObjBase{id,0},
    data(data)
    { }

MCMuonDigitInfoObj::MCMuonDigitInfoObj(const MCMuonDigitInfoObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCMuonDigitInfoObj::~MCMuonDigitInfoObj() {
  if (id.index == podio::ObjectID::untracked) {

  }
}
