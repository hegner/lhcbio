//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCVeloFECollection_H
#define  MCVeloFECollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCVeloFEData.h"
#include "MCVeloFE.h"
#include "MCVeloFEObj.h"

typedef std::vector<MCVeloFEData> MCVeloFEDataContainer;
typedef std::deque<MCVeloFEObj*> MCVeloFEObjPointerContainer;

class MCVeloFECollectionIterator {

  public:
    MCVeloFECollectionIterator(int index, const MCVeloFEObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCVeloFECollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCVeloFE operator*() const;
    const MCVeloFE* operator->() const;
    const MCVeloFECollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCVeloFE m_object;
    const MCVeloFEObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCVeloFECollection : public podio::CollectionBase {

public:
  typedef const MCVeloFECollectionIterator const_iterator;

  MCVeloFECollection();
//  MCVeloFECollection(const MCVeloFECollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCVeloFECollection(MCVeloFEVector* data, int collectionID);
  ~MCVeloFECollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCVeloFE create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCVeloFE create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCVeloFE operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCVeloFE at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCVeloFE object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCVeloFEData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<double,arraysize> m_addedSignal() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_addedPedestal() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_addedNoise() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_addedCMNoise() const;


private:
  int m_collectionID;
  MCVeloFEObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCHit>* m_rel_m_MCHits; //relation buffer for r/w
  std::vector<std::vector<ConstMCHit>*> m_rel_m_MCHits_tmp;
 
  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCVeloFEDataContainer* m_data;
};

template<typename... Args>
MCVeloFE  MCVeloFECollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCVeloFEObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCVeloFE(obj);
}

template<size_t arraysize>
const std::array<double,arraysize> MCVeloFECollection::m_addedSignal() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_addedSignal;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCVeloFECollection::m_addedPedestal() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_addedPedestal;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCVeloFECollection::m_addedNoise() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_addedNoise;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCVeloFECollection::m_addedCMNoise() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_addedCMNoise;
 }
 return tmp;
}


#endif
