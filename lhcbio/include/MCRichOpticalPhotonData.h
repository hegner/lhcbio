#ifndef MCRichOpticalPhotonDATA_H
#define MCRichOpticalPhotonDATA_H

// /< Photon incidence point on the external surface of the HPD Quartz Window
// author: Chris Jones   (Christopher.Rob.Jones@cern.ch)

#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZVector.h"
#include "XYZPoint.h"


class MCRichOpticalPhotonData {
public:
  XYZPoint m_pdIncidencePoint; ////< Point of incidence of the photon on the HPD window 
  XYZPoint m_sphericalMirrorReflectPoint; ////< Spherical mirror reflection point 
  XYZPoint m_flatMirrorReflectPoint; ////< Flat mirror reflection point 
  XYZPoint m_aerogelExitPoint; ////< If applicable, exit point of the photon from the aerogel radiator 
  float m_cherenkovTheta; ////< Cherenkov angle theta at production 
  float m_cherenkovPhi; ////< Cherenkov angle phi at production 
  XYZPoint m_emissionPoint; ////< Photon emission point 
  float m_energyAtProduction; ////< Photon energy at production 
  XYZVector m_parentMomentum; ////< The momentum of the photon parent at production 
  XYZPoint m_hpdQWIncidencePoint; ////< Photon incidence point on the external surface of the HPD Quartz Window 

};

#endif
