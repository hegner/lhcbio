#ifndef MCRichDigitSummaryOBJ_H
#define MCRichDigitSummaryOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCRichDigitSummaryData.h"



// forward declarations
class MCRichDigitSummary;
class ConstMCRichDigitSummary;
class ConstMCParticle;


class MCRichDigitSummaryObj : public podio::ObjBase {
public:
  /// constructor
  MCRichDigitSummaryObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCRichDigitSummaryObj(const MCRichDigitSummaryObj&);
  /// constructor from ObjectID and MCRichDigitSummaryData
  /// does not initialize the internal relation containers
  MCRichDigitSummaryObj(const podio::ObjectID id, MCRichDigitSummaryData data);
  virtual ~MCRichDigitSummaryObj();

public:
  MCRichDigitSummaryData data;
  ConstMCParticle* m_m_MCParticle;


};


#endif
