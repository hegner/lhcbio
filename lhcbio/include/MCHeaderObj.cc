#include "MCHeaderObj.h"


MCHeaderObj::MCHeaderObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_primaryVertices(new std::vector<ConstMCVertex>())
    { }

MCHeaderObj::MCHeaderObj(const podio::ObjectID id, MCHeaderData data) :
    ObjBase{id,0},
    data(data)
    { }

MCHeaderObj::MCHeaderObj(const MCHeaderObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_primaryVertices(new std::vector<ConstMCVertex>(*(other.m_m_primaryVertices)))
    { }

MCHeaderObj::~MCHeaderObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_primaryVertices;

  }
}
