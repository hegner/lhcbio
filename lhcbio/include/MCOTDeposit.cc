// datamodel specific includes
#include "MCOTDeposit.h"
#include "MCOTDepositConst.h"
#include "MCOTDepositObj.h"
#include "MCOTDepositData.h"
#include "MCOTDepositCollection.h"
#include <iostream>
#include "MCHit.h"

MCOTDeposit::MCOTDeposit() : m_obj(new MCOTDepositObj()){
 m_obj->acquire();
};

MCOTDeposit::MCOTDeposit(int m_type,OTChannelID m_channel,double m_time,double m_driftDistance,int m_ambiguity) : m_obj(new MCOTDepositObj()){
 m_obj->acquire();
   m_obj->data.m_type = m_type;  m_obj->data.m_channel = m_channel;  m_obj->data.m_time = m_time;  m_obj->data.m_driftDistance = m_driftDistance;  m_obj->data.m_ambiguity = m_ambiguity;
};

MCOTDeposit::MCOTDeposit(const MCOTDeposit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCOTDeposit& MCOTDeposit::operator=(const MCOTDeposit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCOTDeposit::MCOTDeposit(MCOTDepositObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCOTDeposit MCOTDeposit::clone() const {
  return {new MCOTDepositObj(*m_obj)};
}

MCOTDeposit::~MCOTDeposit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCOTDeposit::operator ConstMCOTDeposit() const {return ConstMCOTDeposit(m_obj);};

  const int& MCOTDeposit::m_type() const { return m_obj->data.m_type; };
  const OTChannelID& MCOTDeposit::m_channel() const { return m_obj->data.m_channel; };
  const double& MCOTDeposit::m_time() const { return m_obj->data.m_time; };
  const double& MCOTDeposit::m_driftDistance() const { return m_obj->data.m_driftDistance; };
  const int& MCOTDeposit::m_ambiguity() const { return m_obj->data.m_ambiguity; };
  const ConstMCHit MCOTDeposit::m_mcHit() const { if (m_obj->m_m_mcHit == nullptr) {
 return ConstMCHit(nullptr);}
 return ConstMCHit(*(m_obj->m_m_mcHit));};

void MCOTDeposit::m_type(int value){ m_obj->data.m_type = value;}
  OTChannelID& MCOTDeposit::m_channel() { return m_obj->data.m_channel; };
void MCOTDeposit::m_channel(class OTChannelID value){ m_obj->data.m_channel = value;}
void MCOTDeposit::m_time(double value){ m_obj->data.m_time = value;}
void MCOTDeposit::m_driftDistance(double value){ m_obj->data.m_driftDistance = value;}
void MCOTDeposit::m_ambiguity(int value){ m_obj->data.m_ambiguity = value;}
void MCOTDeposit::m_mcHit(ConstMCHit value) { if (m_obj->m_m_mcHit != nullptr) delete m_obj->m_m_mcHit; m_obj->m_m_mcHit = new ConstMCHit(value); };


bool  MCOTDeposit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCOTDeposit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCOTDeposit::operator==(const ConstMCOTDeposit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCOTDeposit& p1, const MCOTDeposit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
