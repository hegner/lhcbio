// datamodel specific includes
#include "MCVPDigit.h"
#include "MCVPDigitConst.h"
#include "MCVPDigitObj.h"
#include "MCVPDigitData.h"
#include "MCVPDigitCollection.h"
#include <iostream>

MCVPDigit::MCVPDigit() : m_obj(new MCVPDigitObj()){
 m_obj->acquire();
};



MCVPDigit::MCVPDigit(const MCVPDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCVPDigit& MCVPDigit::operator=(const MCVPDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCVPDigit::MCVPDigit(MCVPDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCVPDigit MCVPDigit::clone() const {
  return {new MCVPDigitObj(*m_obj)};
}

MCVPDigit::~MCVPDigit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCVPDigit::operator ConstMCVPDigit() const {return ConstMCVPDigit(m_obj);};



std::vector<ConstMCHit>::const_iterator MCVPDigit::m_mcHits_begin() const {
  auto ret_value = m_obj->m_m_mcHits->begin();
  std::advance(ret_value, m_obj->data.m_mcHits_begin);
  return ret_value;
}

std::vector<ConstMCHit>::const_iterator MCVPDigit::m_mcHits_end() const {
  auto ret_value = m_obj->m_m_mcHits->begin();
  std::advance(ret_value, m_obj->data.m_mcHits_end-1);
  return ++ret_value;
}

void MCVPDigit::addm_mcHits(ConstMCHit component) {
  m_obj->m_m_mcHits->push_back(component);
  m_obj->data.m_mcHits_end++;
}

unsigned int MCVPDigit::m_mcHits_size() const {
  return (m_obj->data.m_mcHits_end-m_obj->data.m_mcHits_begin);
}

ConstMCHit MCVPDigit::m_mcHits(unsigned int index) const {
  if (m_mcHits_size() > index) {
    return m_obj->m_m_mcHits->at(m_obj->data.m_mcHits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<double>::const_iterator MCVPDigit::deposits_begin() const {
  auto ret_value = m_obj->m_deposits->begin();
  std::advance(ret_value, m_obj->data.deposits_begin);
  return ret_value;
}

std::vector<double>::const_iterator MCVPDigit::deposits_end() const {
  auto ret_value = m_obj->m_deposits->begin();
  std::advance(ret_value, m_obj->data.deposits_end-1);
  return ++ret_value;
}

void MCVPDigit::adddeposits(double component) {
  m_obj->m_deposits->push_back(component);
  m_obj->data.deposits_end++;
}

unsigned int MCVPDigit::deposits_size() const {
  return (m_obj->data.deposits_end-m_obj->data.deposits_begin);
}

double MCVPDigit::deposits(unsigned int index) const {
  if (deposits_size() > index) {
    return m_obj->m_deposits->at(m_obj->data.deposits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCVPDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCVPDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCVPDigit::operator==(const ConstMCVPDigit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCVPDigit& p1, const MCVPDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
