// datamodel specific includes
#include "MCRichSegment.h"
#include "MCRichSegmentConst.h"
#include "MCRichSegmentObj.h"
#include "MCRichSegmentData.h"
#include "MCRichSegmentCollection.h"
#include <iostream>
#include "MCParticle.h"
#include "MCRichTrack.h"

MCRichSegment::MCRichSegment() : m_obj(new MCRichSegmentObj()){
 m_obj->acquire();
};

MCRichSegment::MCRichSegment(unsigned m_historyCode) : m_obj(new MCRichSegmentObj()){
 m_obj->acquire();
   m_obj->data.m_historyCode = m_historyCode;
};

MCRichSegment::MCRichSegment(const MCRichSegment& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCRichSegment& MCRichSegment::operator=(const MCRichSegment& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCRichSegment::MCRichSegment(MCRichSegmentObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCRichSegment MCRichSegment::clone() const {
  return {new MCRichSegmentObj(*m_obj)};
}

MCRichSegment::~MCRichSegment(){
  if ( m_obj != nullptr) m_obj->release();
}

MCRichSegment::operator ConstMCRichSegment() const {return ConstMCRichSegment(m_obj);};

  const unsigned& MCRichSegment::m_historyCode() const { return m_obj->data.m_historyCode; };
  const ConstMCParticle MCRichSegment::m_mcParticle() const { if (m_obj->m_m_mcParticle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_mcParticle));};
  const ConstMCRichTrack MCRichSegment::m_MCRichTrack() const { if (m_obj->m_m_MCRichTrack == nullptr) {
 return ConstMCRichTrack(nullptr);}
 return ConstMCRichTrack(*(m_obj->m_m_MCRichTrack));};

void MCRichSegment::m_historyCode(unsigned value){ m_obj->data.m_historyCode = value;}
void MCRichSegment::m_mcParticle(ConstMCParticle value) { if (m_obj->m_m_mcParticle != nullptr) delete m_obj->m_m_mcParticle; m_obj->m_m_mcParticle = new ConstMCParticle(value); };
void MCRichSegment::m_MCRichTrack(ConstMCRichTrack value) { if (m_obj->m_m_MCRichTrack != nullptr) delete m_obj->m_m_MCRichTrack; m_obj->m_m_MCRichTrack = new ConstMCRichTrack(value); };

std::vector<ConstMCRichOpticalPhoton>::const_iterator MCRichSegment::m_MCRichOpticalPhotons_begin() const {
  auto ret_value = m_obj->m_m_MCRichOpticalPhotons->begin();
  std::advance(ret_value, m_obj->data.m_MCRichOpticalPhotons_begin);
  return ret_value;
}

std::vector<ConstMCRichOpticalPhoton>::const_iterator MCRichSegment::m_MCRichOpticalPhotons_end() const {
  auto ret_value = m_obj->m_m_MCRichOpticalPhotons->begin();
  std::advance(ret_value, m_obj->data.m_MCRichOpticalPhotons_end-1);
  return ++ret_value;
}

void MCRichSegment::addm_MCRichOpticalPhotons(ConstMCRichOpticalPhoton component) {
  m_obj->m_m_MCRichOpticalPhotons->push_back(component);
  m_obj->data.m_MCRichOpticalPhotons_end++;
}

unsigned int MCRichSegment::m_MCRichOpticalPhotons_size() const {
  return (m_obj->data.m_MCRichOpticalPhotons_end-m_obj->data.m_MCRichOpticalPhotons_begin);
}

ConstMCRichOpticalPhoton MCRichSegment::m_MCRichOpticalPhotons(unsigned int index) const {
  if (m_MCRichOpticalPhotons_size() > index) {
    return m_obj->m_m_MCRichOpticalPhotons->at(m_obj->data.m_MCRichOpticalPhotons_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<ConstMCRichHit>::const_iterator MCRichSegment::m_MCRichHits_begin() const {
  auto ret_value = m_obj->m_m_MCRichHits->begin();
  std::advance(ret_value, m_obj->data.m_MCRichHits_begin);
  return ret_value;
}

std::vector<ConstMCRichHit>::const_iterator MCRichSegment::m_MCRichHits_end() const {
  auto ret_value = m_obj->m_m_MCRichHits->begin();
  std::advance(ret_value, m_obj->data.m_MCRichHits_end-1);
  return ++ret_value;
}

void MCRichSegment::addm_MCRichHits(ConstMCRichHit component) {
  m_obj->m_m_MCRichHits->push_back(component);
  m_obj->data.m_MCRichHits_end++;
}

unsigned int MCRichSegment::m_MCRichHits_size() const {
  return (m_obj->data.m_MCRichHits_end-m_obj->data.m_MCRichHits_begin);
}

ConstMCRichHit MCRichSegment::m_MCRichHits(unsigned int index) const {
  if (m_MCRichHits_size() > index) {
    return m_obj->m_m_MCRichHits->at(m_obj->data.m_MCRichHits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<XYZPoint>::const_iterator MCRichSegment::trajectoryPoints_begin() const {
  auto ret_value = m_obj->m_trajectoryPoints->begin();
  std::advance(ret_value, m_obj->data.trajectoryPoints_begin);
  return ret_value;
}

std::vector<XYZPoint>::const_iterator MCRichSegment::trajectoryPoints_end() const {
  auto ret_value = m_obj->m_trajectoryPoints->begin();
  std::advance(ret_value, m_obj->data.trajectoryPoints_end-1);
  return ++ret_value;
}

void MCRichSegment::addtrajectoryPoints(XYZPoint component) {
  m_obj->m_trajectoryPoints->push_back(component);
  m_obj->data.trajectoryPoints_end++;
}

unsigned int MCRichSegment::trajectoryPoints_size() const {
  return (m_obj->data.trajectoryPoints_end-m_obj->data.trajectoryPoints_begin);
}

XYZPoint MCRichSegment::trajectoryPoints(unsigned int index) const {
  if (trajectoryPoints_size() > index) {
    return m_obj->m_trajectoryPoints->at(m_obj->data.trajectoryPoints_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}
std::vector<XYZVector>::const_iterator MCRichSegment::trajectoryMomenta_begin() const {
  auto ret_value = m_obj->m_trajectoryMomenta->begin();
  std::advance(ret_value, m_obj->data.trajectoryMomenta_begin);
  return ret_value;
}

std::vector<XYZVector>::const_iterator MCRichSegment::trajectoryMomenta_end() const {
  auto ret_value = m_obj->m_trajectoryMomenta->begin();
  std::advance(ret_value, m_obj->data.trajectoryMomenta_end-1);
  return ++ret_value;
}

void MCRichSegment::addtrajectoryMomenta(XYZVector component) {
  m_obj->m_trajectoryMomenta->push_back(component);
  m_obj->data.trajectoryMomenta_end++;
}

unsigned int MCRichSegment::trajectoryMomenta_size() const {
  return (m_obj->data.trajectoryMomenta_end-m_obj->data.trajectoryMomenta_begin);
}

XYZVector MCRichSegment::trajectoryMomenta(unsigned int index) const {
  if (trajectoryMomenta_size() > index) {
    return m_obj->m_trajectoryMomenta->at(m_obj->data.trajectoryMomenta_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCRichSegment::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCRichSegment::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCRichSegment::operator==(const ConstMCRichSegment& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCRichSegment& p1, const MCRichSegment& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
