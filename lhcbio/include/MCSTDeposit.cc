// datamodel specific includes
#include "MCSTDeposit.h"
#include "MCSTDepositConst.h"
#include "MCSTDepositObj.h"
#include "MCSTDepositData.h"
#include "MCSTDepositCollection.h"
#include <iostream>
#include "MCHit.h"

MCSTDeposit::MCSTDeposit() : m_obj(new MCSTDepositObj()){
 m_obj->acquire();
};

MCSTDeposit::MCSTDeposit(double m_depositedCharge,STChannelID m_channelID) : m_obj(new MCSTDepositObj()){
 m_obj->acquire();
   m_obj->data.m_depositedCharge = m_depositedCharge;  m_obj->data.m_channelID = m_channelID;
};

MCSTDeposit::MCSTDeposit(const MCSTDeposit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCSTDeposit& MCSTDeposit::operator=(const MCSTDeposit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCSTDeposit::MCSTDeposit(MCSTDepositObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCSTDeposit MCSTDeposit::clone() const {
  return {new MCSTDepositObj(*m_obj)};
}

MCSTDeposit::~MCSTDeposit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCSTDeposit::operator ConstMCSTDeposit() const {return ConstMCSTDeposit(m_obj);};

  const double& MCSTDeposit::m_depositedCharge() const { return m_obj->data.m_depositedCharge; };
  const STChannelID& MCSTDeposit::m_channelID() const { return m_obj->data.m_channelID; };
  const ConstMCHit MCSTDeposit::m_mcHit() const { if (m_obj->m_m_mcHit == nullptr) {
 return ConstMCHit(nullptr);}
 return ConstMCHit(*(m_obj->m_m_mcHit));};

void MCSTDeposit::m_depositedCharge(double value){ m_obj->data.m_depositedCharge = value;}
  STChannelID& MCSTDeposit::m_channelID() { return m_obj->data.m_channelID; };
void MCSTDeposit::m_channelID(class STChannelID value){ m_obj->data.m_channelID = value;}
void MCSTDeposit::m_mcHit(ConstMCHit value) { if (m_obj->m_m_mcHit != nullptr) delete m_obj->m_m_mcHit; m_obj->m_m_mcHit = new ConstMCHit(value); };


bool  MCSTDeposit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCSTDeposit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCSTDeposit::operator==(const ConstMCSTDeposit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCSTDeposit& p1, const MCSTDeposit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
