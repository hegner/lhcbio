// standard includes
#include <stdexcept>
#include "MCVertexCollection.h" 
#include "MCVertexCollection.h" 


#include "MCParticleCollection.h"

MCParticleCollection::MCParticleCollection() : m_collectionID(0), m_entries() ,m_rel_m_endVertices(new std::vector<ConstMCVertex>()),m_rel_m_originVertex(new std::vector<ConstMCVertex>()),m_refCollections(nullptr), m_data(new MCParticleDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCParticle MCParticleCollection::operator[](unsigned int index) const {
  return MCParticle(m_entries[index]);
}

const MCParticle MCParticleCollection::at(unsigned int index) const {
  return MCParticle(m_entries.at(index));
}

int  MCParticleCollection::size() const {
  return m_entries.size();
}

MCParticle MCParticleCollection::create(){
  auto obj = new MCParticleObj();
  m_entries.emplace_back(obj);
  m_rel_m_endVertices_tmp.push_back(obj->m_m_endVertices);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCParticle(obj);
}

void MCParticleCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  // clear relations to m_endVertices. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_endVertices_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_endVertices_tmp.clear();
  for (auto& item : (*m_rel_m_endVertices)) {item.unlink(); }
  m_rel_m_endVertices->clear();
  for (auto& item : (*m_rel_m_originVertex)) {item.unlink(); }
  m_rel_m_originVertex->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCParticleCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
     (*m_data)[i].m_endVertices_begin=index;
   (*m_data)[i].m_endVertices_end+=index;
   index = (*m_data)[index].m_endVertices_end;
   for(auto it : (*m_rel_m_endVertices_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[0]->emplace_back(it.getObjectID());
     m_rel_m_endVertices->push_back(it);
   }

  }
    for (auto& obj : m_entries) {
if (obj->m_m_originVertex != nullptr){
(*m_refCollections)[1]->emplace_back(obj->m_m_originVertex->getObjectID());} else {(*m_refCollections)[1]->push_back({-2,-2}); } };

}

void MCParticleCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCParticleObj({index,m_collectionID}, data);
        obj->m_m_endVertices = m_rel_m_endVertices;
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCParticleCollection::setReferences(const podio::ICollectionProvider* collectionProvider){
  for(unsigned int i=0, size=(*m_refCollections)[0]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCVertexCollection* tmp_coll = static_cast<MCVertexCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_endVertices->emplace_back(tmp);
  }

  for(unsigned int i=0, size=m_entries.size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[1])[i];
    if (id.index != podio::ObjectID::invalid) {
      CollectionBase* coll = nullptr;
      collectionProvider->get(id.collectionID,coll);
      MCVertexCollection* tmp_coll = static_cast<MCVertexCollection*>(coll);
      m_entries[i]->m_m_originVertex = new ConstMCVertex((*tmp_coll)[id.index]);
    } else {
      m_entries[i]->m_m_originVertex = nullptr;
    }
  }

  return true; //TODO: check success
}

void MCParticleCollection::push_back(ConstMCParticle object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
          m_rel_m_endVertices_tmp.push_back(obj->m_m_endVertices);

    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCParticleCollection::setBuffer(void* address){
  m_data = static_cast<MCParticleDataContainer*>(address);
}


const MCParticle MCParticleCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCParticle* MCParticleCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCParticleCollectionIterator& MCParticleCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
