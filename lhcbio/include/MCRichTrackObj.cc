#include "MCRichTrackObj.h"
#include "MCParticleConst.h"


MCRichTrackObj::MCRichTrackObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_mcParticle(nullptr)
,m_m_mcSegments(new std::vector<ConstMCRichSegment>())
    { }

MCRichTrackObj::MCRichTrackObj(const podio::ObjectID id, MCRichTrackData data) :
    ObjBase{id,0},
    data(data)
    { }

MCRichTrackObj::MCRichTrackObj(const MCRichTrackObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    ,m_m_mcSegments(new std::vector<ConstMCRichSegment>(*(other.m_m_mcSegments)))
    { }

MCRichTrackObj::~MCRichTrackObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_mcParticle;
delete m_m_mcSegments;

  }
}
