#include "MCSensPlaneHitObj.h"
#include "MCParticleConst.h"


MCSensPlaneHitObj::MCSensPlaneHitObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_particle(nullptr)

    { }

MCSensPlaneHitObj::MCSensPlaneHitObj(const podio::ObjectID id, MCSensPlaneHitData data) :
    ObjBase{id,0},
    data(data)
    { }

MCSensPlaneHitObj::MCSensPlaneHitObj(const MCSensPlaneHitObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCSensPlaneHitObj::~MCSensPlaneHitObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_particle;

  }
}
