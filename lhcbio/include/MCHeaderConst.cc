// datamodel specific includes
#include "MCHeader.h"
#include "MCHeaderConst.h"
#include "MCHeaderObj.h"
#include "MCHeaderData.h"
#include "MCHeaderCollection.h"
#include <iostream>

ConstMCHeader::ConstMCHeader() : m_obj(new MCHeaderObj()){
 m_obj->acquire();
};

ConstMCHeader::ConstMCHeader(int m_evtNumber,unsigned m_evtTime) : m_obj(new MCHeaderObj()){
 m_obj->acquire();
   m_obj->data.m_evtNumber = m_evtNumber;  m_obj->data.m_evtTime = m_evtTime;
};


ConstMCHeader::ConstMCHeader(const ConstMCHeader& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCHeader& ConstMCHeader::operator=(const ConstMCHeader& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCHeader::ConstMCHeader(MCHeaderObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCHeader ConstMCHeader::clone() const {
  return {new MCHeaderObj(*m_obj)};
}

ConstMCHeader::~ConstMCHeader(){
  if ( m_obj != nullptr) m_obj->release();
}


std::vector<ConstMCVertex>::const_iterator ConstMCHeader::m_primaryVertices_begin() const {
  auto ret_value = m_obj->m_m_primaryVertices->begin();
  std::advance(ret_value, m_obj->data.m_primaryVertices_begin);
  return ret_value;
}

std::vector<ConstMCVertex>::const_iterator ConstMCHeader::m_primaryVertices_end() const {
  auto ret_value = m_obj->m_m_primaryVertices->begin();
  std::advance(ret_value, m_obj->data.m_primaryVertices_end-1);
  return ++ret_value;
}

unsigned int ConstMCHeader::m_primaryVertices_size() const {
  return (m_obj->data.m_primaryVertices_end-m_obj->data.m_primaryVertices_begin);
}

ConstMCVertex ConstMCHeader::m_primaryVertices(unsigned int index) const {
  if (m_primaryVertices_size() > index) {
    return m_obj->m_m_primaryVertices->at(m_obj->data.m_primaryVertices_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCHeader::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCHeader::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCHeader::operator==(const MCHeader& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCHeader& p1, const MCHeader& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
