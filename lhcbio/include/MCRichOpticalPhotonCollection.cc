// standard includes
#include <stdexcept>
#include "MCRichHitCollection.h" 


#include "MCRichOpticalPhotonCollection.h"

MCRichOpticalPhotonCollection::MCRichOpticalPhotonCollection() : m_collectionID(0), m_entries() ,m_rel_m_mcRichHit(new std::vector<ConstMCRichHit>()),m_refCollections(nullptr), m_data(new MCRichOpticalPhotonDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCRichOpticalPhoton MCRichOpticalPhotonCollection::operator[](unsigned int index) const {
  return MCRichOpticalPhoton(m_entries[index]);
}

const MCRichOpticalPhoton MCRichOpticalPhotonCollection::at(unsigned int index) const {
  return MCRichOpticalPhoton(m_entries.at(index));
}

int  MCRichOpticalPhotonCollection::size() const {
  return m_entries.size();
}

MCRichOpticalPhoton MCRichOpticalPhotonCollection::create(){
  auto obj = new MCRichOpticalPhotonObj();
  m_entries.emplace_back(obj);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCRichOpticalPhoton(obj);
}

void MCRichOpticalPhotonCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  for (auto& item : (*m_rel_m_mcRichHit)) {item.unlink(); }
  m_rel_m_mcRichHit->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCRichOpticalPhotonCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
  
  }
    for (auto& obj : m_entries) {
if (obj->m_m_mcRichHit != nullptr){
(*m_refCollections)[0]->emplace_back(obj->m_m_mcRichHit->getObjectID());} else {(*m_refCollections)[0]->push_back({-2,-2}); } };

}

void MCRichOpticalPhotonCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCRichOpticalPhotonObj({index,m_collectionID}, data);
    
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCRichOpticalPhotonCollection::setReferences(const podio::ICollectionProvider* collectionProvider){

  for(unsigned int i=0, size=m_entries.size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    if (id.index != podio::ObjectID::invalid) {
      CollectionBase* coll = nullptr;
      collectionProvider->get(id.collectionID,coll);
      MCRichHitCollection* tmp_coll = static_cast<MCRichHitCollection*>(coll);
      m_entries[i]->m_m_mcRichHit = new ConstMCRichHit((*tmp_coll)[id.index]);
    } else {
      m_entries[i]->m_m_mcRichHit = nullptr;
    }
  }

  return true; //TODO: check success
}

void MCRichOpticalPhotonCollection::push_back(ConstMCRichOpticalPhoton object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
        
    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCRichOpticalPhotonCollection::setBuffer(void* address){
  m_data = static_cast<MCRichOpticalPhotonDataContainer*>(address);
}


const MCRichOpticalPhoton MCRichOpticalPhotonCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCRichOpticalPhoton* MCRichOpticalPhotonCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCRichOpticalPhotonCollectionIterator& MCRichOpticalPhotonCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
