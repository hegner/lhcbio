#ifndef GhostTrackInfoDATA_H
#define GhostTrackInfoDATA_H

// /<
// author: M. Needham

#include "LinkMap.h"
#include "Classification.h"


class GhostTrackInfoData {
public:
  LinkMap m_linkMap; ////< mapping of particles 
  Classification m_classification; ////< 

};

#endif
