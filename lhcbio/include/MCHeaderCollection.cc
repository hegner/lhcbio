// standard includes
#include <stdexcept>
#include "MCVertexCollection.h" 


#include "MCHeaderCollection.h"

MCHeaderCollection::MCHeaderCollection() : m_collectionID(0), m_entries() ,m_rel_m_primaryVertices(new std::vector<ConstMCVertex>()),m_refCollections(nullptr), m_data(new MCHeaderDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCHeader MCHeaderCollection::operator[](unsigned int index) const {
  return MCHeader(m_entries[index]);
}

const MCHeader MCHeaderCollection::at(unsigned int index) const {
  return MCHeader(m_entries.at(index));
}

int  MCHeaderCollection::size() const {
  return m_entries.size();
}

MCHeader MCHeaderCollection::create(){
  auto obj = new MCHeaderObj();
  m_entries.emplace_back(obj);
  m_rel_m_primaryVertices_tmp.push_back(obj->m_m_primaryVertices);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCHeader(obj);
}

void MCHeaderCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  // clear relations to m_primaryVertices. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_primaryVertices_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_primaryVertices_tmp.clear();
  for (auto& item : (*m_rel_m_primaryVertices)) {item.unlink(); }
  m_rel_m_primaryVertices->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCHeaderCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
     (*m_data)[i].m_primaryVertices_begin=index;
   (*m_data)[i].m_primaryVertices_end+=index;
   index = (*m_data)[index].m_primaryVertices_end;
   for(auto it : (*m_rel_m_primaryVertices_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[0]->emplace_back(it.getObjectID());
     m_rel_m_primaryVertices->push_back(it);
   }

  }
  
}

void MCHeaderCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCHeaderObj({index,m_collectionID}, data);
        obj->m_m_primaryVertices = m_rel_m_primaryVertices;
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCHeaderCollection::setReferences(const podio::ICollectionProvider* collectionProvider){
  for(unsigned int i=0, size=(*m_refCollections)[0]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCVertexCollection* tmp_coll = static_cast<MCVertexCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_primaryVertices->emplace_back(tmp);
  }


  return true; //TODO: check success
}

void MCHeaderCollection::push_back(ConstMCHeader object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
          m_rel_m_primaryVertices_tmp.push_back(obj->m_m_primaryVertices);

    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCHeaderCollection::setBuffer(void* address){
  m_data = static_cast<MCHeaderDataContainer*>(address);
}


const MCHeader MCHeaderCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCHeader* MCHeaderCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCHeaderCollectionIterator& MCHeaderCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
