#ifndef ConstMCMuonHitHistory_H
#define ConstMCMuonHitHistory_H
#include "MCMuonHitHistoryData.h"

#include <vector>
#include "podio/ObjectID.h"

// /< hit history bit pattern
// author: Alessia Satta

//forward declarations
class MCMuonHitHistoryObj;
class MCMuonHitHistory;
class MCMuonHitHistoryCollection;
class MCMuonHitHistoryCollectionIterator;


#include "MCMuonHitHistoryObj.h"

class ConstMCMuonHitHistory {

  friend MCMuonHitHistory;
  friend MCMuonHitHistoryCollection;
  friend MCMuonHitHistoryCollectionIterator;

public:

  /// default constructor
  ConstMCMuonHitHistory();
  ConstMCMuonHitHistory(unsigned m_hitHistory);

  /// constructor from existing MCMuonHitHistoryObj
  ConstMCMuonHitHistory(MCMuonHitHistoryObj* obj);
  /// copy constructor
  ConstMCMuonHitHistory(const ConstMCMuonHitHistory& other);
  /// copy-assignment operator
  ConstMCMuonHitHistory& operator=(const ConstMCMuonHitHistory& other);
  /// support cloning (deep-copy)
  ConstMCMuonHitHistory clone() const;
  /// destructor
  ~ConstMCMuonHitHistory();


public:

  const unsigned& m_hitHistory() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCMuonHitHistoryObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCMuonHitHistory& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCMuonHitHistory& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCMuonHitHistory& p1,
//       const MCMuonHitHistory& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCMuonHitHistoryObj* m_obj;

};

#endif
