//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCRichSegmentCollection_H
#define  MCRichSegmentCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCRichSegmentData.h"
#include "MCRichSegment.h"
#include "MCRichSegmentObj.h"

typedef std::vector<MCRichSegmentData> MCRichSegmentDataContainer;
typedef std::deque<MCRichSegmentObj*> MCRichSegmentObjPointerContainer;

class MCRichSegmentCollectionIterator {

  public:
    MCRichSegmentCollectionIterator(int index, const MCRichSegmentObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCRichSegmentCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCRichSegment operator*() const;
    const MCRichSegment* operator->() const;
    const MCRichSegmentCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCRichSegment m_object;
    const MCRichSegmentObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCRichSegmentCollection : public podio::CollectionBase {

public:
  typedef const MCRichSegmentCollectionIterator const_iterator;

  MCRichSegmentCollection();
//  MCRichSegmentCollection(const MCRichSegmentCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCRichSegmentCollection(MCRichSegmentVector* data, int collectionID);
  ~MCRichSegmentCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCRichSegment create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCRichSegment create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCRichSegment operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCRichSegment at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCRichSegment object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCRichSegmentData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<unsigned,arraysize> m_historyCode() const;


private:
  int m_collectionID;
  MCRichSegmentObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCRichOpticalPhoton>* m_rel_m_MCRichOpticalPhotons; //relation buffer for r/w
  std::vector<std::vector<ConstMCRichOpticalPhoton>*> m_rel_m_MCRichOpticalPhotons_tmp;
   std::vector<ConstMCRichHit>* m_rel_m_MCRichHits; //relation buffer for r/w
  std::vector<std::vector<ConstMCRichHit>*> m_rel_m_MCRichHits_tmp;
   std::vector<ConstMCParticle>* m_rel_m_mcParticle; //relation buffer for r/w
  std::vector<ConstMCRichTrack>* m_rel_m_MCRichTrack; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCRichSegmentDataContainer* m_data;
};

template<typename... Args>
MCRichSegment  MCRichSegmentCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCRichSegmentObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCRichSegment(obj);
}

template<size_t arraysize>
const std::array<unsigned,arraysize> MCRichSegmentCollection::m_historyCode() const {
  std::array<unsigned,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_historyCode;
 }
 return tmp;
}


#endif
