#ifndef MCRichDigitHit_H
#define MCRichDigitHit_H
#include "MCRichDigitHitData.h"
#include "MCRichDigitHistoryCode.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Bit-packed word containing the overall history information for this hit. A summary of both how the hit was produced and also how it was used in the digitisation
// author: Chris Jones   Christopher.Rob.Jones@cern.ch

//forward declarations
class MCRichDigitHitCollection;
class MCRichDigitHitCollectionIterator;
class ConstMCRichDigitHit;
class MCRichHit;
class ConstMCRichHit;


#include "MCRichDigitHitConst.h"
#include "MCRichDigitHitObj.h"

class MCRichDigitHit {

  friend MCRichDigitHitCollection;
  friend MCRichDigitHitCollectionIterator;
  friend ConstMCRichDigitHit;

public:

  /// default constructor
  MCRichDigitHit();
    MCRichDigitHit(MCRichDigitHistoryCode m_history);

  /// constructor from existing MCRichDigitHitObj
  MCRichDigitHit(MCRichDigitHitObj* obj);
  /// copy constructor
  MCRichDigitHit(const MCRichDigitHit& other);
  /// copy-assignment operator
  MCRichDigitHit& operator=(const MCRichDigitHit& other);
  /// support cloning (deep-copy)
  MCRichDigitHit clone() const;
  /// destructor
  ~MCRichDigitHit();

  /// conversion to const object
  operator ConstMCRichDigitHit () const;

public:

  const MCRichDigitHistoryCode& m_history() const;
  const ConstMCRichHit m_mcRichHit() const;

  MCRichDigitHistoryCode& m_history();
  void m_history(class MCRichDigitHistoryCode value);
  void m_mcRichHit(ConstMCRichHit value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichDigitHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCRichDigitHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCRichDigitHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichDigitHit& p1,
//       const MCRichDigitHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichDigitHitObj* m_obj;

};

#endif
