#ifndef MCSTDepositDATA_H
#define MCSTDepositDATA_H

// /< channelID
// author: Matthew Needham

#include "STChannelID.h"


class MCSTDepositData {
public:
  double m_depositedCharge; ////< charge deposited on strip 
  STChannelID m_channelID; ////< channelID 

};

#endif
