#include "MCSTDepositObj.h"
#include "MCHitConst.h"


MCSTDepositObj::MCSTDepositObj() :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data()
    ,m_m_mcHit(nullptr)

    { }

MCSTDepositObj::MCSTDepositObj(const podio::ObjectID id, MCSTDepositData data) :
    ObjBase{id,0},
    data(data)
    { }

MCSTDepositObj::MCSTDepositObj(const MCSTDepositObj& other) :
    ObjBase{{podio::ObjectID::untracked,podio::ObjectID::untracked},0}
    ,data(other.data)
    
    { }

MCSTDepositObj::~MCSTDepositObj() {
  if (id.index == podio::ObjectID::untracked) {
delete m_m_mcHit;

  }
}
