// standard includes
#include <stdexcept>
#include "MCHitCollection.h" 


#include "MCSTDepositCollection.h"

MCSTDepositCollection::MCSTDepositCollection() : m_collectionID(0), m_entries() ,m_rel_m_mcHit(new std::vector<ConstMCHit>()),m_refCollections(nullptr), m_data(new MCSTDepositDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCSTDeposit MCSTDepositCollection::operator[](unsigned int index) const {
  return MCSTDeposit(m_entries[index]);
}

const MCSTDeposit MCSTDepositCollection::at(unsigned int index) const {
  return MCSTDeposit(m_entries.at(index));
}

int  MCSTDepositCollection::size() const {
  return m_entries.size();
}

MCSTDeposit MCSTDepositCollection::create(){
  auto obj = new MCSTDepositObj();
  m_entries.emplace_back(obj);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCSTDeposit(obj);
}

void MCSTDepositCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  for (auto& item : (*m_rel_m_mcHit)) {item.unlink(); }
  m_rel_m_mcHit->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCSTDepositCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
  
  }
    for (auto& obj : m_entries) {
if (obj->m_m_mcHit != nullptr){
(*m_refCollections)[0]->emplace_back(obj->m_m_mcHit->getObjectID());} else {(*m_refCollections)[0]->push_back({-2,-2}); } };

}

void MCSTDepositCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCSTDepositObj({index,m_collectionID}, data);
    
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCSTDepositCollection::setReferences(const podio::ICollectionProvider* collectionProvider){

  for(unsigned int i=0, size=m_entries.size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    if (id.index != podio::ObjectID::invalid) {
      CollectionBase* coll = nullptr;
      collectionProvider->get(id.collectionID,coll);
      MCHitCollection* tmp_coll = static_cast<MCHitCollection*>(coll);
      m_entries[i]->m_m_mcHit = new ConstMCHit((*tmp_coll)[id.index]);
    } else {
      m_entries[i]->m_m_mcHit = nullptr;
    }
  }

  return true; //TODO: check success
}

void MCSTDepositCollection::push_back(ConstMCSTDeposit object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
        
    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCSTDepositCollection::setBuffer(void* address){
  m_data = static_cast<MCSTDepositDataContainer*>(address);
}


const MCSTDeposit MCSTDepositCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCSTDeposit* MCSTDepositCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCSTDepositCollectionIterator& MCSTDepositCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
