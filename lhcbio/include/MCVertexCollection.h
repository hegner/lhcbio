//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCVertexCollection_H
#define  MCVertexCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCVertexData.h"
#include "MCVertex.h"
#include "MCVertexObj.h"

typedef std::vector<MCVertexData> MCVertexDataContainer;
typedef std::deque<MCVertexObj*> MCVertexObjPointerContainer;

class MCVertexCollectionIterator {

  public:
    MCVertexCollectionIterator(int index, const MCVertexObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCVertexCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCVertex operator*() const;
    const MCVertex* operator->() const;
    const MCVertexCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCVertex m_object;
    const MCVertexObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCVertexCollection : public podio::CollectionBase {

public:
  typedef const MCVertexCollectionIterator const_iterator;

  MCVertexCollection();
//  MCVertexCollection(const MCVertexCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCVertexCollection(MCVertexVector* data, int collectionID);
  ~MCVertexCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCVertex create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCVertex create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCVertex operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCVertex at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCVertex object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCVertexData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<XYZPoint,arraysize> m_position() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_time() const;
  template<size_t arraysize>  
  const std::array<MCVertexType,arraysize> m_type() const;


private:
  int m_collectionID;
  MCVertexObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCParticle>* m_rel_m_products; //relation buffer for r/w
  std::vector<std::vector<ConstMCParticle>*> m_rel_m_products_tmp;
   std::vector<ConstMCParticle>* m_rel_m_mother; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCVertexDataContainer* m_data;
};

template<typename... Args>
MCVertex  MCVertexCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCVertexObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCVertex(obj);
}

template<size_t arraysize>
const std::array<XYZPoint,arraysize> MCVertexCollection::m_position() const {
  std::array<XYZPoint,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_position;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCVertexCollection::m_time() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_time;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<MCVertexType,arraysize> MCVertexCollection::m_type() const {
  std::array<MCVertexType,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_type;
 }
 return tmp;
}


#endif
