#ifndef MCRichOpticalPhoton_H
#define MCRichOpticalPhoton_H
#include "MCRichOpticalPhotonData.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZPoint.h"
#include "XYZVector.h"
#include "XYZPoint.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Photon incidence point on the external surface of the HPD Quartz Window
// author: Chris Jones   (Christopher.Rob.Jones@cern.ch)

//forward declarations
class MCRichOpticalPhotonCollection;
class MCRichOpticalPhotonCollectionIterator;
class ConstMCRichOpticalPhoton;
class MCRichHit;
class ConstMCRichHit;


#include "MCRichOpticalPhotonConst.h"
#include "MCRichOpticalPhotonObj.h"

class MCRichOpticalPhoton {

  friend MCRichOpticalPhotonCollection;
  friend MCRichOpticalPhotonCollectionIterator;
  friend ConstMCRichOpticalPhoton;

public:

  /// default constructor
  MCRichOpticalPhoton();
    MCRichOpticalPhoton(XYZPoint m_pdIncidencePoint,XYZPoint m_sphericalMirrorReflectPoint,XYZPoint m_flatMirrorReflectPoint,XYZPoint m_aerogelExitPoint,float m_cherenkovTheta,float m_cherenkovPhi,XYZPoint m_emissionPoint,float m_energyAtProduction,XYZVector m_parentMomentum,XYZPoint m_hpdQWIncidencePoint);

  /// constructor from existing MCRichOpticalPhotonObj
  MCRichOpticalPhoton(MCRichOpticalPhotonObj* obj);
  /// copy constructor
  MCRichOpticalPhoton(const MCRichOpticalPhoton& other);
  /// copy-assignment operator
  MCRichOpticalPhoton& operator=(const MCRichOpticalPhoton& other);
  /// support cloning (deep-copy)
  MCRichOpticalPhoton clone() const;
  /// destructor
  ~MCRichOpticalPhoton();

  /// conversion to const object
  operator ConstMCRichOpticalPhoton () const;

public:

  const XYZPoint& m_pdIncidencePoint() const;
  const XYZPoint& m_sphericalMirrorReflectPoint() const;
  const XYZPoint& m_flatMirrorReflectPoint() const;
  const XYZPoint& m_aerogelExitPoint() const;
  const float& m_cherenkovTheta() const;
  const float& m_cherenkovPhi() const;
  const XYZPoint& m_emissionPoint() const;
  const float& m_energyAtProduction() const;
  const XYZVector& m_parentMomentum() const;
  const XYZPoint& m_hpdQWIncidencePoint() const;
  const ConstMCRichHit m_mcRichHit() const;

  XYZPoint& m_pdIncidencePoint();
  void m_pdIncidencePoint(class XYZPoint value);
  XYZPoint& m_sphericalMirrorReflectPoint();
  void m_sphericalMirrorReflectPoint(class XYZPoint value);
  XYZPoint& m_flatMirrorReflectPoint();
  void m_flatMirrorReflectPoint(class XYZPoint value);
  XYZPoint& m_aerogelExitPoint();
  void m_aerogelExitPoint(class XYZPoint value);
  void m_cherenkovTheta(float value);
  void m_cherenkovPhi(float value);
  XYZPoint& m_emissionPoint();
  void m_emissionPoint(class XYZPoint value);
  void m_energyAtProduction(float value);
  XYZVector& m_parentMomentum();
  void m_parentMomentum(class XYZVector value);
  XYZPoint& m_hpdQWIncidencePoint();
  void m_hpdQWIncidencePoint(class XYZPoint value);
  void m_mcRichHit(ConstMCRichHit value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCRichOpticalPhotonObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCRichOpticalPhoton& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCRichOpticalPhoton& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCRichOpticalPhoton& p1,
//       const MCRichOpticalPhoton& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCRichOpticalPhotonObj* m_obj;

};

#endif
