#ifndef ConstMCExtendedHit_H
#define ConstMCExtendedHit_H
#include "MCExtendedHitData.h"
#include "XYZVector.h"

#include <vector>
#include "podio/ObjectID.h"

// /< momentum-vector of particle when entering the detector
// author: G.Corti

//forward declarations
class MCExtendedHitObj;
class MCExtendedHit;
class MCExtendedHitCollection;
class MCExtendedHitCollectionIterator;


#include "MCExtendedHitObj.h"

class ConstMCExtendedHit {

  friend MCExtendedHit;
  friend MCExtendedHitCollection;
  friend MCExtendedHitCollectionIterator;

public:

  /// default constructor
  ConstMCExtendedHit();
  ConstMCExtendedHit(XYZVector m_momentum);

  /// constructor from existing MCExtendedHitObj
  ConstMCExtendedHit(MCExtendedHitObj* obj);
  /// copy constructor
  ConstMCExtendedHit(const ConstMCExtendedHit& other);
  /// copy-assignment operator
  ConstMCExtendedHit& operator=(const ConstMCExtendedHit& other);
  /// support cloning (deep-copy)
  ConstMCExtendedHit clone() const;
  /// destructor
  ~ConstMCExtendedHit();


public:

  const XYZVector& m_momentum() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCExtendedHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCExtendedHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCExtendedHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCExtendedHit& p1,
//       const MCExtendedHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCExtendedHitObj* m_obj;

};

#endif
