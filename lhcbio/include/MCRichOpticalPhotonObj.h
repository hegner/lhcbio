#ifndef MCRichOpticalPhotonOBJ_H
#define MCRichOpticalPhotonOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCRichOpticalPhotonData.h"



// forward declarations
class MCRichOpticalPhoton;
class ConstMCRichOpticalPhoton;
class ConstMCRichHit;


class MCRichOpticalPhotonObj : public podio::ObjBase {
public:
  /// constructor
  MCRichOpticalPhotonObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCRichOpticalPhotonObj(const MCRichOpticalPhotonObj&);
  /// constructor from ObjectID and MCRichOpticalPhotonData
  /// does not initialize the internal relation containers
  MCRichOpticalPhotonObj(const podio::ObjectID id, MCRichOpticalPhotonData data);
  virtual ~MCRichOpticalPhotonObj();

public:
  MCRichOpticalPhotonData data;
  ConstMCRichHit* m_m_mcRichHit;


};


#endif
