#ifndef MCVeloFE_H
#define MCVeloFE_H
#include "MCVeloFEData.h"
#include <vector>
#include "MCHit.h"
#include <vector>

#include <vector>
#include "podio/ObjectID.h"

// /< common mode noise added in Si/FE chip simulation in units of electrons
// author: Chris Parkes, update Tomasz Szumlak

//forward declarations
class MCVeloFECollection;
class MCVeloFECollectionIterator;
class ConstMCVeloFE;


#include "MCVeloFEConst.h"
#include "MCVeloFEObj.h"

class MCVeloFE {

  friend MCVeloFECollection;
  friend MCVeloFECollectionIterator;
  friend ConstMCVeloFE;

public:

  /// default constructor
  MCVeloFE();
    MCVeloFE(double m_addedSignal,double m_addedPedestal,double m_addedNoise,double m_addedCMNoise);

  /// constructor from existing MCVeloFEObj
  MCVeloFE(MCVeloFEObj* obj);
  /// copy constructor
  MCVeloFE(const MCVeloFE& other);
  /// copy-assignment operator
  MCVeloFE& operator=(const MCVeloFE& other);
  /// support cloning (deep-copy)
  MCVeloFE clone() const;
  /// destructor
  ~MCVeloFE();

  /// conversion to const object
  operator ConstMCVeloFE () const;

public:

  const double& m_addedSignal() const;
  const double& m_addedPedestal() const;
  const double& m_addedNoise() const;
  const double& m_addedCMNoise() const;

  void m_addedSignal(double value);
  void m_addedPedestal(double value);
  void m_addedNoise(double value);
  void m_addedCMNoise(double value);

  void addm_MCHits(ConstMCHit);
  unsigned int m_MCHits_size() const;
  ConstMCHit m_MCHits(unsigned int) const;
  std::vector<ConstMCHit>::const_iterator m_MCHits_begin() const;
  std::vector<ConstMCHit>::const_iterator m_MCHits_end() const;
  void addMCHitsCharge(double);
  unsigned int MCHitsCharge_size() const;
  double MCHitsCharge(unsigned int) const;
  std::vector<double>::const_iterator MCHitsCharge_begin() const;
  std::vector<double>::const_iterator MCHitsCharge_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCVeloFEObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCVeloFE& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCVeloFE& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCVeloFE& p1,
//       const MCVeloFE& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCVeloFEObj* m_obj;

};

#endif
