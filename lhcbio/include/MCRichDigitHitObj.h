#ifndef MCRichDigitHitOBJ_H
#define MCRichDigitHitOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCRichDigitHitData.h"



// forward declarations
class MCRichDigitHit;
class ConstMCRichDigitHit;
class ConstMCRichHit;


class MCRichDigitHitObj : public podio::ObjBase {
public:
  /// constructor
  MCRichDigitHitObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCRichDigitHitObj(const MCRichDigitHitObj&);
  /// constructor from ObjectID and MCRichDigitHitData
  /// does not initialize the internal relation containers
  MCRichDigitHitObj(const podio::ObjectID id, MCRichDigitHitData data);
  virtual ~MCRichDigitHitObj();

public:
  MCRichDigitHitData data;
  ConstMCRichHit* m_m_mcRichHit;


};


#endif
