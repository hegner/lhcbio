// standard includes
#include <stdexcept>


#include "GhostTrackInfoCollection.h"

GhostTrackInfoCollection::GhostTrackInfoCollection() : m_collectionID(0), m_entries() ,m_refCollections(nullptr), m_data(new GhostTrackInfoDataContainer() ) {
  
}

const GhostTrackInfo GhostTrackInfoCollection::operator[](unsigned int index) const {
  return GhostTrackInfo(m_entries[index]);
}

const GhostTrackInfo GhostTrackInfoCollection::at(unsigned int index) const {
  return GhostTrackInfo(m_entries.at(index));
}

int  GhostTrackInfoCollection::size() const {
  return m_entries.size();
}

GhostTrackInfo GhostTrackInfoCollection::create(){
  auto obj = new GhostTrackInfoObj();
  m_entries.emplace_back(obj);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return GhostTrackInfo(obj);
}

void GhostTrackInfoCollection::clear(){
  m_data->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void GhostTrackInfoCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
  
  }
  
}

void GhostTrackInfoCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new GhostTrackInfoObj({index,m_collectionID}, data);
    
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool GhostTrackInfoCollection::setReferences(const podio::ICollectionProvider* collectionProvider){


  return true; //TODO: check success
}

void GhostTrackInfoCollection::push_back(ConstGhostTrackInfo object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
        
    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void GhostTrackInfoCollection::setBuffer(void* address){
  m_data = static_cast<GhostTrackInfoDataContainer*>(address);
}


const GhostTrackInfo GhostTrackInfoCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const GhostTrackInfo* GhostTrackInfoCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const GhostTrackInfoCollectionIterator& GhostTrackInfoCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
