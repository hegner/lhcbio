//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCHitCollection_H
#define  MCHitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCHitData.h"
#include "MCHit.h"
#include "MCHitObj.h"

typedef std::vector<MCHitData> MCHitDataContainer;
typedef std::deque<MCHitObj*> MCHitObjPointerContainer;

class MCHitCollectionIterator {

  public:
    MCHitCollectionIterator(int index, const MCHitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCHitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCHit operator*() const;
    const MCHit* operator->() const;
    const MCHitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCHit m_object;
    const MCHitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCHitCollection : public podio::CollectionBase {

public:
  typedef const MCHitCollectionIterator const_iterator;

  MCHitCollection();
//  MCHitCollection(const MCHitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCHitCollection(MCHitVector* data, int collectionID);
  ~MCHitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCHit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCHit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCHit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCHit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCHit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCHitData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<int,arraysize> m_sensDetID() const;
  template<size_t arraysize>  
  const std::array<XYZPoint,arraysize> m_entry() const;
  template<size_t arraysize>  
  const std::array<XYZVector,arraysize> m_displacement() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_energy() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_time() const;
  template<size_t arraysize>  
  const std::array<double,arraysize> m_p() const;


private:
  int m_collectionID;
  MCHitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCParticle>* m_rel_m_MCParticle; //relation buffer for r/w

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCHitDataContainer* m_data;
};

template<typename... Args>
MCHit  MCHitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCHitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCHit(obj);
}

template<size_t arraysize>
const std::array<int,arraysize> MCHitCollection::m_sensDetID() const {
  std::array<int,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_sensDetID;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<XYZPoint,arraysize> MCHitCollection::m_entry() const {
  std::array<XYZPoint,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_entry;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<XYZVector,arraysize> MCHitCollection::m_displacement() const {
  std::array<XYZVector,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_displacement;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCHitCollection::m_energy() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_energy;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCHitCollection::m_time() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_time;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<double,arraysize> MCHitCollection::m_p() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_p;
 }
 return tmp;
}


#endif
