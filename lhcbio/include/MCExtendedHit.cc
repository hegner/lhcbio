// datamodel specific includes
#include "MCExtendedHit.h"
#include "MCExtendedHitConst.h"
#include "MCExtendedHitObj.h"
#include "MCExtendedHitData.h"
#include "MCExtendedHitCollection.h"
#include <iostream>

MCExtendedHit::MCExtendedHit() : m_obj(new MCExtendedHitObj()){
 m_obj->acquire();
};

MCExtendedHit::MCExtendedHit(XYZVector m_momentum) : m_obj(new MCExtendedHitObj()){
 m_obj->acquire();
   m_obj->data.m_momentum = m_momentum;
};

MCExtendedHit::MCExtendedHit(const MCExtendedHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCExtendedHit& MCExtendedHit::operator=(const MCExtendedHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCExtendedHit::MCExtendedHit(MCExtendedHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCExtendedHit MCExtendedHit::clone() const {
  return {new MCExtendedHitObj(*m_obj)};
}

MCExtendedHit::~MCExtendedHit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCExtendedHit::operator ConstMCExtendedHit() const {return ConstMCExtendedHit(m_obj);};

  const XYZVector& MCExtendedHit::m_momentum() const { return m_obj->data.m_momentum; };

  XYZVector& MCExtendedHit::m_momentum() { return m_obj->data.m_momentum; };
void MCExtendedHit::m_momentum(class XYZVector value){ m_obj->data.m_momentum = value;}


bool  MCExtendedHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCExtendedHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCExtendedHit::operator==(const ConstMCExtendedHit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCExtendedHit& p1, const MCExtendedHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
