#ifndef ConstMCHit_H
#define ConstMCHit_H
#include "MCHitData.h"
#include "XYZPoint.h"
#include "XYZVector.h"

#include <vector>
#include "podio/ObjectID.h"

// /< |P| of particle at the entry point (MeV)
// author: Marco Cattaneo, revised by G.Corti

//forward declarations
class MCHitObj;
class MCHit;
class MCHitCollection;
class MCHitCollectionIterator;
class MCParticle;
class ConstMCParticle;


#include "MCHitObj.h"

class ConstMCHit {

  friend MCHit;
  friend MCHitCollection;
  friend MCHitCollectionIterator;

public:

  /// default constructor
  ConstMCHit();
  ConstMCHit(int m_sensDetID,XYZPoint m_entry,XYZVector m_displacement,double m_energy,double m_time,double m_p);

  /// constructor from existing MCHitObj
  ConstMCHit(MCHitObj* obj);
  /// copy constructor
  ConstMCHit(const ConstMCHit& other);
  /// copy-assignment operator
  ConstMCHit& operator=(const ConstMCHit& other);
  /// support cloning (deep-copy)
  ConstMCHit clone() const;
  /// destructor
  ~ConstMCHit();


public:

  const int& m_sensDetID() const;
  const XYZPoint& m_entry() const;
  const XYZVector& m_displacement() const;
  const double& m_energy() const;
  const double& m_time() const;
  const double& m_p() const;
  const ConstMCParticle m_MCParticle() const;


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCHitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCHit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCHit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCHit& p1,
//       const MCHit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCHitObj* m_obj;

};

#endif
