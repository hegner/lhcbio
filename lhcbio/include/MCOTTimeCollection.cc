// standard includes
#include <stdexcept>
#include "MCOTDepositCollection.h" 


#include "MCOTTimeCollection.h"

MCOTTimeCollection::MCOTTimeCollection() : m_collectionID(0), m_entries() ,m_rel_m_deposits(new std::vector<ConstMCOTDeposit>()),m_refCollections(nullptr), m_data(new MCOTTimeDataContainer() ) {
    m_refCollections = new podio::CollRefCollection();
  m_refCollections->push_back(new std::vector<podio::ObjectID>());

}

const MCOTTime MCOTTimeCollection::operator[](unsigned int index) const {
  return MCOTTime(m_entries[index]);
}

const MCOTTime MCOTTimeCollection::at(unsigned int index) const {
  return MCOTTime(m_entries.at(index));
}

int  MCOTTimeCollection::size() const {
  return m_entries.size();
}

MCOTTime MCOTTimeCollection::create(){
  auto obj = new MCOTTimeObj();
  m_entries.emplace_back(obj);
  m_rel_m_deposits_tmp.push_back(obj->m_m_deposits);

  obj->id = {int(m_entries.size()-1),m_collectionID};
  return MCOTTime(obj);
}

void MCOTTimeCollection::clear(){
  m_data->clear();
  for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  // clear relations to m_deposits. Make sure to unlink() the reference data as they may be gone already
  for (auto& pointer : m_rel_m_deposits_tmp) {for(auto& item : (*pointer)) {item.unlink();}; delete pointer;}
  m_rel_m_deposits_tmp.clear();
  for (auto& item : (*m_rel_m_deposits)) {item.unlink(); }
  m_rel_m_deposits->clear();

  for (auto& obj : m_entries) { delete obj; }
  m_entries.clear();
}

void MCOTTimeCollection::prepareForWrite(){
  int index = 0;
  auto size = m_entries.size();
  m_data->reserve(size);
  for (auto& obj : m_entries) {m_data->push_back(obj->data); }
  if (m_refCollections != nullptr) {
    for (auto& pointer : (*m_refCollections)) {pointer->clear(); }
  }
  
  for(int i=0, size = m_data->size(); i != size; ++i){
     (*m_data)[i].m_deposits_begin=index;
   (*m_data)[i].m_deposits_end+=index;
   index = (*m_data)[index].m_deposits_end;
   for(auto it : (*m_rel_m_deposits_tmp[i])) {
     if (it.getObjectID().index == podio::ObjectID::untracked)
       throw std::runtime_error("Trying to persistify untracked object");
     (*m_refCollections)[0]->emplace_back(it.getObjectID());
     m_rel_m_deposits->push_back(it);
   }

  }
  
}

void MCOTTimeCollection::prepareAfterRead(){
  int index = 0;
  for (auto& data : *m_data){
    auto obj = new MCOTTimeObj({index,m_collectionID}, data);
        obj->m_m_deposits = m_rel_m_deposits;
    m_entries.emplace_back(obj);
    ++index;
  }
}

bool MCOTTimeCollection::setReferences(const podio::ICollectionProvider* collectionProvider){
  for(unsigned int i=0, size=(*m_refCollections)[0]->size();i!=size;++i ) {
    auto id = (*(*m_refCollections)[0])[i];
    CollectionBase* coll = nullptr;
    collectionProvider->get(id.collectionID,coll);
    MCOTDepositCollection* tmp_coll = static_cast<MCOTDepositCollection*>(coll);
    auto tmp = (*tmp_coll)[id.index];
    m_rel_m_deposits->emplace_back(tmp);
  }


  return true; //TODO: check success
}

void MCOTTimeCollection::push_back(ConstMCOTTime object){
    int size = m_entries.size();
    auto obj = object.m_obj;
    if (obj->id.index == podio::ObjectID::untracked) {
        obj->id = {size,m_collectionID};
        m_entries.push_back(obj);
          m_rel_m_deposits_tmp.push_back(obj->m_m_deposits);

    } else {
      throw std::invalid_argument( "Object already in a collection. Cannot add it to a second collection " );

    }
}

void MCOTTimeCollection::setBuffer(void* address){
  m_data = static_cast<MCOTTimeDataContainer*>(address);
}


const MCOTTime MCOTTimeCollectionIterator::operator* () const {
  m_object.m_obj = (*m_collection)[m_index];
  return m_object;
}

const MCOTTime* MCOTTimeCollectionIterator::operator-> () const {
    m_object.m_obj = (*m_collection)[m_index];
    return &m_object;
}

const MCOTTimeCollectionIterator& MCOTTimeCollectionIterator::operator++() const {
  ++m_index;
 return *this;
}
