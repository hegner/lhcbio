//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef GhostTrackInfoCollection_H
#define  GhostTrackInfoCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "GhostTrackInfoData.h"
#include "GhostTrackInfo.h"
#include "GhostTrackInfoObj.h"

typedef std::vector<GhostTrackInfoData> GhostTrackInfoDataContainer;
typedef std::deque<GhostTrackInfoObj*> GhostTrackInfoObjPointerContainer;

class GhostTrackInfoCollectionIterator {

  public:
    GhostTrackInfoCollectionIterator(int index, const GhostTrackInfoObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const GhostTrackInfoCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const GhostTrackInfo operator*() const;
    const GhostTrackInfo* operator->() const;
    const GhostTrackInfoCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable GhostTrackInfo m_object;
    const GhostTrackInfoObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class GhostTrackInfoCollection : public podio::CollectionBase {

public:
  typedef const GhostTrackInfoCollectionIterator const_iterator;

  GhostTrackInfoCollection();
//  GhostTrackInfoCollection(const GhostTrackInfoCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  GhostTrackInfoCollection(GhostTrackInfoVector* data, int collectionID);
  ~GhostTrackInfoCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  GhostTrackInfo create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  GhostTrackInfo create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const GhostTrackInfo operator[](unsigned int index) const;
  /// Returns the object of given index
  const GhostTrackInfo at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstGhostTrackInfo object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<GhostTrackInfoData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<LinkMap,arraysize> m_linkMap() const;
  template<size_t arraysize>  
  const std::array<Classification,arraysize> m_classification() const;


private:
  int m_collectionID;
  GhostTrackInfoObjPointerContainer m_entries;
  // members to handle 1-to-N-relations

  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  GhostTrackInfoDataContainer* m_data;
};

template<typename... Args>
GhostTrackInfo  GhostTrackInfoCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new GhostTrackInfoObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return GhostTrackInfo(obj);
}

template<size_t arraysize>
const std::array<LinkMap,arraysize> GhostTrackInfoCollection::m_linkMap() const {
  std::array<LinkMap,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_linkMap;
 }
 return tmp;
}
template<size_t arraysize>
const std::array<Classification,arraysize> GhostTrackInfoCollection::m_classification() const {
  std::array<Classification,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_classification;
 }
 return tmp;
}


#endif
