#ifndef MCVeloFEOBJ_H
#define MCVeloFEOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCVeloFEData.h"

#include <vector>
#include "MCHit.h"


// forward declarations
class MCVeloFE;
class ConstMCVeloFE;


class MCVeloFEObj : public podio::ObjBase {
public:
  /// constructor
  MCVeloFEObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCVeloFEObj(const MCVeloFEObj&);
  /// constructor from ObjectID and MCVeloFEData
  /// does not initialize the internal relation containers
  MCVeloFEObj(const podio::ObjectID id, MCVeloFEData data);
  virtual ~MCVeloFEObj();

public:
  MCVeloFEData data;
  std::vector<ConstMCHit>* m_m_MCHits;
  std::vector<double>* m_MCHitsCharge;


};


#endif
