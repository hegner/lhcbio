#ifndef MCMuonHitHistoryOBJ_H
#define MCMuonHitHistoryOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCMuonHitHistoryData.h"



// forward declarations
class MCMuonHitHistory;
class ConstMCMuonHitHistory;


class MCMuonHitHistoryObj : public podio::ObjBase {
public:
  /// constructor
  MCMuonHitHistoryObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCMuonHitHistoryObj(const MCMuonHitHistoryObj&);
  /// constructor from ObjectID and MCMuonHitHistoryData
  /// does not initialize the internal relation containers
  MCMuonHitHistoryObj(const podio::ObjectID id, MCMuonHitHistoryData data);
  virtual ~MCMuonHitHistoryObj();

public:
  MCMuonHitHistoryData data;


};


#endif
