// datamodel specific includes
#include "MCSTDigit.h"
#include "MCSTDigitConst.h"
#include "MCSTDigitObj.h"
#include "MCSTDigitData.h"
#include "MCSTDigitCollection.h"
#include <iostream>

MCSTDigit::MCSTDigit() : m_obj(new MCSTDigitObj()){
 m_obj->acquire();
};



MCSTDigit::MCSTDigit(const MCSTDigit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCSTDigit& MCSTDigit::operator=(const MCSTDigit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCSTDigit::MCSTDigit(MCSTDigitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCSTDigit MCSTDigit::clone() const {
  return {new MCSTDigitObj(*m_obj)};
}

MCSTDigit::~MCSTDigit(){
  if ( m_obj != nullptr) m_obj->release();
}

MCSTDigit::operator ConstMCSTDigit() const {return ConstMCSTDigit(m_obj);};



std::vector<ConstMCSTDeposit>::const_iterator MCSTDigit::m_mcDeposit_begin() const {
  auto ret_value = m_obj->m_m_mcDeposit->begin();
  std::advance(ret_value, m_obj->data.m_mcDeposit_begin);
  return ret_value;
}

std::vector<ConstMCSTDeposit>::const_iterator MCSTDigit::m_mcDeposit_end() const {
  auto ret_value = m_obj->m_m_mcDeposit->begin();
  std::advance(ret_value, m_obj->data.m_mcDeposit_end-1);
  return ++ret_value;
}

void MCSTDigit::addm_mcDeposit(ConstMCSTDeposit component) {
  m_obj->m_m_mcDeposit->push_back(component);
  m_obj->data.m_mcDeposit_end++;
}

unsigned int MCSTDigit::m_mcDeposit_size() const {
  return (m_obj->data.m_mcDeposit_end-m_obj->data.m_mcDeposit_begin);
}

ConstMCSTDeposit MCSTDigit::m_mcDeposit(unsigned int index) const {
  if (m_mcDeposit_size() > index) {
    return m_obj->m_m_mcDeposit->at(m_obj->data.m_mcDeposit_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCSTDigit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCSTDigit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCSTDigit::operator==(const ConstMCSTDigit& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCSTDigit& p1, const MCSTDigit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
