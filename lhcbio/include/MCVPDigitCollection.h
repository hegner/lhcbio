//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCVPDigitCollection_H
#define  MCVPDigitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCVPDigitData.h"
#include "MCVPDigit.h"
#include "MCVPDigitObj.h"

typedef std::vector<MCVPDigitData> MCVPDigitDataContainer;
typedef std::deque<MCVPDigitObj*> MCVPDigitObjPointerContainer;

class MCVPDigitCollectionIterator {

  public:
    MCVPDigitCollectionIterator(int index, const MCVPDigitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCVPDigitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCVPDigit operator*() const;
    const MCVPDigit* operator->() const;
    const MCVPDigitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCVPDigit m_object;
    const MCVPDigitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCVPDigitCollection : public podio::CollectionBase {

public:
  typedef const MCVPDigitCollectionIterator const_iterator;

  MCVPDigitCollection();
//  MCVPDigitCollection(const MCVPDigitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCVPDigitCollection(MCVPDigitVector* data, int collectionID);
  ~MCVPDigitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCVPDigit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCVPDigit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCVPDigit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCVPDigit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCVPDigit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCVPDigitData>* _getBuffer() { return m_data;};

   

private:
  int m_collectionID;
  MCVPDigitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCHit>* m_rel_m_mcHits; //relation buffer for r/w
  std::vector<std::vector<ConstMCHit>*> m_rel_m_mcHits_tmp;
 
  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCVPDigitDataContainer* m_data;
};

template<typename... Args>
MCVPDigit  MCVPDigitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCVPDigitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCVPDigit(obj);
}



#endif
