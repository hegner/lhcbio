// datamodel specific includes
#include "MCParticle.h"
#include "MCParticleConst.h"
#include "MCParticleObj.h"
#include "MCParticleData.h"
#include "MCParticleCollection.h"
#include <iostream>
#include "MCVertex.h"

MCParticle::MCParticle() : m_obj(new MCParticleObj()){
 m_obj->acquire();
};

MCParticle::MCParticle(LorentzVector m_momentum,ParticleID m_particleID,unsigned m_flags) : m_obj(new MCParticleObj()){
 m_obj->acquire();
   m_obj->data.m_momentum = m_momentum;  m_obj->data.m_particleID = m_particleID;  m_obj->data.m_flags = m_flags;
};

MCParticle::MCParticle(const MCParticle& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

MCParticle& MCParticle::operator=(const MCParticle& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

MCParticle::MCParticle(MCParticleObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

MCParticle MCParticle::clone() const {
  return {new MCParticleObj(*m_obj)};
}

MCParticle::~MCParticle(){
  if ( m_obj != nullptr) m_obj->release();
}

MCParticle::operator ConstMCParticle() const {return ConstMCParticle(m_obj);};

  const LorentzVector& MCParticle::m_momentum() const { return m_obj->data.m_momentum; };
  const ParticleID& MCParticle::m_particleID() const { return m_obj->data.m_particleID; };
  const unsigned& MCParticle::m_flags() const { return m_obj->data.m_flags; };
  const ConstMCVertex MCParticle::m_originVertex() const { if (m_obj->m_m_originVertex == nullptr) {
 return ConstMCVertex(nullptr);}
 return ConstMCVertex(*(m_obj->m_m_originVertex));};

  LorentzVector& MCParticle::m_momentum() { return m_obj->data.m_momentum; };
void MCParticle::m_momentum(class LorentzVector value){ m_obj->data.m_momentum = value;}
  ParticleID& MCParticle::m_particleID() { return m_obj->data.m_particleID; };
void MCParticle::m_particleID(class ParticleID value){ m_obj->data.m_particleID = value;}
void MCParticle::m_flags(unsigned value){ m_obj->data.m_flags = value;}
void MCParticle::m_originVertex(ConstMCVertex value) { if (m_obj->m_m_originVertex != nullptr) delete m_obj->m_m_originVertex; m_obj->m_m_originVertex = new ConstMCVertex(value); };

std::vector<ConstMCVertex>::const_iterator MCParticle::m_endVertices_begin() const {
  auto ret_value = m_obj->m_m_endVertices->begin();
  std::advance(ret_value, m_obj->data.m_endVertices_begin);
  return ret_value;
}

std::vector<ConstMCVertex>::const_iterator MCParticle::m_endVertices_end() const {
  auto ret_value = m_obj->m_m_endVertices->begin();
  std::advance(ret_value, m_obj->data.m_endVertices_end-1);
  return ++ret_value;
}

void MCParticle::addm_endVertices(ConstMCVertex component) {
  m_obj->m_m_endVertices->push_back(component);
  m_obj->data.m_endVertices_end++;
}

unsigned int MCParticle::m_endVertices_size() const {
  return (m_obj->data.m_endVertices_end-m_obj->data.m_endVertices_begin);
}

ConstMCVertex MCParticle::m_endVertices(unsigned int index) const {
  if (m_endVertices_size() > index) {
    return m_obj->m_m_endVertices->at(m_obj->data.m_endVertices_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  MCParticle::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID MCParticle::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool MCParticle::operator==(const ConstMCParticle& other) const {
     return (m_obj==other.m_obj);
}


//bool operator< (const MCParticle& p1, const MCParticle& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
