#ifndef MCSTDeposit_H
#define MCSTDeposit_H
#include "MCSTDepositData.h"
#include "STChannelID.h"

#include <vector>
#include "podio/ObjectID.h"

// /< channelID
// author: Matthew Needham

//forward declarations
class MCSTDepositCollection;
class MCSTDepositCollectionIterator;
class ConstMCSTDeposit;
class MCHit;
class ConstMCHit;


#include "MCSTDepositConst.h"
#include "MCSTDepositObj.h"

class MCSTDeposit {

  friend MCSTDepositCollection;
  friend MCSTDepositCollectionIterator;
  friend ConstMCSTDeposit;

public:

  /// default constructor
  MCSTDeposit();
    MCSTDeposit(double m_depositedCharge,STChannelID m_channelID);

  /// constructor from existing MCSTDepositObj
  MCSTDeposit(MCSTDepositObj* obj);
  /// copy constructor
  MCSTDeposit(const MCSTDeposit& other);
  /// copy-assignment operator
  MCSTDeposit& operator=(const MCSTDeposit& other);
  /// support cloning (deep-copy)
  MCSTDeposit clone() const;
  /// destructor
  ~MCSTDeposit();

  /// conversion to const object
  operator ConstMCSTDeposit () const;

public:

  const double& m_depositedCharge() const;
  const STChannelID& m_channelID() const;
  const ConstMCHit m_mcHit() const;

  void m_depositedCharge(double value);
  STChannelID& m_channelID();
  void m_channelID(class STChannelID value);
  void m_mcHit(ConstMCHit value);


  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCSTDepositObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCSTDeposit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCSTDeposit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCSTDeposit& p1,
//       const MCSTDeposit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCSTDepositObj* m_obj;

};

#endif
