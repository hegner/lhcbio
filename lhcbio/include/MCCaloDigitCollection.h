//AUTOMATICALLY GENERATED - DO NOT EDIT

#ifndef MCCaloDigitCollection_H
#define  MCCaloDigitCollection_H

#include <string>
#include <vector>
#include <deque>
#include <array>

// podio specific includes
#include "podio/ICollectionProvider.h"
#include "podio/CollectionBase.h"
#include "podio/CollectionIDTable.h"

// datamodel specific includes
#include "MCCaloDigitData.h"
#include "MCCaloDigit.h"
#include "MCCaloDigitObj.h"

typedef std::vector<MCCaloDigitData> MCCaloDigitDataContainer;
typedef std::deque<MCCaloDigitObj*> MCCaloDigitObjPointerContainer;

class MCCaloDigitCollectionIterator {

  public:
    MCCaloDigitCollectionIterator(int index, const MCCaloDigitObjPointerContainer* collection) : m_index(index), m_object(nullptr), m_collection(collection) {}

    bool operator!=(const MCCaloDigitCollectionIterator& x) const {
      return m_index != x.m_index; //TODO: may not be complete
    }

    const MCCaloDigit operator*() const;
    const MCCaloDigit* operator->() const;
    const MCCaloDigitCollectionIterator& operator++() const;

  private:
    mutable int m_index;
    mutable MCCaloDigit m_object;
    const MCCaloDigitObjPointerContainer* m_collection;
};

/**
A Collection is identified by an ID.
*/

class MCCaloDigitCollection : public podio::CollectionBase {

public:
  typedef const MCCaloDigitCollectionIterator const_iterator;

  MCCaloDigitCollection();
//  MCCaloDigitCollection(const MCCaloDigitCollection& ) = delete; // deletion doesn't work w/ ROOT IO ! :-(
//  MCCaloDigitCollection(MCCaloDigitVector* data, int collectionID);
  ~MCCaloDigitCollection(){};

  void clear();
  /// Append a new object to the collection, and return this object.
  MCCaloDigit create();

  /// Append a new object to the collection, and return this object.
  /// Initialized with the parameters given
  template<typename... Args>
  MCCaloDigit create(Args&&... args);
  int size() const;

  /// Returns the object of given index
  const MCCaloDigit operator[](unsigned int index) const;
  /// Returns the object of given index
  const MCCaloDigit at(unsigned int index) const;


  /// Append object to the collection
  void push_back(ConstMCCaloDigit object);

  void prepareForWrite();
  void prepareAfterRead();
  void setBuffer(void* address);
  bool setReferences(const podio::ICollectionProvider* collectionProvider);

  podio::CollRefCollection* referenceCollections() { return m_refCollections;};

  void setID(unsigned ID){m_collectionID = ID;};

  // support for the iterator protocol
  const const_iterator begin() const {
    return const_iterator(0, &m_entries);
  }
  const	const_iterator end() const {
    return const_iterator(m_entries.size(), &m_entries);
  }

  /// returns the address of the pointer to the data buffer
  void* getBufferAddress() { return (void*)&m_data;};

  /// returns the pointer to the data buffer
  std::vector<MCCaloDigitData>* _getBuffer() { return m_data;};

     template<size_t arraysize>  
  const std::array<double,arraysize> m_activeE() const;


private:
  int m_collectionID;
  MCCaloDigitObjPointerContainer m_entries;
  // members to handle 1-to-N-relations
  std::vector<ConstMCCaloHit>* m_rel_m_hits; //relation buffer for r/w
  std::vector<std::vector<ConstMCCaloHit>*> m_rel_m_hits_tmp;
 
  // members to handle streaming
  podio::CollRefCollection* m_refCollections;
  MCCaloDigitDataContainer* m_data;
};

template<typename... Args>
MCCaloDigit  MCCaloDigitCollection::create(Args&&... args){
  int size = m_entries.size();
  auto obj = new MCCaloDigitObj({size,m_collectionID},{args...});
  m_entries.push_back(obj);
  return MCCaloDigit(obj);
}

template<size_t arraysize>
const std::array<double,arraysize> MCCaloDigitCollection::m_activeE() const {
  std::array<double,arraysize> tmp;
  auto valid_size = std::min(arraysize,m_entries.size());
  for (unsigned i = 0; i<valid_size; ++i){
    tmp[i] = m_entries[i]->data.m_activeE;
 }
 return tmp;
}


#endif
