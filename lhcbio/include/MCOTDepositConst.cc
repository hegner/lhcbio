// datamodel specific includes
#include "MCOTDeposit.h"
#include "MCOTDepositConst.h"
#include "MCOTDepositObj.h"
#include "MCOTDepositData.h"
#include "MCOTDepositCollection.h"
#include <iostream>
#include "MCHit.h"

ConstMCOTDeposit::ConstMCOTDeposit() : m_obj(new MCOTDepositObj()){
 m_obj->acquire();
};

ConstMCOTDeposit::ConstMCOTDeposit(int m_type,OTChannelID m_channel,double m_time,double m_driftDistance,int m_ambiguity) : m_obj(new MCOTDepositObj()){
 m_obj->acquire();
   m_obj->data.m_type = m_type;  m_obj->data.m_channel = m_channel;  m_obj->data.m_time = m_time;  m_obj->data.m_driftDistance = m_driftDistance;  m_obj->data.m_ambiguity = m_ambiguity;
};


ConstMCOTDeposit::ConstMCOTDeposit(const ConstMCOTDeposit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCOTDeposit& ConstMCOTDeposit::operator=(const ConstMCOTDeposit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCOTDeposit::ConstMCOTDeposit(MCOTDepositObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCOTDeposit ConstMCOTDeposit::clone() const {
  return {new MCOTDepositObj(*m_obj)};
}

ConstMCOTDeposit::~ConstMCOTDeposit(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCHit ConstMCOTDeposit::m_mcHit() const { if (m_obj->m_m_mcHit == nullptr) {
 return ConstMCHit(nullptr);}
 return ConstMCHit(*(m_obj->m_m_mcHit));};


bool  ConstMCOTDeposit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCOTDeposit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCOTDeposit::operator==(const MCOTDeposit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCOTDeposit& p1, const MCOTDeposit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
