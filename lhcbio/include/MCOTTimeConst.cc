// datamodel specific includes
#include "MCOTTime.h"
#include "MCOTTimeConst.h"
#include "MCOTTimeObj.h"
#include "MCOTTimeData.h"
#include "MCOTTimeCollection.h"
#include <iostream>

ConstMCOTTime::ConstMCOTTime() : m_obj(new MCOTTimeObj()){
 m_obj->acquire();
};



ConstMCOTTime::ConstMCOTTime(const ConstMCOTTime& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCOTTime& ConstMCOTTime::operator=(const ConstMCOTTime& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCOTTime::ConstMCOTTime(MCOTTimeObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCOTTime ConstMCOTTime::clone() const {
  return {new MCOTTimeObj(*m_obj)};
}

ConstMCOTTime::~ConstMCOTTime(){
  if ( m_obj != nullptr) m_obj->release();
}


std::vector<ConstMCOTDeposit>::const_iterator ConstMCOTTime::m_deposits_begin() const {
  auto ret_value = m_obj->m_m_deposits->begin();
  std::advance(ret_value, m_obj->data.m_deposits_begin);
  return ret_value;
}

std::vector<ConstMCOTDeposit>::const_iterator ConstMCOTTime::m_deposits_end() const {
  auto ret_value = m_obj->m_m_deposits->begin();
  std::advance(ret_value, m_obj->data.m_deposits_end-1);
  return ++ret_value;
}

unsigned int ConstMCOTTime::m_deposits_size() const {
  return (m_obj->data.m_deposits_end-m_obj->data.m_deposits_begin);
}

ConstMCOTDeposit ConstMCOTTime::m_deposits(unsigned int index) const {
  if (m_deposits_size() > index) {
    return m_obj->m_m_deposits->at(m_obj->data.m_deposits_begin+index);
}
  else throw std::out_of_range ("index out of bounds for existing references");
}

bool  ConstMCOTTime::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCOTTime::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCOTTime::operator==(const MCOTTime& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCOTTime& p1, const MCOTTime& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
