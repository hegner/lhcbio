// datamodel specific includes
#include "MCRichHit.h"
#include "MCRichHitConst.h"
#include "MCRichHitObj.h"
#include "MCRichHitData.h"
#include "MCRichHitCollection.h"
#include <iostream>
#include "MCParticle.h"

ConstMCRichHit::ConstMCRichHit() : m_obj(new MCRichHitObj()){
 m_obj->acquire();
};

ConstMCRichHit::ConstMCRichHit(XYZPoint m_entry,double m_energy,double m_timeOfFlight,RichSmartID m_sensDetID,unsigned m_historyCode) : m_obj(new MCRichHitObj()){
 m_obj->acquire();
   m_obj->data.m_entry = m_entry;  m_obj->data.m_energy = m_energy;  m_obj->data.m_timeOfFlight = m_timeOfFlight;  m_obj->data.m_sensDetID = m_sensDetID;  m_obj->data.m_historyCode = m_historyCode;
};


ConstMCRichHit::ConstMCRichHit(const ConstMCRichHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCRichHit& ConstMCRichHit::operator=(const ConstMCRichHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCRichHit::ConstMCRichHit(MCRichHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCRichHit ConstMCRichHit::clone() const {
  return {new MCRichHitObj(*m_obj)};
}

ConstMCRichHit::~ConstMCRichHit(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCParticle ConstMCRichHit::m_MCParticle() const { if (m_obj->m_m_MCParticle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_MCParticle));};


bool  ConstMCRichHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCRichHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCRichHit::operator==(const MCRichHit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCRichHit& p1, const MCRichHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
