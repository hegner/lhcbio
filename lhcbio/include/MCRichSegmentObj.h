#ifndef MCRichSegmentOBJ_H
#define MCRichSegmentOBJ_H

// std includes
#include <atomic>
#include <iostream>

// data model specific includes
#include "podio/ObjBase.h"
#include "MCRichSegmentData.h"

#include <vector>
#include "MCRichOpticalPhoton.h"
#include "MCRichHit.h"
#include "XYZPoint.h"
#include "XYZVector.h"


// forward declarations
class MCRichSegment;
class ConstMCRichSegment;
class ConstMCParticle;
class ConstMCRichTrack;


class MCRichSegmentObj : public podio::ObjBase {
public:
  /// constructor
  MCRichSegmentObj();
  /// copy constructor (does a deep-copy of relation containers)
  MCRichSegmentObj(const MCRichSegmentObj&);
  /// constructor from ObjectID and MCRichSegmentData
  /// does not initialize the internal relation containers
  MCRichSegmentObj(const podio::ObjectID id, MCRichSegmentData data);
  virtual ~MCRichSegmentObj();

public:
  MCRichSegmentData data;
  ConstMCParticle* m_m_mcParticle;
  ConstMCRichTrack* m_m_MCRichTrack;
  std::vector<ConstMCRichOpticalPhoton>* m_m_MCRichOpticalPhotons;
  std::vector<ConstMCRichHit>* m_m_MCRichHits;
  std::vector<XYZPoint>* m_trajectoryPoints;
  std::vector<XYZVector>* m_trajectoryMomenta;


};


#endif
