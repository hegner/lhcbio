#ifndef ConstMCMuonDigit_H
#define ConstMCMuonDigit_H
#include "MCMuonDigitData.h"
#include "MCMuonDigitInfo.h"
#include <vector>
#include "MCHit.h"
#include <vector>
#include "MCMuonHitHistory.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Firing Time of the hit
// author: Alessia Satta

//forward declarations
class MCMuonDigitObj;
class MCMuonDigit;
class MCMuonDigitCollection;
class MCMuonDigitCollectionIterator;


#include "MCMuonDigitObj.h"

class ConstMCMuonDigit {

  friend MCMuonDigit;
  friend MCMuonDigitCollection;
  friend MCMuonDigitCollectionIterator;

public:

  /// default constructor
  ConstMCMuonDigit();
  ConstMCMuonDigit(MCMuonDigitInfo m_DigitInfo,double m_firingTime);

  /// constructor from existing MCMuonDigitObj
  ConstMCMuonDigit(MCMuonDigitObj* obj);
  /// copy constructor
  ConstMCMuonDigit(const ConstMCMuonDigit& other);
  /// copy-assignment operator
  ConstMCMuonDigit& operator=(const ConstMCMuonDigit& other);
  /// support cloning (deep-copy)
  ConstMCMuonDigit clone() const;
  /// destructor
  ~ConstMCMuonDigit();


public:

  const MCMuonDigitInfo& m_DigitInfo() const;
  const double& m_firingTime() const;

  unsigned int m_MCHits_size() const;
  ConstMCHit m_MCHits(unsigned int) const;
  std::vector<ConstMCHit>::const_iterator m_MCHits_begin() const;
  std::vector<ConstMCHit>::const_iterator m_MCHits_end() const;
  unsigned int HitsHistory_size() const;
  MCMuonHitHistory HitsHistory(unsigned int) const;
  std::vector<MCMuonHitHistory>::const_iterator HitsHistory_begin() const;
  std::vector<MCMuonHitHistory>::const_iterator HitsHistory_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCMuonDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const ConstMCMuonDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const MCMuonDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCMuonDigit& p1,
//       const MCMuonDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCMuonDigitObj* m_obj;

};

#endif
