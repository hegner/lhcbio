// datamodel specific includes
#include "MCExtendedHit.h"
#include "MCExtendedHitConst.h"
#include "MCExtendedHitObj.h"
#include "MCExtendedHitData.h"
#include "MCExtendedHitCollection.h"
#include <iostream>

ConstMCExtendedHit::ConstMCExtendedHit() : m_obj(new MCExtendedHitObj()){
 m_obj->acquire();
};

ConstMCExtendedHit::ConstMCExtendedHit(XYZVector m_momentum) : m_obj(new MCExtendedHitObj()){
 m_obj->acquire();
   m_obj->data.m_momentum = m_momentum;
};


ConstMCExtendedHit::ConstMCExtendedHit(const ConstMCExtendedHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCExtendedHit& ConstMCExtendedHit::operator=(const ConstMCExtendedHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCExtendedHit::ConstMCExtendedHit(MCExtendedHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCExtendedHit ConstMCExtendedHit::clone() const {
  return {new MCExtendedHitObj(*m_obj)};
}

ConstMCExtendedHit::~ConstMCExtendedHit(){
  if ( m_obj != nullptr) m_obj->release();
}



bool  ConstMCExtendedHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCExtendedHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCExtendedHit::operator==(const MCExtendedHit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCExtendedHit& p1, const MCExtendedHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
