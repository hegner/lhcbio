#ifndef MCCaloDigit_H
#define MCCaloDigit_H
#include "MCCaloDigitData.h"
#include <vector>
#include "MCCaloHit.h"

#include <vector>
#include "podio/ObjectID.h"

// /< Monte Carlo active energy deposition in the given cell
// author: Vanya Belyaev Ivan.Belyaev@itep.ru

//forward declarations
class MCCaloDigitCollection;
class MCCaloDigitCollectionIterator;
class ConstMCCaloDigit;


#include "MCCaloDigitConst.h"
#include "MCCaloDigitObj.h"

class MCCaloDigit {

  friend MCCaloDigitCollection;
  friend MCCaloDigitCollectionIterator;
  friend ConstMCCaloDigit;

public:

  /// default constructor
  MCCaloDigit();
    MCCaloDigit(double m_activeE);

  /// constructor from existing MCCaloDigitObj
  MCCaloDigit(MCCaloDigitObj* obj);
  /// copy constructor
  MCCaloDigit(const MCCaloDigit& other);
  /// copy-assignment operator
  MCCaloDigit& operator=(const MCCaloDigit& other);
  /// support cloning (deep-copy)
  MCCaloDigit clone() const;
  /// destructor
  ~MCCaloDigit();

  /// conversion to const object
  operator ConstMCCaloDigit () const;

public:

  const double& m_activeE() const;

  void m_activeE(double value);

  void addm_hits(ConstMCCaloHit);
  unsigned int m_hits_size() const;
  ConstMCCaloHit m_hits(unsigned int) const;
  std::vector<ConstMCCaloHit>::const_iterator m_hits_begin() const;
  std::vector<ConstMCCaloHit>::const_iterator m_hits_end() const;

  /// check whether the object is actually available
  bool isAvailable() const;
  /// disconnect from MCCaloDigitObj instance
  void unlink(){m_obj = nullptr;};

  bool operator==(const MCCaloDigit& other) const {
       return (m_obj==other.m_obj);
  }

  bool operator==(const ConstMCCaloDigit& other) const;

// less comparison operator, so that objects can be e.g. stored in sets.
//  friend bool operator< (const MCCaloDigit& p1,
//       const MCCaloDigit& p2 );

  const podio::ObjectID getObjectID() const;

private:
  MCCaloDigitObj* m_obj;

};

#endif
