// datamodel specific includes
#include "MCCaloHit.h"
#include "MCCaloHitConst.h"
#include "MCCaloHitObj.h"
#include "MCCaloHitData.h"
#include "MCCaloHitCollection.h"
#include <iostream>
#include "MCParticle.h"

ConstMCCaloHit::ConstMCCaloHit() : m_obj(new MCCaloHitObj()){
 m_obj->acquire();
};

ConstMCCaloHit::ConstMCCaloHit(double m_activeE,int m_sensDetID,Time m_time) : m_obj(new MCCaloHitObj()){
 m_obj->acquire();
   m_obj->data.m_activeE = m_activeE;  m_obj->data.m_sensDetID = m_sensDetID;  m_obj->data.m_time = m_time;
};


ConstMCCaloHit::ConstMCCaloHit(const ConstMCCaloHit& other) : m_obj(other.m_obj) {
  m_obj->acquire();
}

ConstMCCaloHit& ConstMCCaloHit::operator=(const ConstMCCaloHit& other) {
  if ( m_obj != nullptr) m_obj->release();
  m_obj = other.m_obj;
  return *this;
}

ConstMCCaloHit::ConstMCCaloHit(MCCaloHitObj* obj) : m_obj(obj){
  if(m_obj != nullptr)
    m_obj->acquire();
}

ConstMCCaloHit ConstMCCaloHit::clone() const {
  return {new MCCaloHitObj(*m_obj)};
}

ConstMCCaloHit::~ConstMCCaloHit(){
  if ( m_obj != nullptr) m_obj->release();
}

  const ConstMCParticle ConstMCCaloHit::m_particle() const { if (m_obj->m_m_particle == nullptr) {
 return ConstMCParticle(nullptr);}
 return ConstMCParticle(*(m_obj->m_m_particle));};


bool  ConstMCCaloHit::isAvailable() const {
  if (m_obj != nullptr) {
    return true;
  }
  return false;
}

const podio::ObjectID ConstMCCaloHit::getObjectID() const {
  if (m_obj !=nullptr){
    return m_obj->id;
  }
  return podio::ObjectID{-2,-2};
}

bool ConstMCCaloHit::operator==(const MCCaloHit& other) const {
     return (m_obj==other.m_obj);
}

//bool operator< (const MCCaloHit& p1, const MCCaloHit& p2 ) {
//  if( p1.m_containerID == p2.m_containerID ) {
//    return p1.m_index < p2.m_index;
//  } else {
//    return p1.m_containerID < p2.m_containerID;
//  }
//}
