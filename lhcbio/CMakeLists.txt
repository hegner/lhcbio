## add include directory
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include ${podio_INCLUDE_DIRS})

## select all source files
file(GLOB sources ${CMAKE_CURRENT_SOURCE_DIR}/include/*.cc)

## create a library target
add_library(lhcbio SHARED ${sources})

## if needed, link against "someexternal"
## which has been included with find_package(someexternal)
#target_link_libraries(examplelibrary ${someexternal_LIBRARIES})

## install the example library into lib/
install(TARGETS lhcbio DESTINATION lib)

## install the headers
install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include/lhcbio DESTINATION include)

## handling of test executables
add_subdirectory(tests)
