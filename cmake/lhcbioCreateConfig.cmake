include(CMakePackageConfigHelpers)
configure_file(cmake/lhcbioConfig.cmake.in "${PROJECT_BINARY_DIR}/lhcbioConfig.cmake" @ONLY)
write_basic_package_version_file(${CMAKE_CURRENT_BINARY_DIR}/lhcbioConfigVersion.cmake
                                 VERSION ${lhcbio_VERSION}
                                 COMPATIBILITY SameMajorVersion )

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lhcbioConfig.cmake
              ${CMAKE_CURRENT_BINARY_DIR}/lhcbioConfigVersion.cmake
        DESTINATION ${CMAKE_INSTALL_PREFIX}/cmake )
